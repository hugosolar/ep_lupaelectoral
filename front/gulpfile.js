var gulp = require('gulp');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require( 'gulp-uglify' );
var cssmin = require('gulp-cssmin');
var concat = require( 'gulp-concat' );
var gutil = require('gulp-util');
var $    = require('gulp-load-plugins')();

var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];
var template_name = 'ep_municipales';
var js = {
  angular : [
    'bower_components/angular/angular.min.js',
    'bower_components/angular-route/angular-route.min.js',
    'bower_components/angular-sanitize/angular-sanitize.min.js',
    'bower_components/angular-i18n/angular-locale_es-cl.js',
    'bower_components/angular-simple-logger/dist/angular-simple-logger.js',
    'bower_components/ui-leaflet/dist/ui-leaflet.js'
  ],
  dependencies: [
    'bower_components/foundation-sites/dist/plugins/foundation.core.js',
    'bower_components/foundation-sites/dist/plugins/foundation.foundation.util.mediaQuery.js',
    'bower_components/foundation-sites/dist/plugins/foundation.dropdownMenu.js',
    'bower_components/foundation-sites/dist/plugins/foundation.sticky.js',
    'bower_components/foundation-sites/dist/plugins/foundation.tabs.js',
    'bower_components/foundation-sites/dist/plugins/foundation.util.keyboard.js',
    'bower_components/foundation-sites/dist/plugins/foundation.util.triggers.js',
    'bower_components/foundation-sites/dist/plugins/foundation.util.timerAndImageLoader.js',
    'bower_components/foundation-sites/dist/plugins/foundation.util.box.js',
    'bower_components/foundation-sites/dist/plugins/foundation.util.nest.js',
    'bower_components/foundation-sites/dist/plugins/foundation.interchange.js',
    'bower_components/foundation-sites/dist/plugins/foundation.tooltip.js'
  ],
  leaflet: [
    'bower_components/leaflet/dist/leaflet-src.js',
    'bower_components/leaflet.markercluster/dist/leaflet.markercluster-src.js'
  ],
  datatables: [
    'bower_components/pdfmake/build/pdfmake.js',
    'bower_components/pdfmake/build/vfs_fonts.js',
    'bower_components/js-xlsx/dist/xlsx.js',
    'bower_components/jszip/dist/jszip.js',
    'bower_components/datatables.net/js/jquery.dataTables.min.js',
    'bower_components/datatables-buttons/js/dataTables.buttons.js',
    'bower_components/datatables-responsive/js/dataTables.responsive.js',
    'bower_components/datatables-buttons/js/buttons.colVis.js',
    'bower_components/datatables-buttons/js/buttons.html5.js',
    'bower_components/datatables-buttons/js/buttons.flash.js',
    'bower_components/datatables-buttons/js/buttons.print.js',
    'bower_components/datatables-buttons/js/buttons.foundation.js',
    'bower_components/datatables-responsive/js/responsive.foundation.js',
  ],
  styles: [
    'bower_components/leaflet/dist/leaflet.css',
    'bower_components/datatables/media/css/jquery.dataTables.css',
    'bower_components/datatables/media/css/dataTables.foundation.css',
    'bower_components/leaflet.markercluster/dist/MarkerCluster.css',
    'bower_components/leaflet.markercluster/dist/MarkerCluster.Default.css'
  ],
  style_sass: [
    'bower_components/datatables-responsive/css/responsive.dataTables.scss',
    'bower_components/datatables-responsive/css/responsive.foundation.scss',
    'bower_components/datatables-buttons/css/buttons.foundation.scss'
  ]
};
/**
 * Preparing JavaScript
 */
gulp.task( 'build-dependencies-js', function() {
  return gulp.src( js.dependencies )
    .pipe( sourcemaps.init() )
    .pipe( concat( 'dependencies.js' ) )
    .pipe( uglify().on('error', gutil.log) )
    .pipe( sourcemaps.write( '/' ) )
    .pipe( gulp.dest( '../htdocs/wp-content/themes/'+template_name+'/js' ) );
});
gulp.task( 'build-angular-js', function() {
  return gulp.src( js.angular )
    .pipe( sourcemaps.init() )
    .pipe( concat( 'angular.js' ) )
    .pipe( sourcemaps.write( '/' ) )
    .pipe( gulp.dest( '../htdocs/wp-content/themes/'+template_name+'/js' ) );
});
gulp.task( 'build-leaflet-js', function() {
  return gulp.src( js.leaflet )
    .pipe( sourcemaps.init() )
    .pipe( concat( 'leaflet.js' ) )
    .pipe( sourcemaps.write( '/' ) )
    .pipe( gulp.dest( '../htdocs/wp-content/themes/'+template_name+'/js' ) );
});
gulp.task( 'build-datatables-js', function() {
  return gulp.src( js.datatables )
    .pipe( sourcemaps.init() )
    .pipe( concat( 'datatables.js' ) )
    .pipe( sourcemaps.write( '/' ) )
    .pipe( gulp.dest( '../htdocs/wp-content/themes/'+template_name+'/js' ) );
});
/*Styles*/
  gulp.task( 'build-dependencies-css', function() {
    return gulp.src( js.styles )
      .pipe( sourcemaps.init() )
      .pipe( concat( 'dependencies.css' ) )
      .pipe(cssmin())
      .pipe( sourcemaps.write( '/' ) )
      .pipe( gulp.dest( '../htdocs/wp-content/themes/'+template_name+'/css' ) );
});
/**
* Rsync amcharts
*/
gulp.task('sync', function() {
   gulp.src('bower_components/amcharts3/amcharts/**')
      .pipe(gulp.dest('../htdocs/wp-content/themes/'+template_name+'/js/amcharts'));
});

/**
* Compile SASS
*/
gulp.task('sass-dependencies', function() {
  return gulp.src( js.style_sass )
    .pipe(sourcemaps.init())
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed'
    })
    .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(sourcemaps.write())
    .pipe(rename('sass-dependencies.css'))
   .pipe( gulp.dest( '../htdocs/wp-content/themes/'+template_name+'/css' ) );
});

gulp.task('sass', function() {
  return gulp.src('./scss/app.scss')
    .pipe(sourcemaps.init())
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed'
    })
    .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(sourcemaps.write())
  .pipe(rename('style.css'))
  .pipe(gulp.dest('../htdocs/wp-content/themes/'+template_name))
    .pipe(gulp.dest('./css'));
});


gulp.task('default', ['sass', 'sass-dependencies', 'build-dependencies-js', 'build-angular-js', 'build-leaflet-js', 'build-datatables-js', 'sync', 'build-dependencies-css'], function() {
  gulp.watch(['./scss/**/*.scss'], ['sass']);
});
