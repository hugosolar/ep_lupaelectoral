<?php
/**
 * Plugin Name: EP Municipales data
 * Version: 2.0
 * Author: Espacio Público
 * Author URI: http://www.espaciopublico.cl
 * Description: Plugin responsable de proveer, una interfaz de importacion de datos a la plataforma, asi como manejar colas para tareas y disponibilizar datos a traves de wp-api.
 */

define('EPM_MAIN_FILE_PATH', __FILE__);
require 'inc/models.php';
require 'inc/colas_cronjob.php';
require 'inc/endpoints.php';
require 'inc/partials/_file_stats.php';

function ep_municipales_data()
{
    add_menu_page(
        '#LE Datos 2017',
        '#LE Datos 2017',
        'manage_options',
        'ep_importer',
        'page_denuncias'
    );
    add_submenu_page(
        'ep_importer',
        'Denuncias - LE Datos 2017',
        'Denuncias',
        'manage_options',
        'ep_importer_denuncias',
        'page_denuncias'
    );
    add_submenu_page(
        'ep_importer',
        'Elección Presidente - LE Datos 2017',
        'Elección Presidente',
        'manage_options',
        'ep_importer_presidente',
        'page_presidente'
    );
    add_submenu_page(
        'ep_importer',
        'Elección Senadores - LE Datos 2017',
        'Elección Senadores',
        'manage_options',
        'ep_importer_senadores',
        'page_senadores'
    );
    add_submenu_page(
        'ep_importer',
        'Elección Diputados - LE Datos 2017',
        'Elección Diputados',
        'manage_options',
        'ep_importer_diputados',
        'page_diputados'
    );
}
add_action("admin_menu", "ep_municipales_data");

function ep_municipales_importer_js_script()
{
    wp_register_script(
        'ep_municipales_importer_script',
        plugin_dir_url(EPM_MAIN_FILE_PATH) . "/inc/js/ep_municipales_importer_script.js",
        array("jquery")
    );
    wp_enqueue_script('ep_municipales_importer_script');
}
add_action('admin_enqueue_scripts', 'ep_municipales_importer_js_script');

function page_presidente() { page_generic('presidente'); }
function page_senadores() { page_generic('senadores'); }
function page_diputados() { page_generic('diputados'); }

function page_denuncias() {
    /**
     * Sección que elimina una carga de archivos de la cola
     */
    if (!empty($_GET['delete_item_queue'])) {
        require 'inc/partials/_delete_cola.php';
    }

    /**
     * Seccion que maneja la carga del archivo y la creacion del registro que
     * lo representa en la cola.
     */
    if (!empty($_FILES) && isset($_FILES["archivo_carga_datos"])) {
        require 'inc/partials/_file_upload.php';
    }

    require 'inc/partials/style.php';
    echo '
        <div class="wrap">
            <h2>Denuncias - Datos 2017</h2>
    ';
    require 'inc/partials/denuncias_2017.php';
    echo '
        </div>
    ';
}

function page_generic($tipo_eleccion = null) {
    /**
     * Sección que elimina una carga de archivos de la cola
     */
    if (!empty($_GET['delete_item_queue'])) {
        require 'inc/partials/_delete_cola.php';
    }

    /**
     * Seccion que maneja la carga del archivo y la creacion del registro que
     * lo representa en la cola.
     */
    if (!empty($_FILES) && isset($_FILES["archivo_carga_datos"])) {
        require 'inc/partials/_file_upload.php';
    }

    require 'inc/partials/style.php';
    echo '
        <div class="wrap">
            <h2>Elección ' . ucfirst($tipo_eleccion) . ' - Datos 2017</h2>
    ';
    require 'inc/partials/candidatos_2017.php';
    require 'inc/partials/aportes_2017.php';
    echo '
        </div>
    ';
}