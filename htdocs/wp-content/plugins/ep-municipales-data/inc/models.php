<?php

/**
 * Funciones Base de datos
 *
 * Carga el modelo de algunos componentes del plugin e instala la data
 * necesaria cuando se activa el plugin.
 */

/**
 * Colas
 */
global $colas_epm_db_version;
$colas_epm_db_version = '2.0';
function colas_epm_install()
{
    global $wpdb;
    global $colas_epm_db_version;
    $table_name      = $wpdb->prefix . 'colas_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    tabla varchar(250) NOT NULL,
    tipo_eleccion varchar(16) DEFAULT NULL,
    archivo_nombre varchar(250) NOT NULL,
    archivo_path text NOT NULL,
    archivo_url text NOT NULL,
    agregado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    ejecutado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    estado tinytext NOT NULL,
    UNIQUE KEY id (id)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('colas_epm_db_version', $colas_epm_db_version);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'colas_epm_install');

/**
 * Pactos
 */
global $pactos_epm_db_version;
$pactos_epm_db_version = '2.0';
function pactos_epm_install()
{
    global $wpdb;
    global $pactos_epm_db_version;
    $table_name      = $wpdb->prefix . 'pactos_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    nombre varchar(250) NOT NULL,
    description varchar(250),
    creado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    actualizado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    estado BOOL NOT NULL DEFAULT '1',
    UNIQUE KEY id (id)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('pactos_epm_db_version', $pactos_epm_db_version);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'pactos_epm_install');

/**
 * Partidos
 */
global $partidos_epm_db_version;
$partidos_epm_db_version = '2.0';
function partidos_epm_install()
{
    global $wpdb;
    global $partidos_epm_db_version;
    $table_name      = $wpdb->prefix . 'partidos_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    id_pacto mediumint(9),
    nombre varchar(250) NOT NULL,
    description varchar(250),
    creado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    actualizado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    estado BOOL NOT NULL DEFAULT '1',
    UNIQUE KEY id (id)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('partidos_epm_db_version', $partidos_epm_db_version);
}
function partidos_epm_install_data()
{
    global $wpdb;
    $sql = file_get_contents(
        ABSPATH .
        'wp-content/plugins/ep-municipales-data/base/partidos.sql'
    );
    $wpdb->query($sql);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'partidos_epm_install');
register_activation_hook(EPM_MAIN_FILE_PATH, 'partidos_epm_install_data');

/**
 * Candidatos
 * Toma en cuenta los tres tipos de elecciones: presidencial, senatorial y
 * diputación, haciendo la diferencia en el campo "tipo_ eleccion" y en los
 * datos de distribución geográfica.
 */
global $candidatos_epm_db_version;
$candidatos_epm_db_version = '2.0';
function candidatos_epm_install()
{
    global $wpdb;
    global $candidatos_epm_db_version;
    $table_name      = $wpdb->prefix . 'candidatos_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    tipo_eleccion varchar(16) NOT NULL,
    distrito varchar(250),
    id_distrito mediumint(5),
    circunscripcion varchar(250),
    id_circunscripcion mediumint(9),
    nombre_candidato varchar(250),
    partido varchar(250),
    id_partido mediumint(9),
    pacto varchar(250),
    id_pacto mediumint(9),
    url_declaracion varchar(250),
    aporte_partido_estimado integer DEFAULT 0,
    creado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    actualizado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    estado BOOL NOT NULL DEFAULT '1',
    UNIQUE KEY id (id)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('candidatos_epm_db_version', $candidatos_epm_db_version);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'candidatos_epm_install');

/**
 * Enlaces externos de candidato
 */
global $enlaces_epm_db_version;
$enlaces_epm_db_version = '2.0';
function enlaces_epm_install()
{
    global $wpdb;
    global $enlaces_epm_db_version;
    $table_name      = $wpdb->prefix . 'enlaces_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    id_candidato mediumint(9) NOT NULL,
    url varchar(256) NOT NULL,
    UNIQUE KEY id (id),
    FOREIGN KEY (id_candidato)
        REFERENCES " . $wpdb->prefix . "candidatos_epm(id)
        ON DELETE CASCADE
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('enlaces_epm_db_version', $enlaces_epm_db_version);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'enlaces_epm_install');

/**
 * Aportes
 */
global $aportes_epm_db_version;
$aportes_epm_db_version = '2.0';
function aportes_epm_install()
{
    global $wpdb;
    global $aportes_epm_db_version;
    $table_name      = $wpdb->prefix . 'aportes_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    tipo_eleccion varchar(16) NOT NULL,
    nombre_aportante varchar(250),
    id_candidato mediumint(9),
    nombre_candidato varchar(250),
    partido varchar(250),
    id_partido mediumint(9),
    pacto varchar(250),
    id_pacto mediumint(9),
    tipo_aporte varchar(250),
    fecha varchar(250),
    monto integer NOT NULL,
    vuelta TINYINT UNSIGNED DEFAULT NULL,
    creado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    actualizado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    estado BOOL NOT NULL DEFAULT '1',
    UNIQUE KEY id (id)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('aportes_epm_db_version', $aportes_epm_db_version);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'aportes_epm_install');

/**
 * Limites
 */
global $limites_epm_db_version;
$limites_epm_db_version = '2.0';
function limites_epm_install()
{
    global $wpdb;
    global $limites_epm_db_version;
    $table_name      = $wpdb->prefix . 'limites_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    tipo_eleccion varchar(16) NOT NULL,
    id_territorio int(5) DEFAULT NULL,
    limite BIGINT UNSIGNED NOT NULL,
    vuelta TINYINT UNSIGNED DEFAULT NULL,
    creado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    actualizado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    UNIQUE KEY id (id)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('limites_epm_db_version', $limites_epm_db_versions);
}
function limites_epm_install_data()
{
    global $wpdb;
    $sql = file_get_contents(
        ABSPATH .
        'wp-content/plugins/ep-municipales-data/base/limites.sql'
    );
    $wpdb->query($sql);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'limites_epm_install');
register_activation_hook(EPM_MAIN_FILE_PATH, 'limites_epm_install_data');

/**
 * Puntos
 */
global $puntos_epm_db_version;
$puntos_epm_db_version = '2.0';
function puntos_epm_install()
{
    global $wpdb;
    global $puntos_epm_db_version;
    $table_name      = $wpdb->prefix . 'puntos_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    comuna varchar(250),
    id_comuna mediumint(9),
    region varchar(250),
    id_region mediumint(9),
    epm_region_romano varchar(5) DEFAULT NULL,
    nombre_espacio varchar(250) NOT NULL,
    latitud varchar(250) NOT NULL,
    longitud varchar(250) NOT NULL,
    creado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    actualizado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    estado BOOL NOT NULL DEFAULT '1',
    UNIQUE KEY id (id)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('puntos_epm_db_version', $puntos_epm_db_version);
}
function puntos_epm_install_data()
{
    global $wpdb;
    $wpdb->query("
        INSERT INTO wp_colas_epm(
            tabla,
            archivo_nombre,
            archivo_path,
            archivo_url,
            agregado,
            estado
        )
        VALUES(
            'wp_puntos_epm',
            '2017_puntos.xlsx',
            '" . plugin_dir_path(EPM_MAIN_FILE_PATH) . "base/2017_puntos.xlsx" . "',
            '" . plugins_url('/base/2017_puntos.xlsx', EPM_MAIN_FILE_PATH) . "',
            '" . date('Y-m-d H:i:s') . "',
            'agregado'
        )
    ");
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'puntos_epm_install');
register_activation_hook(EPM_MAIN_FILE_PATH, 'puntos_epm_install_data');

/**
 * Denuncias
 */
global $denuncias_epm_db_version;
$denuncias_epm_db_version = '2.0';
function denuncias_epm_install()
{
    global $wpdb;
    global $denuncias_epm_db_version;
    $table_name      = $wpdb->prefix . 'denuncias_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    descripcion varchar(250),
    admisible integer,
    inadmisible integer,
    en_analisis integer,
    total integer,
    creado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    actualizado datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    estado BOOL NOT NULL DEFAULT '1',
    UNIQUE KEY id (id),
    KEY descripcion (descripcion)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('denuncias_epm_db_version', $denuncias_epm_db_version);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'denuncias_epm_install');

/**
 * Comunas
 */
global $comunas_epm_db_version;
$comunas_epm_db_version = '2.0';
function comunas_epm_install()
{
    global $wpdb;
    global $comunas_epm_db_version;
    $table_name      = $wpdb->prefix . 'comuna_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    COMUNA_ID int(5) NOT NULL DEFAULT '0',
    COMUNA_NOMBRE varchar(20) DEFAULT NULL,
    COMUNA_PROVINCIA_ID int(3) DEFAULT NULL,
    COMUNA_DISTRITO_ID int(5) DEFAULT NULL,
    PRIMARY KEY (`COMUNA_ID`),
    KEY `COMUNA_PROVINCIA_ID` (`COMUNA_PROVINCIA_ID`),
    KEY `COMUNA_DISTRITO_ID` (`COMUNA_DISTRITO_ID`)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('comunas_epm_db_version', $comunas_epm_db_version);
}
function comunas_epm_install_data()
{
    global $wpdb;
    $sql = file_get_contents(
        ABSPATH .
        'wp-content/plugins/ep-municipales-data/base/comunas_cl_utf8.sql'
    );
    $wpdb->query($sql);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'comunas_epm_install');
register_activation_hook(EPM_MAIN_FILE_PATH, 'comunas_epm_install_data');

/**
 * Provincias
 */
global $provincias_epm_db_version;
$provincias_epm_db_version = '2.0';
function provincias_epm_install()
{
    global $wpdb;
    global $provincias_epm_db_version;
    $table_name      = $wpdb->prefix . 'provincia_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    PROVINCIA_ID int(3) NOT NULL DEFAULT '0',
    PROVINCIA_NOMBRE varchar(23) DEFAULT NULL,
    PROVINCIA_REGION_ID int(2) DEFAULT NULL,
    PRIMARY KEY (`PROVINCIA_ID`),
    KEY `PROVINCIA_REGION_ID` (`PROVINCIA_REGION_ID`)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('provincias_epm_db_version', $provincias_epm_db_version);
}
function provincias_epm_install_data()
{
    global $wpdb;
    $sql = file_get_contents(
        ABSPATH .
        'wp-content/plugins/ep-municipales-data/base/provincias_cl_utf8.sql'
    );
    $wpdb->query($sql);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'provincias_epm_install');
register_activation_hook(EPM_MAIN_FILE_PATH, 'provincias_epm_install_data');

/**
 * Regiones
 */
global $regiones_epm_db_version;
$regiones_epm_db_version = '2.0';
function regiones_epm_install()
{
    global $wpdb;
    global $regiones_epm_db_version;
    $table_name      = $wpdb->prefix . 'region_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    REGION_ID int(2) NOT NULL DEFAULT '0',
    REGION_NOMBRE varchar(50) DEFAULT NULL,
    ISO_3166_2_CL varchar(5) DEFAULT NULL,
    ROMANO varchar(5) DEFAULT NULL,
    PRIMARY KEY (`REGION_ID`)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('regiones_epm_db_version', $regiones_epm_db_version);
}
function regiones_epm_install_data()
{
    global $wpdb;
    $sql = file_get_contents(
        ABSPATH .
        'wp-content/plugins/ep-municipales-data/base/regiones_cl_utf8.sql'
    );
    $wpdb->query($sql);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'regiones_epm_install');
register_activation_hook(EPM_MAIN_FILE_PATH, 'regiones_epm_install_data');

/**
 * Distritos
 */
global $distritos_epm_db_version;
$distritos_epm_db_version = '2.0';
function distritos_epm_install()
{
    global $wpdb;
    global $distritos_epm_db_version;
    $table_name      = $wpdb->prefix . 'distrito_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    DISTRITO_ID int(5) NOT NULL DEFAULT '0',
    DISTRITO_CANTIDAD_DIPUTADOS int(5) NOT NULL DEFAULT '0',
    DISTRITO_CIRCUNSCRIPCION_ID int(2) DEFAULT NULL,
    PRIMARY KEY (`DISTRITO_ID`),
    KEY `DISTRITO_CIRCUNSCRIPCION_ID` (`DISTRITO_CIRCUNSCRIPCION_ID`)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('distritos_epm_db_version', $distritos_epm_db_version);
}
function distritos_epm_install_data()
{
    global $wpdb;
    $sql = file_get_contents(
        ABSPATH .
        'wp-content/plugins/ep-municipales-data/base/distritos_cl_utf8.sql'
    );
    $wpdb->query($sql);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'distritos_epm_install');
register_activation_hook(EPM_MAIN_FILE_PATH, 'distritos_epm_install_data');

/**
 * Circunscripciones
 */
global $circunscripciones_epm_db_version;
$circunscripciones_epm_db_version = '2.0';
function circunscripciones_epm_install()
{
    global $wpdb;
    global $circunscripciones_epm_db_version;
    $table_name      = $wpdb->prefix . 'circunscripcion_epm';
    $charset_collate = $wpdb->get_charset_collate();
    $sql             = "CREATE TABLE $table_name (
    CIRCUNSCRIPCION_ID int(2) NOT NULL DEFAULT '0',
    CIRCUNSCRIPCION_CANTIDAD_SENADORES int(5) NOT NULL DEFAULT '0',
    CIRCUNSCRIPCION_REGION_ID int(2) DEFAULT NULL,
    PRIMARY KEY (`CIRCUNSCRIPCION_ID`),
    KEY `CIRCUNSCRIPCION_REGION_ID` (`CIRCUNSCRIPCION_REGION_ID`)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
    add_option('circunscripciones_epm_db_version', $circunscripciones_epm_db_version);
}
function circunscripciones_epm_install_data()
{
    global $wpdb;
    $sql = file_get_contents(
        ABSPATH .
        'wp-content/plugins/ep-municipales-data/base/circunscripciones_cl_utf8.sql'
    );
    $wpdb->query($sql);
}
register_activation_hook(EPM_MAIN_FILE_PATH, 'circunscripciones_epm_install');
register_activation_hook(EPM_MAIN_FILE_PATH, 'circunscripciones_epm_install_data');
