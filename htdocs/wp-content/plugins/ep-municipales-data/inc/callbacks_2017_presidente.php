<?php

/**
 * [presidente_get_candidatos_func obtiene los candidatos a presidente.]
 * @param  [param] $data [contiene o no el id de la comuna.]
 * @return [array]       [retorna el detalle de los candidatos por comuna.]
 */
function presidente_get_candidatos_func()
{
    global $wpdb;
    $result = $wpdb->get_results("
        SELECT *
        FROM wp_candidatos_epm
        WHERE tipo_eleccion = 'presidente'
    ");
    return $result;
}

/**
 * [get_limites_por_comuna_func obtiene los limites por comuna.]
 * @param  [param] $data [contiene o no el id de la comuna.]
 * @return [array]       [retorna el detalle de limites por comuna.]
 */
function presidente_get_limites_por_vuelta_func($data)
{
    global $wpdb;
    $vuelta  = esc_sql($data['id']);
    $limites = $wpdb->get_results("
        SELECT *
        FROM wp_limites_epm
        WHERE tipo_eleccion = 'presidente'
        AND vuelta = $vuelta
    ");

    if (is_null($limites)) {
        return [
            "error" => "No existe el limite para la vuelta especificada.",
        ];
    }

    $result = array(
        "comuna"  => $comuna,
        "limites" => $limites,
    );
    return $result;
}

/**
 * [presidente_get_aportes_by_candidato_func obtiene los aportes y sus respectivos limites y cantidades segun tipo.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de aportes por candidato con su detalle.]
 */
function presidente_get_aportes_by_candidato_func($data)
{
    global $diccionario_tipo_aportes;
    global $wpdb;

    $candidato_id = esc_sql($data['id']);
    $candidato    = $wpdb->get_row("
        SELECT *
        FROM wp_candidatos_epm
        WHERE id = $candidato_id
        AND tipo_eleccion = 'presidente'
    ");

    if (is_null($candidato)) {
        return [
            "error" => "No existe el candidato con el ID especificado.",
        ];
    }

    $enlaces = $wpdb->get_results("
        SELECT url
        FROM wp_enlaces_epm
        WHERE id_candidato = $candidato_id
    ");
    $limite_1ra_vuelta = $wpdb->get_row("
        SELECT *
        FROM wp_limites_epm
        WHERE tipo_eleccion = 'presidente'
        AND vuelta = 1
    ");
    $limite_2da_vuelta = $wpdb->get_row("
        SELECT *
        FROM wp_limites_epm
        WHERE tipo_eleccion = 'presidente'
        AND vuelta = 2
    ");
    $nombre_candidato = preg_replace('/\s+/', '%', $candidato->nombre_candidato);
    $aportes          = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE tipo_eleccion = 'presidente'
        AND estado = 1
        AND nombre_candidato LIKE '%$nombre_candidato%'
    ");

    $total                = 0;
    $total_primera_vuelta = 0;
    $total_segunda_vuelta = 0;
    $total_propio         = 0;
    $total_publico        = 0;
    $total_spublicidad    = 0;
    $total_partido        = 0;
    $total_credito        = 0;
    $total_por_tipo       = [];
    foreach ($aportes as $aporte) {
        $aporte_key = mb_strtoupper($aporte->tipo_aporte);
        if (array_key_exists($aporte_key, $diccionario_tipo_aportes)) {
            $total = $total + $aporte->monto;
            if ($aporte->vuelta == 1) {
                $total_primera_vuelta += $aporte->monto;
            } else if ($aporte->vuelta == 2) {
                $total_segunda_vuelta += $aporte->monto;
            }

            switch ($diccionario_tipo_aportes[$aporte_key]) {
                case 'PROPIO':
                    $total_propio = $total_propio + $aporte->monto;
                    break;
                case 'CON PUBLICIDAD':
                    $total_publico = $total_publico + $aporte->monto;
                    break;
                case 'SIN PUBLICIDAD':
                    $total_spublicidad = $total_spublicidad + $aporte->monto;
                    break;
                case 'PARTIDO POLÍTICO':
                    $total_partido = $total_partido + $aporte->monto;
                    break;
                case 'CRÉDITO':
                    $total_credito = $total_credito + $aporte->monto;
                    break;
            }
        }
    }

    $total_gastos = 0;
    // Armo el objeto de retorno.
    $result = [
        "total_gastos" => $total_gastos,
        "candidato"    => $candidato,
        "enlaces"      => $enlaces,
        "aportes"      => [
            "propio"            => $total_propio,
            "publico"           => $total_publico,
            "total_spublicidad" => $total_spublicidad,
            "partido"           => $total_partido,
            "credito"           => $total_credito,
            "total"             => $total,
        ],
        "limites"      => [
            "primera_vuelta" => [
                "total"  => $total_primera_vuelta,
                "limite" => $limite_1ra_vuelta->limite,
            ],
            "segunda_vuelta" => [
                "total"  => $total_segunda_vuelta,
                "limite" => $limite_2da_vuelta->limite,
            ],
        ],
    ];
    return $result;
}

/**
 * [presidente_get_aportes_por_candidato_func obtiene los aportes correspondientes a un candidato desde la BD.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de los aportes por candidato.]
 */
function presidente_get_aportes_por_candidato_func($data)
{
    global $wpdb;
    $candidato_id = esc_sql($data['id']);
    $candidato    = $wpdb->get_row("
        SELECT *
        FROM wp_candidatos_epm
        WHERE id = $candidato_id
        AND tipo_eleccion = 'presidente'
    ");

    if (is_null($candidato)) {
        return [
            "error" => "No existe el candidato con el ID especificado.",
        ];
    }

    $nombre_candidato = preg_replace('/\s+/', '%', $candidato->nombre_candidato);
    $result           = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE tipo_eleccion = 'presidente'
        AND estado = 1
        AND nombre_candidato LIKE '%$nombre_candidato%'
    ");
    return $result;
}
