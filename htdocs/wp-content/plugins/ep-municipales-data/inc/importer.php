<?php

require 'importer_helpers.php';

require_once 'spout-2.5.0/src/Spout/Autoloader/autoload.php';
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;

/**
 * [import_puntos_mapa description]
 * @param  [type] $filePath [description]
 * @return [type]           [description]
 */
function import_puntos_mapa($filePath, $tabla)
{
    global $wpdb;

    /*
    La tabla de puntos solo guarda la información del 2016 y el archivo que se carga contiene todos los puntos.
    Por lo tanto en cada carga del archivo es necesario eliminar los registros anteriores.
     */
    // Se eliminan los datos anteriores
    $wpdb->query("DELETE FROM $tabla");

    $reader = ReaderFactory::create(Type::XLSX);
    $reader->open($filePath);

    foreach ($reader->getSheetIterator() as $sheet) {
        $pass_header = false;
        // Only the first sheet
        if ($sheet->getIndex() === 0) {
            foreach ($sheet->getRowIterator() as $row) {
                // First row is the Column Header
                if (!$pass_header) {
                    $pass_header = true;
                    continue;
                }

                // Revisión mínima para ver si el registro es válido:
                //   - Si latitud viene vacio se considera invalido el registro
                //   - Si longitud viene vacio se considera invalido el registro
                //   - Si comuna viene vacio se considera invalido el registro
                if (empty(trim($row[3])) || empty(trim($row[4])) || empty(trim($row[1]))) {
                    continue;
                }

                // Preparo el objeto a insertar.
                $region_romano = _epm_get_romano($row[0], $row[1]);
                $comuna        = _epm_clean_comuna($row[1]);
                $punto_mapa    = [
                    "region"            => trim($row[0]),
                    "comuna"            => $comuna,
                    "nombre_espacio"    => trim($row[2]),
                    "latitud"           => trim($row[3]),
                    "longitud"          => trim($row[4]),
                    "epm_region_romano" => $region_romano,
                ];
                // Inserto el objeto en la BD.
                $wpdb->insert($tabla, $punto_mapa);
            }
        }
        break;
    }
    $reader->close();
}

/**
 * [import_aportes description]
 * @param  [type] $filePath [description]
 * @return [type]           [description]
 */
function import_aportes($filePath, $tabla, $tipo_eleccion)
{
    global $wpdb;

    /*
    Los archivos que se cargan para aportes corresponden al listao completo del año
    Es necesario eliminar los datos para evitar duplicados.
     */
    // Se eliminan los datos anteriores
    $wpdb->query("DELETE FROM $tabla WHERE tipo_eleccion = '$tipo_eleccion'");

    $reader = ReaderFactory::create(Type::XLSX);
    $reader->open($filePath);

    foreach ($reader->getSheetIterator() as $sheet) {
        $pass_header = false;
        // Only the first sheet
        if ($sheet->getIndex() === 0) {
            foreach ($sheet->getRowIterator() as $row) {
                // First row is the Column Header
                if (!$pass_header) {
                    $pass_header = true;
                    continue;
                }

                // Revisión mínima para ver si el registro es válido:
                //   - Si nombre_candidato viene vacio se considera invalido
                //   - Si monto viene vacio se considera invalido
                if (empty(trim($row[1])) || empty(trim($row[6]))) {
                    continue;
                }

                // Preparo el objeto a insertar.
                // $region_romano = _epm_get_romano($row[2], $row[3]);
                // $comuna        = _epm_clean_comuna($row[3]);
                $candidato = _epm_get_candidato($row[1], $tipo_eleccion);
                $monto     = (int) str_replace(['$', '.'], ['', ''], $row[6]);

                // Fecha ha llegado con dos formatos: numero excel que representa fecha o como obj DateTime
                if (is_numeric($row[5])) {
                    $fecha = gmdate("d-m-Y", (((int) $row[5] - 25569) * 86400));
                } else if ($row[5] instanceof \DateTime) {
                    $fecha = $row[5]->format("d-m-Y");
                } else {
                    $fecha = trim($row[5]);
                }

                $aporte = [
                    "nombre_aportante" => trim($row[0]),
                    "nombre_candidato" => $candidato['nombre'],
                    "pacto"            => trim($row[2]),
                    "partido"          => trim($row[3]),
                    "tipo_aporte"      => trim($row[4]),
                    "fecha"            => $fecha,
                    // INFO: Conversión a valor numérico
                    "monto"            => $monto,
                    "tipo_eleccion"    => $tipo_eleccion,
                    "estado"           => $candidato['estado'],
                ];

                // Si es eleccion presidencial, revisa de qué vuelva se trata el aporte
                if ($tipo_eleccion == 'presidente') {
                    $vuelta           = in_array(trim($row[7]), [1, 2]) ? trim($row[7]) : 1;
                    $aporte['vuelta'] = $vuelta;
                }

                // Inserto el objeto en la BD.
                $wpdb->insert($tabla, $aporte);
            }
        }
        break;
    }
    $reader->close();
}

/**
 * [import_candidatos description]
 * @param  [type] $filePath [description]
 * @return [type]           [description]
 */
function import_candidatos($filePath, $tabla, $tipo_eleccion)
{
    global $wpdb;

    // INFO: row que marca donde está el nombre del candidato
    $row_offset = [
        'presidente' => 0,
        'senadores'  => 2,
        'diputados'  => 3,
    ];

    /*
    Los archivos que se cargan para candidatos corresponden al listao completo del año
    Es necesario eliminar los datos para evitar duplicados.
     */
    // Se eliminan los datos anteriores
    $wpdb->query("DELETE FROM $tabla WHERE tipo_eleccion = '$tipo_eleccion'");

    $reader = ReaderFactory::create(Type::XLSX);
    $reader->open($filePath);

    foreach ($reader->getSheetIterator() as $sheet) {
        $pass_header = false;
        // Only the first sheet
        if ($sheet->getIndex() === 0) {
            foreach ($sheet->getRowIterator() as $row) {
                // First row is the Column Header
                if (!$pass_header) {
                    $pass_header = true;
                    continue;
                }

                // Revisión mínima para ver si el registro es válido:
                //  - Si nombre_candidato viene vacio se considera invalido
                //  - Si el distrito/circunscripción viene vacio se considera invalido
                if (
                    empty(trim($row[2])) ||
                    (
                        $tipo_eleccion != 'presidente' &&
                        empty(trim($row[$row_offset[$tipo_eleccion] - 1]))
                    )
                ) {
                    continue;
                }

                // Preparo el objeto a insertar.
                // $region_romano   = _epm_get_romano($row[0], $row[1]);
                // $comuna          = _epm_clean_comuna($row[1]);
                $url_declaracion = empty(trim(
                    $row[$row_offset[$tipo_eleccion] + 3])) ? null : trim(
                    $row[$row_offset[$tipo_eleccion] + 3]);
                $nombre = preg_replace('/\s+/', ' ',
                    trim($row[$row_offset[$tipo_eleccion]]));
                $candidato = [
                    "tipo_eleccion"           => $tipo_eleccion,
                    "nombre_candidato"        => $nombre,
                    "partido"                 => trim($row[$row_offset[$tipo_eleccion] + 1]),
                    "pacto"                   => trim($row[$row_offset[$tipo_eleccion] + 2]),
                    "url_declaracion"         => $url_declaracion,
                    "aporte_partido_estimado" => trim($row[$row_offset[$tipo_eleccion] + 4]),
                ];
                if ($tipo_eleccion == 'senadores') {
                    $rom = [ 'i' => 1, 'ii' => 2, 'iii' => 3, 'iv' => 4, 'v' => 5, 'vi' => 6, 'vii' => 7, 'viii' => 8, 'ix' => 9, 'x' => 10, 'xi' => 11, 'xii' => 12, 'xiii' => 13, 'xiv' => 14, 'xv' => 15, 'xvi' => 16, 'xvii' => 17, 'xviii' => 18, 'xix' => 19, 'xx' => 20 ];
                    $val = trim(str_replace(
                        'circunscripción',
                        '',
                        mb_convert_case($row[$row_offset[$tipo_eleccion] - 1], MB_CASE_LOWER)
                    ));
                    if (!is_numeric($val) && isset($rom[$val])) {
                        $val = $rom[$val];
                    }
                    $candidato['id_circunscripcion'] = $val;
                } else if ($tipo_eleccion == 'diputados') {
                    $candidato['id_distrito'] = trim($row[$row_offset[$tipo_eleccion] - 1]);
                }
                // Inserto el objeto en la BD.
                $wpdb->insert($tabla, $candidato);
                $id_candidato = $wpdb->insert_id;

                // Se agregan los enlaces relacionados
                $enlaces = explode(';', trim($row[$row_offset[$tipo_eleccion] + 5]));
                foreach ($enlaces as $url_enlace) {
                    $url_enlace = trim($url_enlace);
                    $enlace     = [
                        'id_candidato' => $id_candidato,
                        'url'          => $url_enlace,
                    ];
                    $wpdb->insert($wpdb->prefix . 'enlaces_epm', $enlace);
                }
            }
        }
        break;
    }
    $reader->close();
}

/**
 * [import_denuncias description]
 * @param  [type] $filePath [description]
 * @return [type]           [description]
 */
function import_denuncias($filePath, $tabla)
{
    global $wpdb;

    /*
    La tabla de denuncias solo guarda la información del 2016 y el archivo que se carga contiene toda la información.
    Por lo tanto en cada carga del archivo es necesario eliminar los registros anteriores.
     */
    // Se eliminan los datos anteriores
    $wpdb->query("DELETE FROM $tabla");

    $reader = ReaderFactory::create(Type::XLSX);
    $reader->open($filePath);

    foreach ($reader->getSheetIterator() as $sheet) {
        $pass_header = false;
        // Only the first sheet
        if ($sheet->getIndex() === 0) {
            foreach ($sheet->getRowIterator() as $row) {
                // First row is the Column Header
                if (!$pass_header) {
                    $pass_header = true;
                    continue;
                }

                // Revisión mínima para ver si el registro es válido:
                //   - Si descripcion viene vacio se considera invalido
                if (empty(trim($row[0]))) {
                    continue;
                }

                // Preparo el objeto a insertar.
                $tipo_denuncia = [
                    "descripcion" => trim($row[0]),
                    "admisible"   => trim($row[1]),
                    "inadmisible" => trim($row[2]),
                    "en_analisis" => trim($row[3]),
                    "total"       => trim($row[4]),
                ];
                // Inserto el objeto en la BD.
                $wpdb->insert($tabla, $tipo_denuncia);
            }
        }
        break;
    }
    $reader->close();
}
