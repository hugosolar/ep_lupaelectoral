<?php

require 'importer.php';

/******** Endpoint Trigger ********/
/**
 * [check_queue_and_import_file]
 * @param  [param]
 * @return [boolean]
 */
function check_queue_and_import_file()
{
    global $wpdb;
    global $epm_cache;
    $epm_cache = [];

    $queue = $wpdb->get_results(
        "
        SELECT *
        FROM wp_colas_epm
        WHERE estado = 'agregado'
        ",
        ARRAY_A
    );
    foreach ($queue as $item) {
        if (file_exists($item['archivo_path'])) {
            switch ($item['tabla']) {
                case 'wp_puntos_epm':
                    import_puntos_mapa($item['archivo_path'], $item['tabla']);
                    break;
                case 'wp_aportes_epm':
                    import_aportes($item['archivo_path'], $item['tabla'], $item['tipo_eleccion']);
                    break;
                case 'wp_candidatos_epm':
                    import_candidatos($item['archivo_path'], $item['tabla'], $item['tipo_eleccion']);
                    break;
                case 'wp_denuncias_epm':
                    import_denuncias($item['archivo_path'], $item['tabla']);
                    break;
            }

            $wpdb->update(
                'wp_colas_epm',
                ['estado' => 'ejecutado', 'ejecutado' => date('Y-m-d H:i:s')],
                array('id' => $item['id'])
            );
        }
        // else {
        //     echo "El fichero $item['archivo_path'] no existe";
        // }
    }
    return $queue;
}

/* Hook para registrar la tarea en wp cron */
register_activation_hook(EPM_MAIN_FILE_PATH, 'epm_cron_activation');
function epm_cron_activation()
{
    if (!wp_next_scheduled('cron_check_queue_and_import_file')) {
        wp_schedule_event(time(), 'hourly', 'cron_check_queue_and_import_file');
    }
}
add_action('cron_check_queue_and_import_file', 'check_queue_and_import_file');

register_deactivation_hook(EPM_MAIN_FILE_PATH, 'epm_cron_deactivation');
function epm_cron_deactivation()
{
    wp_clear_scheduled_hook('cron_check_queue_and_import_file');
}
