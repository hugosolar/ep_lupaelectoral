<?php

/**
 * [get_denuncias_por_candidato_func obtiene las denuncias correspondientes a un candidato desde la BD.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de las denuncias por candidato.]
 */
function get_denuncias_func()
{
    global $wpdb;
    $denuncias = $wpdb->get_results("
        SELECT descripcion,
            admisible,
            inadmisible,
            en_analisis,
            (admisible + inadmisible + en_analisis) AS 'total'
        FROM wp_denuncias_epm
        ORDER BY total DESC
    ");
    $total_tipos = [
        'admisible'   => 0,
        'inadmisible' => 0,
        'en_analisis' => 0,
        'total'       => 0,
    ];
    foreach ($denuncias as $tipo_denuncia) {
        $total_tipos['admisible'] += $tipo_denuncia->admisible;
        $total_tipos['inadmisible'] += $tipo_denuncia->inadmisible;
        $total_tipos['en_analisis'] += $tipo_denuncia->en_analisis;
        $total_tipos['total'] += $tipo_denuncia->total;
    }
    $rtn = [
        "denuncias" => $denuncias,
        "totales"   => $total_tipos,
    ];
    return $rtn;
}

/**
 * [get_denuncias_por_candidato_func obtiene las denuncias correspondientes a un candidato desde la BD.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de las denuncias por candidato.]
 */
function get_denuncias_por_candidato_func($data)
{
    global $wpdb;
    $candidato_id = $data['id'];
    $candidato    = $wpdb->get_row("
        SELECT *
        FROM wp_candidatos_epm
        WHERE id = $candidato_id
    ");

    if (is_null($candidato)) {
        return [
            "error" => "No existe el candidato con el ID especificado.",
        ];
    }

    $nombre_candidato = preg_replace('/\s+/', '%', $candidato->nombre_candidato);
    $result           = $wpdb->get_results(
        "
        SELECT *
        FROM wp_denuncias_epm
        WHERE denunciado LIKE '%$nombre_candidato%'
        "
    );
    return $result;
}

function get_denuncias_lastupdate()
{
    global $wpdb;
    $row = $wpdb->get_row("
        SELECT *
        FROM wp_colas_epm
        WHERE tabla = 'wp_denuncias_epm'
        ORDER BY agregado DESC
    ", ARRAY_A);
    if (is_null($row)) {
        return 'nunca';
    } else {
        return Date("d/m/Y", strtotime($row['ejecutado']));
    }
}
