<?php

global $wpdb;

$candidatos2017_candidatos = $wpdb->get_row("
    SELECT count(DISTINCT nombre_candidato) AS count
    FROM wp_candidatos_epm
    WHERE tipo_eleccion = '$tipo_eleccion'
    ", ARRAY_A);
$candidatos2017_partidos = $wpdb->get_row("
    SELECT count(DISTINCT partido) AS count
    FROM wp_candidatos_epm
    WHERE tipo_eleccion = '$tipo_eleccion'
    ", ARRAY_A);
$candidatos2017_lastloadedfile = $wpdb->get_row("
    SELECT *
    FROM wp_colas_epm
    WHERE tabla = 'wp_candidatos_epm'
    AND tipo_eleccion = '$tipo_eleccion'
    AND estado = 'ejecutado'
    ORDER BY ejecutado DESC
    ", ARRAY_A);
$candidatos2017_pendingfile = $wpdb->get_row("
    SELECT *
    FROM wp_colas_epm
    WHERE tabla = 'wp_candidatos_epm'
    AND tipo_eleccion = '$tipo_eleccion'
    AND estado = 'agregado'
    ORDER BY agregado DESC
    ", ARRAY_A);

if ($tipo_eleccion == 'senadores') {
    $candidatos2017_circunscripciones = $wpdb->get_row("
        SELECT count(DISTINCT id_circunscripcion) AS count
        FROM wp_candidatos_epm
        WHERE tipo_eleccion = '$tipo_eleccion'
    ", ARRAY_A);
    $table = [
        'head' => '<th scope="col" class="manage-column">Total Circunscripciones</th>',
        'value' => '<td>' . $candidatos2017_circunscripciones['count'] . '</td>',
    ];
} else if ($tipo_eleccion == 'diputados') {
    $candidatos2017_distritos = $wpdb->get_row("
        SELECT count(DISTINCT id_distrito) AS count
        FROM wp_candidatos_epm
        WHERE tipo_eleccion = '$tipo_eleccion'
    ", ARRAY_A);
    $table = [
        'head' => '<th scope="col" class="manage-column">Total Distritos</th>',
        'value' => '<td>' . $candidatos2017_distritos['count'] . '</td>',
    ];
} else {
    $table = [
        'head' => '',
        'value' => '',
    ];
}

$html_estadisticas_candidatos_2017 = '
            <table class="wp-list-table widefat fixed striped posts" style="width: auto;">
                <thead>
                    <tr>
                        <th scope="col" class="manage-column">Total Candidatos</th>
                        <th scope="col" class="manage-column">Total Partidos</th>
                        ' . $table['head'] . '
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>' . $candidatos2017_candidatos['count'] . '</td>
                        <td>' . $candidatos2017_partidos['count'] . '</td>
                        ' . $table['value'] . '
                    </tr>
                </tbody>
            </table>
';

echo _epm_echo_file_status('candidatos', $tipo_eleccion, $candidatos2017_lastloadedfile, $candidatos2017_pendingfile, $html_estadisticas_candidatos_2017);
