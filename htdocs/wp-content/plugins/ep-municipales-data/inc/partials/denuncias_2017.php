<?php

global $wpdb;

$denuncias2017_tipos = $wpdb->get_results("
    SELECT descripcion,
        admisible,
        inadmisible,
        en_analisis,
        (admisible + inadmisible + en_analisis) AS 'total'
    FROM wp_denuncias_epm
    ", ARRAY_A);
$denuncias2017_lastloadedfile = $wpdb->get_row("
    SELECT *
    FROM wp_colas_epm
    WHERE tabla = 'wp_denuncias_epm'
    AND estado = 'ejecutado'
    ORDER BY ejecutado DESC
    ", ARRAY_A);
$denuncias2017_pendingfile = $wpdb->get_row("
    SELECT *
    FROM wp_colas_epm
    WHERE tabla = 'wp_denuncias_epm'
    AND estado = 'agregado'
    ORDER BY agregado DESC
    ", ARRAY_A);

$total_tipos = [
    'admisible' => 0,
    'inadmisible' => 0,
    'en_analisis' => 0,
    'total' => 0,
];
foreach ($denuncias2017_tipos as $tipo_denuncia) {
    $total_tipos['admisible'] += $tipo_denuncia['admisible'];
    $total_tipos['inadmisible'] += $tipo_denuncia['inadmisible'];
    $total_tipos['en_analisis'] += $tipo_denuncia['en_analisis'];
    $total_tipos['total'] += $tipo_denuncia['total'];
}

$html_estadisticas_denuncias_2017 = '
            <table class="wp-list-table widefat fixed striped posts" style="width: auto;">
                <thead>
                    <tr>
                        <th scope="col" class="manage-column">Descripción</th>
                        <th scope="col" class="manage-column">Adminisible</th>
                        <th scope="col" class="manage-column">Inadmisible</th>
                        <th scope="col" class="manage-column">En análisis</th>
                        <th scope="col" class="manage-column">Total</th>
                    </tr>
                </thead>
                <tbody>
';
// foreach ($denuncias2016_tipos as $d_tipo) {
//     $html_estadisticas_denuncias_2016 .= '
//                 <tr>
//                     <td>' . $d_tipo['descripcion'] . '</td>
//                     <td>' . $d_tipo['admisible'] . '</td>
//                     <td>' . $d_tipo['inadmisible'] . '</td>
//                     <td>' . $d_tipo['en_analisis'] . '</td>
//                     <td>' . $d_tipo['total'] . '</td>
//                 </tr>
//     ';
// }
$html_estadisticas_denuncias_2017 .= '
                </tbody>
                <thead>
                    <tr>
                        <td scope="col" class="manage-column">Total general</td>
                        <td scope="col" class="manage-column">' . $total_tipos['admisible'] . '</td>
                        <td scope="col" class="manage-column">' . $total_tipos['inadmisible'] . '</td>
                        <td scope="col" class="manage-column">' . $total_tipos['en_analisis'] . '</td>
                        <td scope="col" class="manage-column">' . $total_tipos['total'] . '</td>
                    </tr>
                </thead>
            </table>
';

echo _epm_echo_file_status('denuncias', null, $denuncias2017_lastloadedfile, $denuncias2017_pendingfile, $html_estadisticas_denuncias_2017);
