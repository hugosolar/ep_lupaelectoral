<?php
    $gastos2016_countregistros = $wpdb->get_row("
        SELECT count(id) as count
        FROM wp_gastos_epm
        WHERE epm_year = '2016';
        ", ARRAY_A);
    $gastos2016_lastloadedfile = $wpdb->get_row("
        SELECT *
        FROM wp_colas_epm
        WHERE tabla = 'wp_gastos_epm'
        AND epm_year = '2016'
        AND estado = 'ejecutado'
        ORDER BY ejecutado DESC
        ", ARRAY_A);
    $gastos2016_pendingfile = $wpdb->get_row("
        SELECT *
        FROM wp_colas_epm
        WHERE tabla = 'wp_gastos_epm'
        AND epm_year = '2016'
        AND estado = 'agregado'
        ORDER BY agregado DESC
        ", ARRAY_A);

    $html_estadisticas_gastos_2016 = '
                <table class="wp-list-table widefat fixed striped posts" style="width: auto;">
                    <tbody>
                        <tr>
                            <td><strong>Total registros de Gastos</strong></td>
                            <td>' . $gastos2016_countregistros['count'] . '</td>
                        </tr>
                    </tbody>
                </table>
        ';

    echo _epm_echo_file_status('gastos', $gastos2016_lastloadedfile, $gastos2016_pendingfile, $html_estadisticas_gastos_2016);
