<?php

global $wpdb;

$id_cola = $_GET['delete_item_queue'];
$cola    = $wpdb->get_row("
	SELECT *
	FROM wp_colas_epm
	WHERE id = $id_cola
");

if ($wpdb->delete($wpdb->prefix . 'colas_epm', ['id' => $id_cola]) === false) {
    echo "
                <div class='notice notice-error is-dismissible'>
                    <p>Ocurrio un problema al intentar eliminar el elemento de la cola, por favor vuelve a intentarlo.</p>
                </div>
                ";
} else {
    if (!is_null($cola)) {
        @unlink($cola->archivo_path);
    }
    echo "
                <div class='notice notice-success is-dismissible'>
                    <p>Se eliminó el elemento de la cola de forma exitosa.</p>
                </div>
                ";
}
