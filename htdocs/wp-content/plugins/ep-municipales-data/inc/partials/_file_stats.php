<?php

function _epm_echo_file_status($tabla, $tipo_eleccion, $archivo_cargado, $archivo_pendiente, $html_propio)
{
    echo '
            <div id="' . strtolower($tabla) . '_2017">
                <h3>' . ucfirst($tabla) . '</h3>
    ';
    if (is_null($archivo_cargado) && is_null($archivo_pendiente)) {
        echo '
            <form class="createjobform" name="createjob" method="post" action="" enctype="multipart/form-data">
                <input type="hidden" name="ntabla" value="' . strtolower($tabla) . '_epm">
                <input type="hidden" name="ntipo" value="' . strtolower($tipo_eleccion) . '">
                <table class="wp-list-table widefat fixed striped posts" style="width: auto;">
                    <tbody>
                        <tr>
                            <td>Fecha última carga archivo</td>
                            <td><strong>No se ha cargado un archivo aún.</strong></td>
                        </tr>
                        <tr>
                            <td>
                                Subir nuevo archivo
                            </td>
                            <td>
                                <input type="file" name="archivo_carga_datos" accept=".xlsx" />
                                <input type="submit" class="button-success" value="Subir" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        ';
    } else {
        echo '
        <table><tr>
            <td>
                <table class="wp-list-table widefat fixed striped posts" style="width: auto;">
                    <tbody>
        ';
        if (is_null($archivo_pendiente)) {
            echo '
                        <tr>
                            <td><strong>Último archivo cargado</strong></td>
                            <td><a href="' . $archivo_cargado['archivo_url'] . '">' . $archivo_cargado['archivo_nombre'] . '</a></td>
                        </tr>
                        <tr>
                            <td><strong>Fecha última actualización</strong></td>
                            <td>' . Date("d/m/Y \a \l\a\s H:i", strtotime($archivo_cargado['ejecutado'])) . '</td>
                        </tr>
                        <tr>
                            <td><strong>Actualizar el archivo</strong></td>
                            <td>
                            <form name="createjob" method="post" action="" enctype="multipart/form-data">
                                <input type="hidden" name="ntabla" value="' . strtolower($tabla) . '_epm">
                                <input type="hidden" name="ntipo" value="' . strtolower($tipo_eleccion) . '">
                                <input type="file" name="archivo_carga_datos" accept=".xlsx" />
                                <input type="submit" class="button-success" value="Actualizar" />
                            </form>
                            </td>
                        </tr>
                ';
        } else {
            echo '
                        <tr>
                            <td>
                                El archivo <a href="' . $archivo_pendiente['archivo_url'] . '"><strong>' . $archivo_pendiente['archivo_nombre'] . '</strong></a> está a la espera de ser cargado al final del día.
                            </td>
                            <td>
                                <a class="button button-danger" href="http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '&delete_item_queue=' . $archivo_pendiente['id'] . '">Cancelar carga</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Fecha agregado</td>
                            <td>' . Date("d/m/Y \a \l\a\s H:i", strtotime($archivo_pendiente['agregado'])) . '</td>
                        </tr>
            ';
        }
        echo '
                    </tbody>
                </table>
            </td>
            <td style="padding-left: 50px;">
            ' . $html_propio . '
        ';
        if (!is_null($archivo_cargado)) {
            echo '
            <em style="padding-left: 5px;">
                Datos basados en <a href="' . $archivo_cargado['archivo_url'] . '">' . $archivo_cargado['archivo_nombre'] . '</a>
            </em>
            ';
        }
        echo '
            </td>
        </tr></table>
        ';
    }
    $base_url = '/base/base_' . strtolower($tabla);
    $base_url .= strtolower($tabla) == 'denuncias' ? '.xlsx' : '_' . strtolower($tipo_eleccion) . '.xlsx';
    echo '
                <br/>
                <a href="' . plugins_url($base_url, EPM_MAIN_FILE_PATH) . '">
                Descargar documento <strong>base</strong> para el formato de columnas que debe seguir el archivo de ' . ucfirst($tabla) . '
                </a>
            </div>
            <hr/>
    ';
}
