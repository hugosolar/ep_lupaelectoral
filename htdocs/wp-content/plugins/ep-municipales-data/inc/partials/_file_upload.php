<?php

global $wpdb;

$file = wp_handle_upload($_FILES["archivo_carga_datos"], array('test_form' => false));

if (isset($file['error'])) {
    return '';
}

// Se subio bien el archivo, ahora se crea la referencia en la cola.
$file_name = end(explode('/', $file['file']));
$tabla = $wpdb->prefix . esc_sql($_POST['ntabla']);
$tipo_eleccion = empty(esc_sql($_POST['ntipo'])) ? null : esc_sql($_POST['ntipo']);

$insert    = $wpdb->insert($wpdb->prefix . 'colas_epm', [
    'tabla'          => $tabla,
    'tipo_eleccion'  => $tipo_eleccion,
    'archivo_nombre' => $file_name,
    'archivo_path'   => esc_sql($file['file']),
    'archivo_url'    => esc_sql($file['url']),
    'agregado'       => current_time('mysql'),
    'estado'         => 'agregado',
]);

if ($insert === false) {
    echo "
			<div class='notice notice-error is-dismissible'>
				<p>Ocurrió un problema al intentar cargar el archivo, por favor vuelve a intentarlo.</p>
			</div>
			";
} else {
    echo "
			<div class='notice notice-success is-dismissible'>
				<p>El archivo se cargó de forma exitosa. <a href='/wp-admin/admin.php?page=ep_importer' target='_self'> << volver >> </a></p>
			</div>
			";
}
