<?php

global $wpdb;

$aportes2017_aportes = $wpdb->get_row("
    SELECT count(id) as count
    FROM wp_aportes_epm
    WHERE tipo_eleccion = '$tipo_eleccion'
    ", ARRAY_A);
$aportes2017_tipos = $wpdb->get_results("
    SELECT tipo_aporte, sum(1) AS sum
    FROM wp_aportes_epm
    WHERE tipo_eleccion = '$tipo_eleccion'
    GROUP BY tipo_aporte
    ", ARRAY_A);
$aportes2017_lastloadedfile = $wpdb->get_row("
    SELECT *
    FROM wp_colas_epm
    WHERE tabla = 'wp_aportes_epm'
    AND tipo_eleccion = '$tipo_eleccion'
    AND estado = 'ejecutado'
    ORDER BY ejecutado DESC
    ", ARRAY_A);
$aportes2017_pendingfile = $wpdb->get_row("
    SELECT *
    FROM wp_colas_epm
    WHERE tabla = 'wp_aportes_epm'
    AND tipo_eleccion = '$tipo_eleccion'
    AND estado = 'agregado'
    ORDER BY agregado DESC
    ", ARRAY_A);

$html_estadisticas_aportes_2017 = '
            <table class="wp-list-table widefat fixed striped posts" style="width: auto;">
                <tbody>
                    <tr>
                        <td><strong>Total aportes</strong></td>
                        <td>' . $aportes2017_aportes['count'] . '</td>
                    </tr>
';
foreach ($aportes2017_tipos as $a_tipo) {
    $html_estadisticas_aportes_2017 .= '
                <tr>
                    <td><strong>' . $a_tipo['tipo_aporte'] . '</strong></td>
                    <td>' . $a_tipo['sum'] . '</td>
                </tr>
    ';
}
$html_estadisticas_aportes_2017 .= '
            </tbody>
        </table>
';

echo _epm_echo_file_status('aportes', $tipo_eleccion, $aportes2017_lastloadedfile, $aportes2017_pendingfile, $html_estadisticas_aportes_2017);
