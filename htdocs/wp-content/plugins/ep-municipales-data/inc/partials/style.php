<?php

echo '
    <style>
a.button {
  -webkit-appearance: button;
  -moz-appearance: button;
  appearance: button;

  text-decoration: none;
  color: white !important;
}
.button-success {
  font-size: 14px;
  line-height: 26px;
  height: 28px;
  padding: 0px 10px 1px;
  color: white;
  text-align: center;
  text-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);
  background: #27ae60 !important;
  border: 0;
  border-bottom: 1px solid #219d55 !important;
  cursor: pointer;
  -webkit-box-shadow: inset 0 -2px #219d55;
  box-shadow: inset 0 -2px #219d55;
}
.button-success:hover {
  background: #32bf70 !important;
  border-top: none !important;
  top: 1px;
  outline: none;
  -webkit-box-shadow: none;
  box-shadow: none;
}
.button-danger {
  font-size: 14px;
  line-height: 26px;
  height: 28px;
  padding: 0px 10px 1px;
  color: white;
  text-align: center;
  text-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);
  background: #ff1a1a !important;
  border: 0;
  border-bottom: 1px solid #cc0000 !important;
  cursor: pointer;
  -webkit-box-shadow: inset 0 -2px #cc0000;
  box-shadow: inset 0 -2px #cc0000;
}
.button-danger:hover {
  background: #ff3333 !important;
  border-top: none !important;
  top: 1px;
  outline: none;
  -webkit-box-shadow: none;
  box-shadow: none;
}
    </style>
    ';
