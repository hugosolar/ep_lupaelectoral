<?php

require 'dictionary.php';
// require 'callbacks_municipales2016.php';
require 'callbacks_2017_general.php';
require 'callbacks_2017_denuncias.php';
require 'callbacks_2017_presidente.php';
require 'callbacks_2017_senadores.php';
require 'callbacks_2017_diputados.php';

function register_endpoint($uri, $callback)
{
    $args = ['uri' => $uri, 'callback' => $callback];
    add_action('rest_api_init', function () use ($args) {
        // register_rest_route('elecciones2017/v1', $uri,
        register_rest_route('municipales/v1', $args['uri'],
            array(
                'methods'  => 'GET',
                'callback' => $args['callback'],
            )
        );
    });
}

register_endpoint('/buscar/(?P<termino>[a-zA-Z0-9\s\%]+)', 'get_busqueda_termino_func');

register_endpoint('/puntos/(?P<id>\d+)', 'get_puntos_por_comuna_func');

register_endpoint('/regiones', 'get_regiones_func');
register_endpoint('/comunas', 'get_comunas_func');
register_endpoint('/comunas/(?P<id>\d+)', 'get_comunas_por_region_func');
register_endpoint('/comuna/(?P<id>\d+)', 'get_info_comuna_func');

register_endpoint('/denuncias', 'get_denuncias_func');
register_endpoint('/denuncias/nomina/(?P<id>\d+)', 'get_denuncias_por_candidato_func');
register_endpoint('/denuncias/lastupdate', 'get_denuncias_lastupdate');

// Generales
register_endpoint('/candidato/(?P<id>\d+)', 'get_candidato_func');
register_endpoint('/aportes/candidato/(?P<id>\d+)', 'get_aportes_by_candidato_func');
register_endpoint('/aportante/perfil/(?P<aportante>[a-zA-Z0-9\s\%][-a-zA-Z0-9\s\%]+)', 'get_aportes_by_aportante');

// Endpoints para elecciones de presidente
register_endpoint('/candidatos/presidente', 'presidente_get_candidatos_func');
register_endpoint('/limites/presidente/vuelta/(?P<id>\d+)', 'presidente_get_limites_por_vuelta_func');
register_endpoint('/aportes/presidente/candidato/(?P<id>\d+)', 'presidente_get_aportes_by_candidato_func');
register_endpoint('/aportes/presidente/nomina/(?P<id>\d+)', 'presidente_get_aportes_por_candidato_func');

// Endpoints para elecciones de senadores
register_endpoint('/candidatos/senadores/(?P<id>\d+)', 'senadores_get_candidatos_por_comuna_func');
register_endpoint('/limites/senadores/comuna/(?P<id>\d+)', 'senadores_get_limites_por_comuna_func');
register_endpoint('/aportes/senadores/candidato/(?P<id>\d+)', 'senadores_get_aportes_by_candidato_func');
register_endpoint('/aportes/senadores/nomina/(?P<id>\d+)', 'senadores_get_aportes_por_candidato_func');

// Endpoints para elecciones de diputados
register_endpoint('/candidatos/diputados/(?P<id>\d+)', 'diputados_get_candidatos_por_comuna_func');
register_endpoint('/limites/diputados/comuna/(?P<id>\d+)', 'diputados_get_limites_por_comuna_func');
register_endpoint('/aportes/diputados/candidato/(?P<id>\d+)', 'diputados_get_aportes_by_candidato_func');
register_endpoint('/aportes/diputados/nomina/(?P<id>\d+)', 'diputados_get_aportes_por_candidato_func');

// --
// TODO: ver si hay que implementarlas para cada tipo de elección
// register_endpoint('/aportes/regiones', 'get_aportes_por_region_func');
// register_endpoint('/aportes/region/(?P<id>\d+)', 'get_aportes_de_la_region_func');
// register_endpoint('/aportes/partidos', 'get_aportes_por_partido_func');
// register_endpoint('/aportes/partido/(?P<id>\d+)', 'get_aportes_por_partido_por_tipo_func');
// register_endpoint('/aportes/total/comuna/(?P<id>\d+)', 'get_aportes_total_por_comuna_func');
// register_endpoint('/aportes/comunas/region/(?P<id>\d+)', 'get_aportes_por_comuna_por_region_func');

// TODO: Eliminar (dejo referencias mientras para saber que existía y que se está eliminando adrede)
// register_endpoint('/aportes/top10monto', 'get_aportes_top10monto_func');
// register_endpoint('/aportes/top10limitecomuna', 'get_aportes_top10limitecomuna_func');
// register_endpoint('/aportes/top10montopadron', 'get_aportes_top10montopadron_func');
// register_endpoint('/aportes/comparativa2012tipo/(?P<id>\d+)', 'get_aportes_por_tipo_por_comuna_comparativa_func');
// register_endpoint('/aportes/top10regiones', 'get_aportes_top10regiones_func');
// register_endpoint('/aportes/regiones/comparativa2012', 'get_aportes_por_region_comparativa_func');
// register_endpoint('/aportes/partidos/comparativa2012', 'get_aportes_por_partido_comparativa_func');
// register_endpoint('/gastos/top10regiones', 'get_gastos_top10regiones_func');
// register_endpoint('/gastos/top10monto', 'get_gastos_top10monto_func');
// register_endpoint('/gastos/top10partidos', 'get_gastos_top10partidos_func');
// register_endpoint('/gastos/top10limitecomuna', 'get_gastos_top10limitecomuna_func');
// register_endpoint('/gastos/regiones/comparativa2012', 'get_gastos_por_region_comparativa_func');
// register_endpoint('/gastos/regiones', 'get_gastos_por_region_func');
// register_endpoint('/frontpage/indicadores', 'get_frontpage_indicadores');
// register_endpoint('/frontpage/top10comunas', 'get_frontpage_top10comunas');
