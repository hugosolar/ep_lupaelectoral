<?php

/******** Helpers ********/
function _epm_clean_comuna($comuna)
{
    $diccionario = [
        'AISEN'                      => 'AYSEN',
        'BIO BÍO'                    => 'BIOBÍO',
        'BIO BIO'                    => 'BIOBÍO',
        'MARCHIGUE'                  => 'MARCHIHUE',
        'CABO DE HORNOS Y ANTARTICA' => 'CABO DE HORNOS',
        'CALERA'                     => 'LA CALERA',
        'LA CALERA DE TANGO'         => 'CALERA DE TANGO',
        '`'                          => '\'',
        '-'                          => ' ',
        '’'                          => '\'',
    ];
    $comuna = mb_strtoupper(trim($comuna));
    return str_replace(array_keys($diccionario), array_values($diccionario), $comuna);
}

function _epm_get_romano($region, $comuna)
{
    global $wpdb;
    global $epm_cache;

    $escape_chars = [
        '`' => '\'',
        '-' => ' ',
        '’' => '\'',
    ];
    $p_region     = str_replace(array_keys($escape_chars), array_values($escape_chars), trim($region));
    $p_esc_region = esc_sql($p_region);
    $p_comuna     = _epm_clean_comuna($comuna);
    $p_esc_comuna = esc_sql($p_comuna);

    if (isset($epm_cache[$p_region])) {
        $region_romano = $epm_cache[$p_region];
    } else {
        $row = $wpdb->get_row("
            SELECT ROMANO
            FROM wp_region_epm
            WHERE REGION_NOMBRE LIKE '%$p_esc_region%'
            OR ROMANO = '$p_esc_region'
            " .
            (is_numeric($p_region) ? "OR REGION_ID = $p_region" : "")
            , ARRAY_A);
        if (is_null($row)) {
            if (isset($epm_cache[$p_comuna])) {
                $epm_cache[$p_region] = $epm_cache[$p_comuna];
                $region_romano        = $epm_cache[$p_comuna];
            } else {
                $row = $wpdb->get_row("
                    SELECT r.ROMANO
                    FROM wp_region_epm AS r
                    INNER JOIN wp_provincia_epm AS p
                    ON r.REGION_ID = p.PROVINCIA_REGION_ID
                    INNER JOIN wp_comuna_epm AS c
                    ON p.PROVINCIA_ID = c.COMUNA_PROVINCIA_ID
                    WHERE c.COMUNA_NOMBRE LIKE '%$p_esc_comuna%';
                    ", ARRAY_A);
                $epm_cache[$p_comuna] = $ROW['ROMANO'];
                $epm_cache[$p_region] = $row['ROMANO'];
                $region_romano        = $epm_cache[$p_region];
            }
        } else {
            $epm_cache[$p_region] = $row['ROMANO'];
            $region_romano        = $epm_cache[$p_region];
        }
    }

    return $region_romano;
}

function _epm_get_candidato($search_nombre, $tipo_eleccion)
{
    global $wpdb;
    global $epm_cache;

    $p_nombre     = trim($search_nombre);
    $p_esc_nombre = esc_sql($p_nombre);
    $r_nombre     = $p_nombre;
    $r_estado     = 0;

    if (isset($epm_cache[$p_nombre])) {
        $r_nombre = $epm_cache[$p_nombre];
        $r_estado = 1;
    } else {
        $row = $wpdb->get_row("
            SELECT *
            FROM wp_candidatos_epm
            WHERE nombre_candidato LIKE '%$p_esc_nombre%'
            AND tipo_eleccion = '$tipo_eleccion'
            ", ARRAY_A);
        if (!is_null($row)) {
            $epm_cache[$p_nombre] = $row['nombre_candidato'];
            $r_nombre             = $row['nombre_candidato'];
            $r_estado             = 1;
        } else {
            // Toma en cuenta el caso donde p_nombre viene con 2 nombres y 2 apellidos:
            //  -> Le quita el 2do nombre y trata de encontrar el registro nuevamente
            $p2_arr = explode(' ', $p_nombre);
            if (count($p2_arr) == 4) {
                unset($p2_arr[1]);
                $p2_nombre     = implode(' ', $p2_arr);
                $p2_esc_nombre = esc_sql($p2_nombre);
                $row           = $wpdb->get_row("
                    SELECT *
                    FROM wp_candidatos_epm
                    WHERE nombre_candidato LIKE '%$p2_esc_nombre%'
                    AND tipo_eleccion = '$tipo_eleccion'
                    ", ARRAY_A);
                if (!is_null($row)) {
                    $epm_cache[$p_nombre] = $row['nombre_candidato'];
                    $r_nombre             = $row['nombre_candidato'];
                    $r_estado             = 1;
                } else {
                    //  Le quita el 1er nombre y trata de encontrar el registro nuevamente
                    $p2_arr = explode(' ', $p_nombre);
                    unset($p2_arr[0]);
                    $p2_nombre     = implode(' ', $p2_arr);
                    $p2_esc_nombre = esc_sql($p2_nombre);
                    $row           = $wpdb->get_row("
                        SELECT *
                        FROM wp_candidatos_epm
                        WHERE nombre_candidato LIKE '%$p2_esc_nombre%'
                        AND tipo_eleccion = '$tipo_eleccion'
                        ", ARRAY_A);
                    if (!is_null($row)) {
                        $epm_cache[$p_nombre] = $row['nombre_candidato'];
                        $r_nombre             = $row['nombre_candidato'];
                        $r_estado             = 1;
                    }
                }
            } else if (count($p2_arr) > 4) {
                // Si tiene más de 4 palabras, arma el nombre sólo con la primera y las últimas dos
                // e intenta encontrar el candidato con el nuevo nombre armado
                $p2_nombre     = $p2_arr[0] . ' ' . $p2_arr[count($p2_arr) - 2] . ' ' . $p2_arr[count($p2_arr) - 1];
                $p2_esc_nombre = esc_sql($p2_nombre);
                $row           = $wpdb->get_row("
                    SELECT *
                    FROM wp_candidatos_epm
                    WHERE nombre_candidato LIKE '%$p2_esc_nombre%'
                    AND tipo_eleccion = '$tipo_eleccion'
                    ", ARRAY_A);
                if (!is_null($row)) {
                    $epm_cache[$p_nombre] = $row['nombre_candidato'];
                    $r_nombre             = $row['nombre_candidato'];
                    $r_estado             = 1;
                }
            }
        }
    }

    $rtn = [
        'nombre' => $r_nombre,
        'estado' => $r_estado,
    ];
    return $rtn;
}
