<?php

/**
 * [get_busqueda_termino_func obtiene el resultado de la busqueda contra comuna o canidatos.]
 * @param  [param] $data [contiene e termino a buscar.]
 * @return [array]       [retorna el total resultado del termino contra la tabla candidatos y comunas.]
 */
function get_busqueda_termino_func($data)
{
    global $wpdb;
    $termino = urldecode($data['termino']);
    // INFO: se busca con wildcards entre las palabras del term.
    $termino = preg_replace('/\s+/', '%', $termino);
    $termino = esc_sql($termino);
    $result  = $wpdb->get_results("
        SELECT tipo_eleccion AS 'tipo',
            nombre_candidato AS 'valor',
            id
        FROM wp_candidatos_epm
        WHERE nombre_candidato LIKE '%$termino%'
        UNION
        SELECT 'comuna' AS 'tipo',
            UPPER(COMUNA_NOMBRE) AS 'valor',
            COMUNA_ID AS 'id'
        FROM wp_comuna_epm
        WHERE COMUNA_NOMBRE LIKE '%$termino%'
        UNION
        SELECT 'aportante' AS 'tipo',
            UPPER(nombre_aportante) AS 'valor',
            nombre_aportante AS 'id'
        FROM wp_aportes_epm
        WHERE nombre_aportante LIKE '%$termino%'
        GROUP BY nombre_aportante
    ");
    return $result;
}

/**
 * [get_info_comuna_func devuelve la información base de la comuna]
 * @param [param] $data [contiene el id de comuna para devolver información]
 * @return [array]      [devuelve un array con la info de la comuna]
 */
function get_info_comuna_func($data)
{
    global $wpdb;
    $comuna_id = $data['id'];
    $comuna    = $wpdb->get_results("
        SELECT
            c.COMUNA_NOMBRE as comuna_nombre,
            p.PROVINCIA_NOMBRE as provincia_nombre,
            r.REGION_NOMBRE as region_nombre,
            d.DISTRITO_ID as distrito_id,
            d.DISTRITO_CIRCUNSCRIPCION_ID as circunscripcion_id
        FROM wp_comuna_epm AS c
        LEFT JOIN wp_provincia_epm AS p ON c.COMUNA_PROVINCIA_ID = p.PROVINCIA_ID
        LEFT JOIN wp_region_epm AS r ON p.PROVINCIA_REGION_ID = r.REGION_ID
        LEFT JOIN wp_distrito_epm AS d ON c.COMUNA_DISTRITO_ID = d.DISTRITO_ID
        WHERE c.COMUNA_ID = $comuna_id
    ");

    if (is_null($comuna)) {
        return [
            "error" => "No existe la comuna con el ID especificado.",
        ];
    }

    return $comuna;
}

/**
 * [get_regiones_func obtiene las regiones o region desde la BD.]
 * @param  [param] $data [contiene o no el id de la region.]
 * @return [array]       [retorna el detalle de la region, o el index de regiones.]
 */
function get_regiones_func()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'region_epm';
    $result     = $wpdb->get_results("
        SELECT *
        FROM $table_name
    ");
    return $result;
}

/**
 * [get_comunas_func obtiene las comunas o comuna desde la BD.]
 * @param  [param] $data [contiene o no el id de la comuna.]
 * @return [array]       [retorna el detalle de la comuna, o el index de comunas.]
 */
function get_comunas_func()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'comuna_epm';
    $result     = $wpdb->get_results("
        SELECT *
        FROM $table_name
    ");
    return $result;
}

/**
 * [get_comunas_por_region_func obtiene las comunas correspondientes a una region desde la BD.]
 * @param  [param] $data [contiene o no el id de la region.]
 * @return [array]       [retorna el detalle de las comunas por region.]
 */
function get_comunas_por_region_func($data)
{
    global $wpdb;
    $region_id = $data['id'];
    $comunas   = $wpdb->get_results("
        SELECT c.*
        FROM wp_provincia_epm AS p
        LEFT JOIN wp_comuna_epm as c
        ON p.PROVINCIA_ID = c.COMUNA_PROVINCIA_ID
        WHERE p.PROVINCIA_REGION_ID = $region_id
        ORDER BY c.COMUNA_NOMBRE ASC
    ");

    if (is_null($comunas)) {
        return [
            "error" => "No existe la comuna con el ID especificado.",
        ];
    }

    return $comunas;
}

/**
 * [get_puntos_por_comuna_func obtiene los puntos habilitados por comuna desde la BD.]
 * @param  [param] $data [contiene o no el id de la comuna.]
 * @return [array]       [retorna el detalle de los puntos por comuna.]
 */
function get_puntos_por_comuna_func($data)
{
    global $wpdb;
    $comuna_id = $data['id'];
    $comuna    = $wpdb->get_row("
        SELECT *
        FROM wp_comuna_epm
        WHERE COMUNA_ID = $comuna_id
        ");

    if (is_null($comuna)) {
        return [
            "error" => "No existe la comuna con el ID especificado.",
        ];
    }

    $nombre_comuna = esc_sql($comuna->COMUNA_NOMBRE);
    $result        = $wpdb->get_results("
        SELECT id,
            comuna, region, epm_region_romano,
            latitud, longitud,
            REPLACE(nombre_espacio, ' (PP)', '') AS 'nombre_espacio'
        FROM wp_puntos_epm
        WHERE comuna LIKE '$nombre_comuna'
    ");
    return $result;
}

/**
 * [get_aportes_by_aportante obtiene los aportes de una persona en especifico.]
 * @param  [param] $data [contiene el nombre de la persona.]
 * @return [array]       [retorna el detalle de aportes por candidato con su detalle.]
 */
function get_aportes_by_aportante($data)
{
    global $diccionario_tipo_aportes;
    global $wpdb;

    $nombre_aportante = preg_replace('/-/', '%', $data['aportante']);
    $aportes          = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE estado = 1
        AND nombre_aportante LIKE '%$nombre_aportante%'
    ");

    return $aportes;
}

/**
 * [get_aportes_by_candidato_func obtiene los aportes y sus respectivos limites y cantidades segun tipo.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de aportes por candidato con su detalle.]
 */
function get_aportes_by_candidato_func($data)
{
    global $diccionario_tipo_aportes;
    global $wpdb;

    $candidato_id = esc_sql($data['id']);
    $candidato    = $wpdb->get_row("
        SELECT *
        FROM wp_candidatos_epm
        WHERE id = $candidato_id
    ");

    if (is_null($candidato)) {
        return [
            "error" => "No existe el candidato con el ID especificado.",
        ];
    }

    $enlaces = $wpdb->get_results("
        SELECT url
        FROM wp_enlaces_epm
        WHERE id_candidato = $candidato_id
    ");
    $nombre_candidato = preg_replace('/\s+/', '%', $candidato->nombre_candidato);
    $aportes          = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE tipo_eleccion = 'diputados'
        AND estado = 1
        AND nombre_candidato LIKE '%$nombre_candidato%'
    ");

    $total             = 0;
    $total_propio      = 0;
    $total_publico     = 0;
    $total_spublicidad = 0;
    $total_partido     = 0;
    $total_credito     = 0;
    $total_por_tipo    = [];
    foreach ($aportes as $aporte) {
        $aporte_key = mb_strtoupper($aporte->tipo_aporte);
        if (array_key_exists($aporte_key, $diccionario_tipo_aportes)) {
            $total = $total + $aporte->monto;

            switch ($diccionario_tipo_aportes[$aporte_key]) {
                case 'PROPIO':
                    $total_propio = $total_propio + $aporte->monto;
                    break;
                case 'CON PUBLICIDAD':
                    $total_publico = $total_publico + $aporte->monto;
                    break;
                case 'SIN PUBLICIDAD':
                    $total_spublicidad = $total_spublicidad + $aporte->monto;
                    break;
                case 'PARTIDO POLÍTICO':
                    $total_partido = $total_partido + $aporte->monto;
                    break;
                case 'CRÉDITO':
                    $total_credito = $total_credito + $aporte->monto;
                    break;
            }
        }
    }

    $total_gastos = 0;
    // Armo el objeto de retorno.
    $result = [
        "total_gastos" => $total_gastos,
        "candidato"    => $candidato,
        "enlaces"      => $enlaces,
        "aportes"      => [
            "propio"            => $total_propio,
            "publico"           => $total_publico,
            "total_spublicidad" => $total_spublicidad,
            "partido"           => $total_partido,
            "credito"           => $total_credito,
            "total"             => $total,
        ],
    ];
    return $result;
}

function get_candidato_func($data)
{
    global $diccionario_tipo_aportes;
    global $wpdb;

    $candidato_id = esc_sql($data['id']);
    $candidato    = $wpdb->get_row("
        SELECT *
        FROM wp_candidatos_epm
        WHERE id = $candidato_id
    ");

    if (is_null($candidato)) {
        return [
            "error" => "No existe el candidato con el ID especificado.",
        ];
    }

    $col_name = $candidato->tipo_eleccion == 'diputados' ? 'DISTRITO_ID' : 'DISTRITO_CIRCUNSCRIPCION_ID';
    $col_val  = $candidato->tipo_eleccion == 'diputados' ? $candidato->id_distrito : $candidato->id_circunscripcion;
    $col_val = $col_val ? $col_val : 'null';
    $comuna   = $wpdb->get_row("
        SELECT c.COMUNA_ID AS comuna_id
        FROM wp_comuna_epm AS c
        LEFT JOIN wp_distrito_epm AS d
        ON c.COMUNA_DISTRITO_ID = d.DISTRITO_ID
        WHERE d.$col_name = $col_val
    ");
    $candidato->comuna_id = $comuna ? $comuna->comuna_id : null;

    return $candidato;
}
