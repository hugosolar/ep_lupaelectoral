<?php


/**
 * [get_gastos_por_candidato_func obtiene los gastos correspondientes a un candidato desde la BD.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de los gastos por candidato.]
 */
function get_gastos_por_candidato_func($data)
{
    global $wpdb;
    $candidato_id = $data['id'];
    $candidato    = $wpdb->get_row("
        SELECT *
        FROM wp_candidatos_epm
        WHERE id = $candidato_id
        ");
    $nombre_candidato = preg_replace('/\s+/', '%', $candidato->nombre_candidato);
    $result           = $wpdb->get_results(
        "
        SELECT *
        FROM wp_gastos_epm
        WHERE epm_year = '2016'
        AND nombre_candidato LIKE '%$nombre_candidato%'
        "
    );
    return $result;
}
/* Hook para colgar el metodo que obtiene datos. */
add_action('rest_api_init', function () {
    register_rest_route('municipales/v1', '/gastos/nomina/(?P<id>\d+)',
        array(
            'methods'  => 'GET',
            'callback' => 'get_gastos_por_candidato_func',
        )
    );
});

/**
 * [get_aportes_por_region_func obtiene los aportes y sus respectivos limites y cantidades segun tipo.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de aportes por candidato con su detalle.]
 */
function get_gastos_por_region_func()
{
    global $wpdb;
    $result    = [];
    $_regiones = $wpdb->get_results("
        SELECT *
        FROM wp_region_epm
        ");
    // Regiones en orden de norte a sur
    $regiones = [
        $_regiones[14], $_regiones[0], $_regiones[1], $_regiones[2], $_regiones[3],
        $_regiones[4], $_regiones[12], $_regiones[5], $_regiones[6], $_regiones[7],
        $_regiones[8], $_regiones[13], $_regiones[9], $_regiones[10], $_regiones[11],
    ];
    foreach ($regiones as $region) {
        $gastos = $wpdb->get_row("
            SELECT SUM(g.monto_documento) as 'monto'
            FROM wp_comuna_epm AS c
            INNER JOIN wp_provincia_epm AS p
            ON p.provincia_id = c.comuna_provincia_id
            INNER JOIN wp_region_epm AS r
            ON p.provincia_region_id = r.region_id
            INNER JOIN wp_gastos_epm AS g
            ON c.comuna_nombre LIKE g.comuna
            WHERE g.epm_year = '2016'
            AND r.romano = '$region->ROMANO'
            ");
        $gastos->monto = is_null($gastos->monto) ? 0 : $gastos->monto;
        array_push($result, array($region, $gastos));
    }
    return $result;
}

/**
 * [get_aportes_por_candidato_func obtiene los aportes correspondientes a un candidato desde la BD.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de los aportes por candidato.]
 */
function get_aportes_por_candidato_func($data)
{
    global $wpdb;
    $candidato_id = $data['id'];
    $candidato    = $wpdb->get_row("
        SELECT *
        FROM wp_candidatos_epm
        WHERE id = $candidato_id
        ");
    $nombre_candidato = preg_replace('/\s+/', '%', $candidato->nombre_candidato);
    $result           = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE epm_year = '2016'
        AND estado = 1
        AND nombre_candidato LIKE '%$nombre_candidato%'
        ");
    return $result;
}

/**
 * [get_puntos_por_comuna_func obtiene los puntos habilitados por comuna desde la BD.]
 * @param  [param] $data [contiene o no el id de la comuna.]
 * @return [array]       [retorna el detalle de los puntos por comuna.]
 */
function get_puntos_por_comuna_func($data)
{
    global $wpdb;
    $comuna_id = $data['id'];
    $comuna    = $wpdb->get_row("
        SELECT *
        FROM wp_comuna_epm
        WHERE COMUNA_ID = $comuna_id
        ");
    $nombre_comuna = esc_sql($comuna->COMUNA_NOMBRE);
    $result        = $wpdb->get_results("
        SELECT id,
            comuna, region, epm_region_romano,
            latitud, longitud,
            REPLACE(nombre_espacio, ' (PP)', '') AS 'nombre_espacio'
        FROM wp_puntos_epm
        /* WHERE comuna LIKE '%$nombre_comuna%' -- TODO: revisar si no limita algun resultado el cambio */
        WHERE comuna LIKE '$nombre_comuna'
        ");
    return $result;
}

/**
 * [get_aportes_y_candidatos_por_comuna_func obtiene los candidatos y sus respectivos aportes por comuna desde la BD.]
 * @param  [param] $data [contiene o no el id de la comuna.]
 * @return [array]       [retorna el detalle de aportes por candidato por comuna.]
 */
function get_aportes_y_candidatos_por_comuna_func($data)
{
    global $wpdb;
    $comuna_id = $data['id'];
    $comuna    = $wpdb->get_row("
        SELECT *
        FROM wp_comuna_epm
        WHERE COMUNA_ID = $comuna_id
        ");
    $nombre_comuna = esc_sql($comuna->COMUNA_NOMBRE);
    $candidatos    = $wpdb->get_results("
        SELECT *
        FROM wp_candidatos_epm
        /* WHERE comuna LIKE '%$nombre_comuna%' -- TODO: revisar si no limita algun resultado el cambio */
        WHERE comuna LIKE '$nombre_comuna'
        ");
    $result = [];
    foreach ($candidatos as $candidato) {
        $nombre_candidato     = preg_replace('/\s+/', '%', $candidato->nombre_candidato);
        $candidato->id_comuna = $comuna_id;
        $aportes              = $wpdb->get_results("
            SELECT *
            FROM wp_aportes_epm
            WHERE epm_year = '2016'
            AND estado = 1
            AND nombre_candidato LIKE '%$nombre_candidato%'
            ");
        array_push($result, array($candidato, array("aportes" => $aportes)));
    }
    return $result;
}

/**
 * [get_aportes_by_candidato_func obtiene los aportes y sus respectivos limites y cantidades segun tipo.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de aportes por candidato con su detalle.]
 */
function get_aportes_by_candidato_func($data)
{
    global $diccionario_tipo_aportes;
    global $wpdb;
    $candidato_id = $data['id'];
    $candidato    = $wpdb->get_row("
        SELECT *
        FROM wp_candidatos_epm
        WHERE id = $candidato_id
        ");
    $comuna_candidato = esc_sql($candidato->comuna);
    $nombre_candidato = preg_replace('/\s+/', '%', $candidato->nombre_candidato);
    $limite           = $wpdb->get_row("
        SELECT *
        FROM wp_limites_epm
        /* WHERE comuna LIKE '%$comuna_candidato%' -- TODO: revisar si no limita algun resultado el cambio */
        WHERE comuna LIKE '$comuna_candidato'
        ");
    $comuna = $wpdb->get_row("
        SELECT *
        FROM wp_comuna_epm
        /* WHERE COMUNA_NOMBRE LIKE '%$comuna_candidato%' -- TODO: revisar si no limita algun resultado el cambio */
        WHERE COMUNA_NOMBRE LIKE '$comuna_candidato'
        ");
    $aportes = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE epm_year = '2016'
        AND estado = 1
        AND nombre_candidato LIKE '%$nombre_candidato%'
        ");
    $total             = 0;
    $total_propio      = 0;
    $total_publico     = 0;
    $total_spublicidad = 0;
    $total_partido     = 0;
    $total_por_tipo    = [];
    foreach ($aportes as $aporte) {
        $aporte_key = mb_strtoupper($aporte->tipo_aporte);
        if (array_key_exists($aporte_key, $diccionario_tipo_aportes)) {
            $total = $total + $aporte->monto;

            switch ($diccionario_tipo_aportes[$aporte_key]) {
                case 'PROPIO':
                    $total_propio = $total_propio + $aporte->monto;
                    break;
                case 'CON PUBLICIDAD':
                    $total_publico = $total_publico + $aporte->monto;
                    break;
                case 'SIN PUBLICIDAD':
                    $total_spublicidad = $total_spublicidad + $aporte->monto;
                    break;
                case 'PARTIDO POLÍTICO':
                    $total_partido = $total_partido + $aporte->monto;
                    break;
            }
        }
    }
    $total_gastos_row = $wpdb->get_row("
        SELECT SUM(monto_documento) AS 'monto'
        FROM wp_gastos_epm
        WHERE epm_year = '2016'
        AND nombre_candidato LIKE '$nombre_candidato'
        ");
    $total_gastos = is_null($total_gastos_row->monto) ? 0 : $total_gastos_row->monto;
    // Armo el objeto de retorno.
    $result                 = [];
    $result["total_gastos"] = $total_gastos;
    $result["candidato"]    = $candidato;
    $result["limite"]       = $limite->limite;
    $result["comuna"]       = $comuna;
    $result["aportes"]      = [
        "propio"            => $total_propio,
        "publico"           => $total_publico,
        "total_spublicidad" => $total_spublicidad,
        "partido"           => $total_partido,
        "total"             => $total,
    ];
    return $result;
}

/**
 * [get_aportes_por_region_func obtiene los aportes y sus respectivos limites y cantidades segun tipo.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de aportes por candidato con su detalle.]
 */
function get_aportes_por_region_func()
{
    global $wpdb;
    $result    = [];
    $_regiones = $wpdb->get_results("
        SELECT *
        FROM wp_region_epm
        ");
    // Regiones en orden de norte a sur
    $regiones = [
        $_regiones[14], $_regiones[0], $_regiones[1], $_regiones[2], $_regiones[3],
        $_regiones[4], $_regiones[12], $_regiones[5], $_regiones[6], $_regiones[7],
        $_regiones[8], $_regiones[13], $_regiones[9], $_regiones[10], $_regiones[11],
    ];
    foreach ($regiones as $region) {
        $aportes = $wpdb->get_row("
            SELECT SUM(monto) as 'monto'
            FROM wp_aportes_epm
            WHERE epm_year = '2016'
            AND estado = 1
            AND epm_region_romano = '$region->ROMANO'
            ");
        $aportes->monto = is_null($aportes->monto) ? 0 : $aportes->monto;
        array_push($result, array($region, $aportes));
    }
    return $result;
}

/**
 * [get_aportes_de_la_region_func obtiene los aportes por partido, listo de aportes por tipo y cantidad total, ademas de los gastos.]
 * @param  [param] $data [contiene o no el id del candidato.]
 * @return [array]       [retorna el detalle de aportes por candidato con su detalle.]
 */
function get_aportes_de_la_region_func($data)
{
    global $wpdb;
    global $diccionario_tipo_aportes;
    global $diccionario_partidos;
    $region_id = $data['id'];

    // Obtengo la region para usar el string con su nombre.
    $region = $wpdb->get_row("
        SELECT *
        FROM wp_region_epm
        WHERE REGION_ID = $region_id
        ");
    // Obtengo comunas de la región
    $comunas = $wpdb->get_results("
        SELECT *
        FROM wp_comuna_epm AS c
        INNER JOIN wp_provincia_epm AS p
        ON c.COMUNA_PROVINCIA_ID = p.PROVINCIA_ID
        WHERE p.PROVINCIA_REGION_ID = $region_id
        ");
    // Obtengo los aportes del 2016 por region.
    $aportes_2016 = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE epm_region_romano = '$region->ROMANO'
        AND epm_year = '2016'
        AND estado = 1
        ", ARRAY_A);
    // Obtengo los aportes del 2012 por region.
    $tipos_diccionario_str = implode("','", array_keys($diccionario_tipo_aportes));
    $aportes_2012          = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE epm_region_romano = '$region->ROMANO'
        AND epm_year = '2012'
        AND estado = 1
        AND UPPER(tipo_aporte) IN ('" . $tipos_diccionario_str . "')
        ", ARRAY_A);
    // Total de aportes de la región
    $monto_total_aportes_region = array_sum(array_column($aportes_2016, 'monto'));
    // Cantidad de aportes
    $cantidad_total_aportes_region = count($aportes_2016);

    // Aportes por tipo de aporte y por partido.
    $aportes_por_partido     = array();
    $aportes_por_tipo_aporte = [
        '2016' => [
            'PROPIO'           => 0,
            'CON PUBLICIDAD'   => 0,
            'SIN PUBLICIDAD'   => 0,
            'PARTIDO POLÍTICO' => 0,
        ],
        '2012' => [
            'PROPIO'           => 0,
            'CON PUBLICIDAD'   => 0,
            'SIN PUBLICIDAD'   => 0,
            'PARTIDO POLÍTICO' => 0,
        ],
    ];
    foreach ($aportes_2016 as $aporte) {
        // Partido
        $nombre_partido = isset($diccionario_partidos[$aporte['partido']]) ? $diccionario_partidos[$aporte['partido']] : $aporte['partido'];
        if (array_key_exists($nombre_partido, $aportes_por_partido)) {
            $aportes_por_partido[$nombre_partido] += $aporte['monto'];
        } else {
            $aportes_por_partido[$nombre_partido] = $aporte['monto'];
        }

        // Tipo aporte
        // Solo se toman en cuenta los que pueden ser comparados
        $aporte_key = mb_strtoupper($aporte['tipo_aporte']);
        if (array_key_exists($aporte_key, $diccionario_tipo_aportes)) {
            $aportes_por_tipo_aporte['2016'][$diccionario_tipo_aportes[$aporte_key]] += $aporte['monto'];
        }
    }

    $sum_aportes_2012 = 0;
    foreach ($aportes_2012 as $aporte) {
        // Tipo aporte
        // Solo se toman en cuenta los que pueden ser comparados
        $aporte_key = mb_strtoupper($aporte['tipo_aporte']);
        if (array_key_exists($aporte_key, $diccionario_tipo_aportes)) {
            $sum_aportes_2012 += $aporte['monto'];
            $aportes_por_tipo_aporte['2012'][$diccionario_tipo_aportes[$aporte_key]] += $aporte['monto'];
        }
    }

    // GASTOS
    // Creo un array para contener los gastos del 2016 por comuna correspondientes a la region en cuestion.
    $gastos_por_tipo = array();
    $gastos          = $wpdb->get_results("
        SELECT g.descripcion_documento,
            SUM(g.monto_documento) AS 'monto'
        FROM wp_region_epm AS r
        LEFT JOIN wp_provincia_epm AS p
        ON p.PROVINCIA_REGION_ID = r.REGION_ID
        LEFT JOIN wp_comuna_epm AS c
        ON c.COMUNA_PROVINCIA_ID = p.PROVINCIA_ID
        LEFT JOIN wp_gastos_epm AS g
        ON g.comuna LIKE c.COMUNA_NOMBRE
        WHERE r.REGION_ID = $region_id
        AND g.epm_year = '2016'
        GROUP BY g.descripcion_documento
        ", ARRAY_A);
    foreach ($gastos as $gasto) {
        $gastos_por_tipo[$gasto['descripcion_documento']] = $gasto['monto'];
    }

    // Construyo los objetos a retornar.
    // $cantidad_total_gastos_region = 0;
    // $monto_total_gastos_region    = 0;
    $result = array(
        "region"                        => $region,
        "aportes_por_partido"           => $aportes_por_partido,
        "aportes_por_tipo_aporte"       => $aportes_por_tipo_aporte,
        "cantidad_total_aportes_region" => $cantidad_total_aportes_region,
        "monto_total_aportes_region"    => $monto_total_aportes_region,
        "gastos_por_tipo"               => $gastos_por_tipo,
        "total_aportes_2012"            => $sum_aportes_2012,
    );
    return $result;
}

/**
 * [get_aportes_por_partido_func obtiene los limites por comuna.]
 * @param  [param] $data [contiene o no el id de la comuna.]
 * @return [array]       [retorna el detalle de limites por comuna.]
 */
function get_aportes_por_partido_func()
{
    global $wpdb;
    global $diccionario_partidos;
    $aportes = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE epm_year = '2016'
        AND estado = 1
        ", ARRAY_A);
    $result = array();
    foreach ($aportes as $aporte) {
        // Partido
        $nombre_partido = isset($diccionario_partidos[$aporte['partido']]) ? $diccionario_partidos[$aporte['partido']] : $aporte['partido'];
        if (array_key_exists($nombre_partido, $result)) {
            array_push($result[$nombre_partido], $aporte);
        } else {
            $result[$nombre_partido] = array();
            array_push($result[$nombre_partido], $aporte);
        }
    }
    return $result;
}

/**
 * [get_aportes_por_partido_por_tipo_func obtiene los aportes por partido segun id y de ellos por tipo.]
 * @param  [param] $data [contiene el id del partido.]
 * @return [array]       [retorna el detalle de aportes por partido por tipo.]
 */
function get_aportes_por_partido_por_tipo_func($data)
{
    global $wpdb;
    global $diccionario_tipo_aportes;
    global $diccionario_partidos;
    $partido_id = $data['id'];
    $partido    = $wpdb->get_row("
        SELECT *
        FROM wp_partidos_epm
        WHERE id = $partido_id
        ");
    $partido_sinonimos     = array_keys($diccionario_partidos, $partido->nombre);
    $partido_sinonimos_str = empty($partido_sinonimos) ? $partido->nombre : implode("','", $partido_sinonimos);
    $aportes               = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE epm_year = '2016'
        AND estado = 1
        AND partido IN ('" . $partido_sinonimos_str . "')
        ", ARRAY_A);
    $aportes_por_tipo = [
        'PROPIO'         => [],
        'CON PUBLICIDAD' => [],
        'SIN PUBLICIDAD' => [],
    ];
    foreach ($aportes as $aporte) {
        // Tipo
        $aporte_key = mb_strtoupper($aporte['tipo_aporte']);
        if (array_key_exists($aporte_key, $diccionario_tipo_aportes)) {
            $aportes_por_tipo[$diccionario_tipo_aportes[$aporte_key]][] = $aporte;
        }
    }
    $result = array(
        "partido" => $partido,
        "aportes" => $aportes_por_tipo,
    );
    return $result;
}

/**
 * [get_aportes_total_por_comuna_func obtiene el total de aportes por comuna.]
 * @param  [param] $data [contiene el id de la comuna.]
 * @return [array]       [retorna el total de aportes de la comuna consultada.]
 */
function get_aportes_total_por_comuna_func($data)
{
    global $wpdb;
    $comuna_id = $data['id'];
    $comuna    = $wpdb->get_row("
        SELECT *
        FROM wp_comuna_epm
        WHERE COMUNA_ID = $comuna_id
        ");
    $aportes = $wpdb->get_results("
        SELECT *
        FROM wp_aportes_epm
        WHERE epm_year = '2016'
        AND estado = 1
        /* AND comuna LIKE '%$comuna->COMUNA_NOMBRE%' -- TODO: revisar si no limita algun resultado el cambio */
        AND comuna LIKE '$comuna->COMUNA_NOMBRE'
        ", ARRAY_A);
    $result = array(
        "total" => array_sum(array_column($aportes, 'monto')),
    );
    return $result;
}

/**
 * [get_aportes_top10regiones_func obtiene 10 regiones con mas aporte.]
 * @param  [param] []
 * @return [array]       [retorna el detalle de limites por comuna.]
 */
function get_aportes_top10regiones_func()
{
    global $wpdb;
    global $diccionario_tipo_aportes;

    $_regiones = $wpdb->get_results("
        SELECT REGION_NOMBRE AS 'region',
            ROMANO AS 'epm_region_romano',
            0 AS 'monto_2016'
        FROM wp_region_epm
        ORDER BY region ASC
        ", ARRAY_A);
    $aportes_2016 = $wpdb->get_results("
        SELECT region,
            epm_region_romano,
            SUM(monto) AS 'monto_2016'
        FROM wp_aportes_epm
        WHERE epm_year = '2016'
        AND estado = 1
        GROUP BY epm_region_romano
        ORDER BY monto_2016 DESC
        ", ARRAY_A);
    $_aportes = [];
    foreach ($aportes_2016 as $aporte) {
        $_aportes[$aporte['epm_region_romano']] = true;
    }
    foreach ($_regiones as $region) {
        if (!isset($_aportes[$region['epm_region_romano']])) {
            $aportes_2016[] = $region;
        }
    }

    $result = array();
    foreach ($aportes_2016 as $aporte) {
        $romano                = $aporte['epm_region_romano'];
        $tipos_diccionario_str = implode("','", array_keys($diccionario_tipo_aportes));
        $monto_2012_row        = $wpdb->get_row("
            SELECT SUM(monto) as 'monto_2012'
            FROM wp_aportes_epm
            WHERE epm_year = '2012'
            AND estado = 1
            AND epm_region_romano = '$romano'
            AND UPPER(tipo_aporte) IN ('" . $tipos_diccionario_str . "')
            ", ARRAY_A);
        $monto_2012           = is_null($monto_2012_row['monto_2012']) ? 0 : $monto_2012_row['monto_2012'];
        $aporte['monto_2012'] = $monto_2012;
        $region               = $wpdb->get_row("
            SELECT *
            FROM wp_region_epm
            WHERE ROMANO = '$romano'
            ", ARRAY_A);
        $resumen = array(
            "region" => $region,
            "aporte" => $aporte,
        );
        array_push($result, $resumen);
    }
    return $result;
}

/**
 * [get_aportes_top10regiones_func obtiene 10 regiones con mas aporte.]
 * @param  [param] []
 * @return [array]       [retorna el detalle de limites por comuna.]
 */
function get_gastos_top10regiones_func()
{
    global $wpdb;
    $gastos_2016 = $wpdb->get_results("
        SELECT SUM(g.monto_documento) as 'monto_2016',
            r.REGION_NOMBRE,
            r.ROMANO
        FROM wp_comuna_epm AS c
        INNER JOIN wp_provincia_epm AS p
        ON p.provincia_id = c.comuna_provincia_id
        INNER JOIN wp_region_epm AS r
        ON p.provincia_region_id = r.region_id
        INNER JOIN wp_gastos_epm AS g
        ON c.comuna_nombre LIKE g.comuna
        WHERE g.epm_year = '2016'
        GROUP BY r.ROMANO
        ORDER BY monto_2016 DESC
        LIMIT 10
        ", ARRAY_A);
    $result = array();
    foreach ($gastos_2016 as $gasto) {
        $romano         = $gasto['ROMANO'];
        $monto_2012_row = $wpdb->get_row("
            SELECT SUM(g.monto_documento) as 'monto_2012'
            FROM wp_comuna_epm AS c
            INNER JOIN wp_provincia_epm AS p
            ON p.provincia_id = c.comuna_provincia_id
            INNER JOIN wp_region_epm AS r
            ON p.provincia_region_id = r.region_id
            INNER JOIN wp_gastos_epm AS g
            ON c.comuna_nombre LIKE g.comuna
            WHERE g.epm_year = '2012'
            AND r.ROMANO = '$romano'
            ", ARRAY_A);
        $monto_2012          = is_null($monto_2012_row['monto_2012']) ? 0 : $monto_2012_row['monto_2012'];
        $gasto['monto_2012'] = $monto_2012;
        $region              = $wpdb->get_row("
            SELECT *
            FROM wp_region_epm
            WHERE ROMANO = '$romano'
            ", ARRAY_A);
        $resumen = array(
            "region" => $region,
            "gasto"  => $gasto,
        );
        array_push($result, $resumen);
    }
    return $result;
}

/**
 * [get_aportes_top10regiones_func obtiene 10 candidatos con mas aporte.]
 * @param  [param] []
 * @return [array]       [retorna el detalle de limites por comuna.]
 */
function get_aportes_top10monto_func()
{
    global $wpdb;
    $result = $wpdb->get_results("
            SELECT nombre_candidato,
                   comuna,
                   SUM(monto) AS 'monto'
            FROM wp_aportes_epm
            WHERE epm_year = '2016'
            AND estado = 1
            GROUP BY nombre_candidato
            ORDER BY monto DESC
            LIMIT 10
        ", ARRAY_A);
    return $result;
}

/**
 * [get_aportes_top10regiones_func obtiene 10 candidatos con mas aporte.]
 * @param  [param] []
 * @return [array]       [retorna el detalle de limites por comuna.]
 */
function get_gastos_top10monto_func()
{
    global $wpdb;
    $result = $wpdb->get_results("
            SELECT nombre_candidato,
                   comuna,
                   SUM(monto_documento) AS 'monto'
            FROM wp_gastos_epm
            WHERE epm_year = '2016'
            GROUP BY nombre_candidato
            ORDER BY monto DESC
            LIMIT 10
        ", ARRAY_A);
    return $result;
}

/**
 * [get_aportes_top10regiones_func obtiene 10 candidatos con mas aporte.]
 * @param  [param] []
 * @return [array]       [retorna el detalle de limites por comuna.]
 */
function get_gastos_top10partidos_func()
{
    global $wpdb;
    $result = $wpdb->get_results("
            SELECT nombre_partido,
                   nombre_pacto,
                   SUM(monto_documento) AS 'monto'
            FROM wp_gastos_epm
            WHERE epm_year = '2016'
            GROUP BY nombre_partido,nombre_pacto
            ORDER BY monto DESC
            LIMIT 10
        ", ARRAY_A);
    return $result;
}

/**
 * [get_aportes_top10regiones_func obtiene 10 candidatos % del limite con mas aporte.]
 * @param  [param] []
 * @return [array]       [retorna el detalle de limites por comuna.]
 */
function get_aportes_top10limitecomuna_func()
{
    global $wpdb;
    $result = $wpdb->get_results(
        "
        SELECT a.nombre_candidato AS 'nombre',
            a.comuna AS 'comuna',
            sum(a.monto) AS 'monto',
            l.limite AS 'limite',
            (( sum(a.monto)/l.limite ) * 100 ) AS 'porcentaje'
        FROM wp_aportes_epm AS a
        INNER JOIN wp_limites_epm AS l
        ON l.comuna LIKE a.comuna
        WHERE a.epm_year = '2016'
        AND a.estado = 1
        GROUP BY a.nombre_candidato
        ORDER BY porcentaje DESC
        LIMIT 10
        ", ARRAY_A);
    return $result;
}

/**
 * [get_aportes_top10regiones_func obtiene 10 candidatos % del limite con mas aporte.]
 * @param  [param] []
 * @return [array]       [retorna el detalle de limites por comuna.]
 */
function get_gastos_top10limitecomuna_func()
{
    global $wpdb;
    $result = $wpdb->get_results(
        "
        SELECT g.comuna AS 'comuna',
            SUM(monto_documento) AS 'monto',
            l.limite AS 'limite',
            ((sum(g.monto_documento)/l.limite) * 100) AS 'porcentaje'
        FROM wp_gastos_epm AS g
        INNER JOIN wp_limites_epm AS l
        ON l.comuna LIKE g.comuna
        WHERE g.epm_year = '2016'
        GROUP BY g.comuna
        ORDER BY porcentaje DESC
        LIMIT 10
        ", ARRAY_A);
    return $result;
}

/**
 * [get_aportes_top10montopadron_func obtiene 10 candidatos con mas aporte por elector.]
 * @param  [param] []
 * @return [array]       [retorna 10 candidatos con mas aporte por elector.]
 */
function get_aportes_top10montopadron_func()
{
    global $wpdb;
    $result = $wpdb->get_results(
        "
        SELECT a.nombre_candidato AS 'nombre',
            a.comuna AS 'comuna',
            sum(a.monto) AS 'monto',
            e.electores AS 'cantidad_electores',
            (sum(a.monto)/e.electores) AS 'monto_x_elector'
        FROM wp_aportes_epm AS a
        INNER JOIN wp_electores_epm AS e
        ON e.comuna LIKE a.comuna
        WHERE a.epm_year = '2016'
        AND a.estado = 1
        GROUP BY a.nombre_candidato
        ORDER BY monto_x_elector DESC
        LIMIT 10
        ", ARRAY_A);
    return $result;
}

/**
 * [get_busqueda_termino_func obtiene el resultado de la busqueda contra comuna o canidatos.]
 * @param  [param] $data [contiene e termino a buscar.]
 * @return [array]       [retorna el total resultado del termino contra la tabla candidatos y comunas.]
 */
function get_busqueda_termino_func($data)
{
    global $wpdb;
    $termino = urldecode($data['termino']);
    // INFO: se busca con wildcards entre las palabras del term.
    $termino = preg_replace('/\s+/', '%', $termino);
    $termino = esc_sql($termino);
    $result  = $wpdb->get_results(
        "
        SELECT 'candidato' AS 'tipo',
            nombre_candidato AS 'valor',
            id
        FROM wp_candidatos_epm
        WHERE nombre_candidato LIKE '%$termino%'
        AND epm_year = '2016'
        UNION
        SELECT 'comuna' AS 'tipo',
            UPPER(COMUNA_NOMBRE) AS 'valor',
            COMUNA_ID AS 'id'
        FROM wp_comuna_epm
        WHERE COMUNA_NOMBRE LIKE '%$termino%'
        "
    );
    return $result;
}


/**
 * [get_aportes_por_region_comparativa_func]
 * @param  [param] []
 * @return [array]       [retorna el detalle de aportes por region.]
 */
function get_aportes_por_region_comparativa_func()
{
    global $wpdb;
    global $diccionario_tipo_aportes;

    $tipos_diccionario_str = implode("','", array_keys($diccionario_tipo_aportes));
    $_obj_2012             = $wpdb->get_results("
        SELECT r.REGION_ID AS 'id',
            r.REGION_NOMBRE AS 'region',
            r.ROMANO AS 'romano',
            SUM(monto) AS 'monto'
        FROM wp_aportes_epm AS a
        INNER JOIN wp_region_epm AS r
        ON r.ROMANO = a.epm_region_romano
        WHERE a.epm_year = '2012'
        AND a.estado = 1
        AND UPPER(a.tipo_aporte) IN ('" . $tipos_diccionario_str . "')
        GROUP BY a.region
        ORDER BY monto DESC
        ", ARRAY_A);
    $obj_2016 = $wpdb->get_results("
        SELECT r.REGION_ID AS 'id',
            r.REGION_NOMBRE AS 'region',
            r.ROMANO AS 'romano',
            SUM(monto) AS 'monto'
        FROM wp_aportes_epm AS a
        INNER JOIN wp_region_epm AS r
        ON r.ROMANO = a.epm_region_romano
        WHERE a.epm_year = '2016'
        AND a.estado = 1
        GROUP BY a.region
        ORDER BY monto DESC
        ", ARRAY_A);

    $obj_2012 = [];
    for ($i = 0; $i < count($obj_2016); $i++) {
        for ($j = 0; $j < count($_obj_2012); $j++) {
            if ($obj_2016[$i]['romano'] === $_obj_2012[$j]['romano']) {
                $obj_2012[] = $_obj_2012[$j];
                break;
            }
        }
    }

    $result = array(
        "2012" => $obj_2012,
        "2016" => $obj_2016,
    );
    return $result;
}

/**
 * [get_aportes_por_region_comparativa_func]
 * @param  [param] []
 * @return [array]       [retorna el detalle de aportes por region.]
 */
function get_gastos_por_region_comparativa_func()
{
    global $wpdb;
    $obj_2012 = $wpdb->get_results("
        SELECT SUM(g.monto_documento) as 'monto',
            r.REGION_NOMBRE AS 'region',
            r.ROMANO AS 'romano',
            r.REGION_ID AS 'id'
        FROM wp_comuna_epm AS c
        INNER JOIN wp_provincia_epm AS p
        ON p.provincia_id = c.comuna_provincia_id
        INNER JOIN wp_region_epm AS r
        ON p.provincia_region_id = r.region_id
        INNER JOIN wp_gastos_epm AS g
        ON c.comuna_nombre LIKE g.comuna
        WHERE g.epm_year = '2012'
        GROUP BY r.ROMANO
        ORDER BY monto DESC
    ", ARRAY_A);
    $obj_2016 = $wpdb->get_results("
        SELECT SUM(g.monto_documento) as 'monto',
            r.REGION_NOMBRE AS 'region',
            r.ROMANO AS 'romano',
            r.REGION_ID AS 'id'
        FROM wp_comuna_epm AS c
        INNER JOIN wp_provincia_epm AS p
        ON p.provincia_id = c.comuna_provincia_id
        INNER JOIN wp_region_epm AS r
        ON p.provincia_region_id = r.region_id
        INNER JOIN wp_gastos_epm AS g
        ON c.comuna_nombre LIKE g.comuna
        WHERE g.epm_year = '2016'
        GROUP BY r.ROMANO
        ORDER BY monto DESC
    ", ARRAY_A);
    $result = array(
        "2012" => $obj_2012,
        "2016" => $obj_2016,
    );
    return $result;
}

/**
 * [get_aportes_por_partido_comparativa_func]
 * @param  [param] []
 * @return [array]       [retorna el detalle de aportes por partido.]
 */
function get_aportes_por_partido_comparativa_func()
{
    global $wpdb;

    $tipos_diccionario_str = implode("','", array_keys($diccionario_tipo_aportes));
    $obj_2012              = $wpdb->get_results("
        SELECT partido, SUM(monto) as 'monto'
        FROM wp_aportes_epm
        WHERE epm_year = '2012'
        AND estado = 1
        AND UPPER(tipo_aporte) IN ('" . $tipos_diccionario_str . "')
        GROUP BY partido
        ORDER BY monto DESC
        ", ARRAY_A);
    $obj_2016 = $wpdb->get_results("
        SELECT partido, SUM(monto) as 'monto'
        FROM wp_aportes_epm
        WHERE epm_year = '2016'
        AND estado = 1
        GROUP BY partido
        ORDER BY monto DESC
        ", ARRAY_A);
    $result = array(
        "2012" => $obj_2012,
        "2016" => $obj_2016,
    );
    return $result;
}

/**
 * [get_aportes_por_tipo_por_comuna_comparativa_func]
 * @param  [param] $data [contiene e id de la comuna.]
 * @return [array]       [retorna el detalle de aportes por partido y por comuna.]
 */
function get_aportes_por_tipo_por_comuna_comparativa_func($data)
{
    global $wpdb;
    global $diccionario_tipo_aportes;

    $comuna_id = $data['id'];
    $comuna    = $wpdb->get_row("
        SELECT *
        FROM wp_comuna_epm
        WHERE COMUNA_ID LIKE '$comuna_id'
        ");
    $nombre_comuna         = esc_sql($comuna->COMUNA_NOMBRE);
    $tipos_diccionario_str = implode("','", array_keys($diccionario_tipo_aportes));
    $obj_2012              = $wpdb->get_results("
        SELECT tipo_aporte, SUM(monto) as 'monto'
        FROM wp_aportes_epm
        WHERE epm_year = '2012'
        AND estado = 1
        /* AND comuna LIKE '%$nombre_comuna%' -- TODO: revisar si no limita algun resultado el cambio */
        AND comuna LIKE '$nombre_comuna'
        AND UPPER(tipo_aporte) IN ('" . $tipos_diccionario_str . "')
        GROUP BY tipo_aporte
        ORDER BY monto DESC
        ");
    $obj_2016 = $wpdb->get_results("
        SELECT tipo_aporte, SUM(monto) as 'monto'
        FROM wp_aportes_epm
        WHERE epm_year = '2016'
        AND estado = 1
        /* AND comuna LIKE '%$nombre_comuna%' -- TODO: revisar si no limita algun resultado el cambio */
        AND comuna LIKE '$nombre_comuna'
        GROUP BY tipo_aporte
        ORDER BY monto DESC
        ");
    $result = array(
        "2012" => $obj_2012,
        "2016" => $obj_2016,
    );
    return $result;
}

/**
 * [get_aportes_por_comuna_por_region_func]
 * @param  [param] $data [contiene e id de la comuna.]
 * @return [array]       [retorna el detalle de aportes por partido y por comuna.]
 */
function get_aportes_por_comuna_por_region_func($data)
{
    global $wpdb;
    $region_id = $data['id'];
    $result    = $wpdb->get_results("
        SELECT c.COMUNA_ID AS 'id',
            c.COMUNA_NOMBRE AS 'nombre',
            SUM(a.monto) AS 'aporte'
        FROM wp_provincia_epm AS p
        INNER JOIN wp_comuna_epm AS c
        ON c.COMUNA_PROVINCIA_ID = p.PROVINCIA_ID
        INNER JOIN wp_aportes_epm AS a
        ON a.comuna LIKE c.COMUNA_NOMBRE
        WHERE a.epm_year = '2016'
        AND a.estado = 1
        AND p.PROVINCIA_REGION_ID = $region_id
        GROUP BY c.COMUNA_NOMBRE
        ORDER BY aporte DESC
        "
    );
    return $result;
}

/**
 * [get_frontpage_indicadores obtiene las comunas correspondientes a una region desde la BD.]
 * @return [array]       [retorna el detalle de las comunas por region.]
 */
function get_frontpage_indicadores()
{
    global $wpdb;
    $comunas = $wpdb->get_row("
        SELECT count(DISTINCT comuna) AS count
        FROM wp_candidatos_epm
        WHERE epm_year = '2016'
        ", ARRAY_A);
    $candidatos = $wpdb->get_row("
        SELECT count(DISTINCT nombre_candidato) AS count
        FROM wp_candidatos_epm
        WHERE epm_year = '2016'
        ", ARRAY_A);
    $partidos = $wpdb->get_row("
        SELECT count(DISTINCT partido) AS count
        FROM wp_candidatos_epm
        WHERE epm_year = '2016'
        ", ARRAY_A);
    $result = [
        'comunas'    => $comunas['count'],
        'candidatos' => $candidatos['count'],
        'partidos'   => $partidos['count'],
    ];
    return $result;
}

/**
 * [get_frontpage_indicadores obtiene las comunas correspondientes a una region desde la BD.]
 * @return [array]       [retorna el detalle de las comunas por region.]
 */
function get_frontpage_top10comunas()
{
    global $wpdb;
    $comunas = $wpdb->get_results("
        SELECT c.COMUNA_ID,
            c.COMUNA_NOMBRE,
            electores
        FROM wp_electores_epm AS e
        LEFT JOIN wp_comuna_epm AS c
        ON e.comuna LIKE c.COMUNA_NOMBRE
        ORDER BY e.electores DESC
        LIMIT 10
        ", ARRAY_A);
    return $comunas;
}

function get_info_comuna_func($data)
{
    global $wpdb;
    $comuna_id = $data['id'];
    $comuna    = $wpdb->get_results("
        SELECT c.*
        FROM wp_comuna_epm AS c
        WHERE c.COMUNA_ID = $comuna_id
        ");
    return $comuna;
}