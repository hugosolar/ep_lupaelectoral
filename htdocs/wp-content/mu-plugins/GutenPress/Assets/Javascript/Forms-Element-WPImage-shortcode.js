// mediaControl for wp.media in widget
// uses jquery data attached to object
// 		uploader-title 	: Title for the wordpress media windows
//		button-text 	: Text for the button
//		targetid		: target ID of the hidden input that contains the attachment id of the selected picture
// @hugosolar
function bindEventWidgetImage(id) {
	var obj = jQuery('#'+id);
	//mediaControl.init();

	// Create the media frame.
	file_frame  = wp.media({
		title: obj.data( 'uploader_title' ),
		button: {
			text: obj.data( 'uploader_button_text' )
		},
		library: wp.media.query( { type: 'image' } ),
		multiple: false  // Set to true to allow multiple files to be selected
	});
	// When an image is selected, run a callback.
	file_frame.on( 'select', function() {
		//var img_obj = obj.data('targetimg');
		var img_html = obj.data('receiver_id');
		var img_id = obj.data('target_id');
		attachment = file_frame.state().get('selection').first().toJSON();
		if (attachment.sizes !== undefined)
			var img_selected = '<img src="'+attachment.sizes.thumbnail.url+'">';
		else
			var img_selected = '<img src="'+attachment.url+'" width="150" height="150" />';

		jQuery('#'+img_html).html(img_selected);

		jQuery('#'+img_id).val(attachment.id);
	});

	file_frame.open();

	return false;
}
;(function($){
	$.fn.gpWpImage = function(){
		this.each(function(){
			var $this = $(this),
				$mediaButton = $this.find('button.gp-wpimage-upload'),
				$deleteButton = $this.find('button.gp-wpimage-delete'),
				$receiver = $this.find('div.gp-wpimage-receiver'),
				$input = $this.find('input.gp-wpimage-field');
			$deleteButton.on('click', function(event){
				$receiver.find('img').fadeOut('normal', function(){
					$input.val('');
				});
				$(this).fadeOut('fast');
				event.preventDefault();
			});
		});
	};
	$('.gp-wpimage').gpWpImage();
})(jQuery);