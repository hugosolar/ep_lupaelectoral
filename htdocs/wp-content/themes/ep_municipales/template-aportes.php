<?php 
	/*
		Template name: Aportes
	*/
	get_header(); 
?>
<section class="main-content" ng-controller="aportesCtrl" ng-cloak>
	<div class="row inner-space">
		<div class="large-12 columns">
			<!-- <h2 class="section-title"><?php the_title(); ?></h2> -->
		</div>
	</div>
	<div class="row show-for-small-only" id="menuButtons">
		<div class="small-12 columns">
			<div class="row small-up-3">
				<div class="column">
					<button class="button secondary small expanded" scroll-to-item scroll-to="#listDetail">Gráfico</button>
				</div>
				<div class="column">
					<button class="button secondary small expanded" scroll-to-item scroll-to="#aportesRanking">Ranking</button>
				</div>
				<?php if (VISTA_COMPARATIVA): ?>
					<div class="column">
						<button class="button secondary small expanded" scroll-to-item scroll-to="#aportesComparativa">Comparativa</button>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="large-7 columns">
			<div class="white box">
				<div class="clearfix">
					<span class="bold-title float-left">Aportes a</span>
					<div class="float-right select-space">
						<select name="tipo_tipo" ng-model="elementType" ng-change="updateElementList()" id="tipo_tipo" class="select-box">
							<option value="Region" selected="selected">Regiones</option>
							<option value="Candidato">Candidatos</option>
						</select>
					</div>
				</div>
				<div class="switch-content" ng-switch on="elementType">
					<div class="region-filter clearfix" ng-switch-when="Candidato">
						<div class="row">
							<div class="large-6 columns">
								<span class="filter-text">Región</span>
								<select ng-init="selectRegion = regionList[0]" name="selectRegion" id="selectRegion" class="select-box" ng-change="updateComunas(selectRegion)" ng-model="selectRegion" ng-options="region[0].REGION_NOMBRE for region in regionList">
								</select>
							</div>
							<div class="large-6 columns">
								<span class="filter-text">Comuna</span>
								<select name="selectComuna" id="selectComuna" class="select-box" ng-change="updateCandidato(selectComuna)" ng-model="selectComuna" ng-options="comuna.COMUNA_NOMBRE for comuna in comunasList">
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="white box">
					<div ng-show="tableList.length == 0">
						<span class="mini-loader">Cargando datos...</span>
					</div>
					<div ng-show="tableList.length > 0">
						<table class="formated-tables">
							<thead>
								<tr>
									<th>{{elementType}}</th>
									<th>Monto</th>
								</tr>
							</thead>
							<tbody ng-switch on="elementType">

								<tr ng-class="{ 'active': value[0].REGION_ID == selectedIndex }" ng-repeat="value in tableList" ng-click="selectElement(value)" scroll-to-item scroll-to="#listDetail" ng-switch-when="Region">

									<td>{{value[0].REGION_NOMBRE}}</td>
									<td>{{value[1].monto | currency:"$":0}}</td>
								</tr>
								<tr ng-class="{ 'active': value.id == selectedIndex }" ng-repeat="value in tableList" ng-click="selectElement(value)" scroll-to-item scroll-to="#listDetail" ng-switch-when="Candidato">

									<td>{{value.nombre}}</td>
									<td>{{value.monto | currency:"$":0}}</td>
								</tr>
							</tbody>
						</table>
					</div>
			</div>
		</div>
		<div class="large-5 columns">
			<div class="white box" id="listDetail">
				<div class="show-for-small-only text-center"><button class="secondary small" scroll-to-item scroll-to="#menuButtons"><i class="material-icons">keyboard_arrow_up</i></button></div>
				<div ng-view></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="white box">
				<div class="top10tables" ng-switch on="elementType" id="aportesRanking">
					<div class="show-for-small-only text-center"><button class="secondary small" scroll-to-item scroll-to="#menuButtons"><i class="material-icons">keyboard_arrow_up</i></button></div>
					<div class="candidate-table" ng-switch-when="Candidato">
	
						<h3 class="main-title"><i class="material-icons">local_atm</i> <span>Top 10 aportes a candidatos a nivel nacional</span></h3>
						<div ng-controller="top10AportesCandidato">
							<div ng-show="elementList.length == 0">
								<span class="mini-loader">Cargando datos...</span>
							</div>
							<div ng-show="elementList.length > 0">
								<table class="formated-tables no-hover">
									<thead>
										<tr>
											<th ng-repeat="item in headersTable" class="{{item.class}}">{{item.value}}</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="value in elementList">
											<td ng-repeat="(key, data) in value" class="{{data.class}}">{{data.value}}</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="large-8 columns button-list">
									<div class="row large-up-3 clearfix">
										<div class="column">
											<button ng-click="spendLimit()" class="button" ng-class="{ 'active': 1 == candidateButton }">
												<i class="material-icons">warning</i><br>
												<span>% Límite al gasto</span>
											</button>
											<span class="has-tip" tooltips tooltip-template="Porcentaje de aportes con respecto al límite de gastos establecidos para la comuna."><i class="material-icons">live_help</i></span>
										</div>
										<div class="column">
											<button ng-click="totalElectorLimit()" class="button" ng-class="{ 'active': 2 == candidateButton }">
												<i class="material-icons">person</i><br>
												<span>Total aportes por elector</span>
											</button>

											<span class="has-tip" tooltips tooltip-template="Total del aporte con respecto a la cantidad de electores de la comuna"><i class="material-icons">live_help</i></span>
										</div>
										<div class="column">
											<button ng-click="totalLimit()" class="button" ng-class="{ 'active': 3 == candidateButton }"">
												<i class="material-icons">attach_money</i><br>
												<span>Total aportes</span>
											</button>
											<span class="has-tip" tooltips tooltip-template="Total de montos por aportes recibidos"><i class="material-icons">live_help</i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="region-table" ng-switch-when="Region">
						<h3 class="main-title"><i class="material-icons">local_atm</i> <span>Ranking de Aportes por Región</span></h3>
						<div ng-controller="top10AportesRegiones">
							<div ng-show="elementList.length == 0">
								<span class="mini-loader">Cargando datos...</span>
							</div>
							<div ng-show="elementList.length > 0">
								<table class="formated-tables no-hover">
									<thead>
										<tr>
											<th ng-repeat="item in headersTable" class="{{item.class}}">{{item.value}}</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="value in elementList">
											<td>{{$index+1}}</td>
											<td>{{value.nombre}}</td>
											<td>{{value.monto2016 | currency:"$":0}}</td>
											<td class="hide-for-small-only">{{value.monto2012 | currency:"$":0}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if (VISTA_COMPARATIVA): ?>
	<div class="row" ng-controller="aportesPastElection">
		<div class="large-12 columns" id="aportesComparativa">
			<div class="white box">
				<div class="show-for-small-only text-center"><button class="secondary small" scroll-to-item scroll-to="#menuButtons"><i class="material-icons">keyboard_arrow_up</i></button></div>
				<span class="bold-title">Comparativa año 2012</span>

				<gauge-chart id="aportesPasados" options="pastChart" height="100%" width="100%"></gauge-chart>

				<div id="leyendaComparativa"></div>
			</div>
		</div> 
	</div>
		<?php endif; ?>
</section>
<div ng-show="::false" class="big-loader">
    <div class="text-loader">
        Cargando...
        <div class="sk-folding-cube">
		  <div class="sk-cube1 sk-cube"></div>
		  <div class="sk-cube2 sk-cube"></div>
		  <div class="sk-cube4 sk-cube"></div>
		  <div class="sk-cube3 sk-cube"></div>
		</div>
    </div>
</div>
<?php get_footer(); ?>