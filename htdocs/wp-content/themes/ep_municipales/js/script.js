/*
* Convert a string into a slug
* Props to dense13.com
* http://dense13.com/blog/2009/05/03/converting-string-to-slug-javascript/
*/
function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();
  
  // remove accents, swap ñ for n, etc
  var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to   = "aaaaeeeeiiiioooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
}

jQuery(document).ready(function($){
	$(document).foundation();

    /* Home: Contador hasta el día de las eleccionas */
	if ($('#elecciones-countdown').length) {
        var delta = (new Date("2017/11/19 00:00:00")) - (new Date());
        if (delta > 0) {
            $('#elecciones-countdown').countdown('2017/11/19 00:00:00', function(event) {
                $(this).html(event.strftime('<span class="day module"><span class="day big">%D</span> <span class="day text">días</span></span> <span class="hour module"><span class="hour big">%H</span> <span class="hour text">horas</span></span> <span class="mminute module"><span class="minute big">%M</span> <span class="minute text">minutos</span></span> <span class="seconds module"><span class="seconds big">%S</span> <span class="seconds text">segundos</span></span>'));
            });
        } else {
            $('#elecciones-countdown-box').html('<h2>ELECCIONES PRESIDENCIALES Y PARLAMENTARIAS 19/11/2017</h2>');
        }
	}
    /*Evitar que se muestre pestaña cuando tiene un class "link"*/
    $('.tabs').find('.link a').on('click', function(e){
        e.preventDefault();
        var target = $(this).attr('href');
        window.location.href = target;
        return false;
    });
    /* Home: Buscador de comunas y candidatos */
    if ($('#ep_search').length) {
        var cache = {};
        var convert_person_type = function(text) {
            switch(text) {
                case 'senadores': return 'Senador'; break;
                case 'diputados': return 'Diputado'; break;
                case 'presidente': return 'Presidente'; break;
            }
        }
        var convert_icon = function(text) {
            switch(text) {
                case 'comuna': return 'place'; break;
                case 'senadores': 
                case 'diputados':
                case 'presidente':
                  return 'person'; break;
                case 'aportante': return 'local_atm'; break;
            }
        }
        var convert_url = function(text) {
            var uniq = (new Date()).getTime().toString();
            switch(text) {
                case 'comuna': return '/comuna/#/perfil/'; break;
                case 'senadores': 
                case 'diputados':
                case 'presidente': 
                    return '/perfil/#/candidato/'; break;
                case 'aportante': return '/aportante/?d='+uniq.substr(uniq.length-4)+'#/perfil/'; break;
            }
        }
         var convert_subtitle = function(text) {
            switch(text) {
                case 'comuna': return 'Comuna'; break;
                case 'senadores': 
                case 'diputados':
                case 'presidente': 
                    return convert_person_type(text); break;
                case 'aportante': return 'Aportante'; break;
            }
        }
        var ep_search_results = function(response, obj_results) {
            obj_results.removeClass('hide');
            if (response.length) {
                var html = '<h3>Resultados</h3>';
                for (var i = 0; i < response.length; i++) {
                    var _class = convert_icon(response[i]['tipo']);
                    var _wp_url = convert_url(response[i]['tipo']);
                    var _subtitle = '<span class="subtitle">'+convert_subtitle(response[i]['tipo'])+'</span>';
                    var _texto = '<a href="' + Ajax.site_url + _wp_url + string_to_slug(response[i]['id']) + '">' + response[i]['valor'] + '</a>';
                    html += '<div class="row result-item"><div class="small-1 columns"><i class="material-icons">' + _class + '</i></div><div class="small-10 columns end">' + _texto +_subtitle+'</div></div>';
                }
                obj_results.html(html);
            } else {
                obj_results.html('<div class="alert callout">No hay resultados</div>');
            }
        };
        $('.search-open').on('click', function(e){
            e.preventDefault();
            $(this).toggleClass('active');
            $('.float-search-box').toggleClass('visible');
            if ($('.float-search-box').hasClass('visible')) {
                $('.float-search-box').find('.search-input').focus();
            } else {
                $('.float-search-box').find('.search-results').html('').addClass('hide');
                $('.float-search-box').find('.search-input').val('');
            }
            return false;
        });
        $('.search-button').on('click', function(e){
            e.preventDefault();
            $(this).toggleClass('active');
            $('.float-search-box').toggleClass('visible');
            if ($('.float-search-box').hasClass('visible')) {
                $('.float-search-box').find('.search-input').focus();
            } else {
                $('.float-search-box').find('.search-results').html('').addClass('hide');
                $('.float-search-box').find('.search-input').val('');
            }
            return false;
        });
        $('.the-search').find('.search-input').on('input', function() {
            var minLength = 3,
                obj = $(this),
                $results = obj.parent().next('.search-results');
            $results.addClass('hide');
            var term = $.trim($(this).val());
            if (term.length >= minLength) {
                if (term in cache) {
                    ep_search_results(cache[term], $results);
                    return;
                }
                $.ajax({
                    url  : Ajax.site_url + '/wp-json/municipales/v1/buscar/' + escape(term),
                    type : 'GET',
                    success : function(response) {
                        cache[term] = response;
                        ep_search_results(response, $results);
                    }
                });
            }
        });
        $('.search-box .submit-search').click(function() {
            $('#ep_search').trigger('input');
        });
    }

    /* Home: Indicadores de cantidades de comunas, candidatos y partidos */
    // if ($('#indicators').length) {
    //     $.ajax({
    //         url  : Ajax.site_url + '/wp-json/municipales/v1/frontpage/indicadores',
    //         type : 'GET',
    //         success : function(response) {
    //             $('#indicators').find('.number.comunas').html(response.comunas);
    //             $('#indicators').find('.number.candidatos').html(response.candidatos);
    //             $('#indicators').find('.number.partidos').html(response.partidos);
    //         }
    //     });
    // }

    /* Home: Listado de comunas con más electores */
    // if ($('.map-cities .featured-cities').length) {
    //     $.ajax({
    //         url  : Ajax.site_url + '/wp-json/municipales/v1/frontpage/top10comunas',
    //         type : 'GET',
    //         success : function(response) {
    //             var html = '';
    //             for(var i = 0; i < response.length; i++) {
    //                 html += '<li><a href="' + Ajax.site_url + '/comuna/#/perfil/' + response[i].COMUNA_ID + '"> <i class="material-icons">my_location</i> ' + response[i].COMUNA_NOMBRE + '</a></li>';
    //             }
    //             $('.map-cities .featured-cities').html(html);
    //         }
    //     });
    // }

    /* Home: Selectore de Región/Comuna en mobile */
    if ($('.map-related .show-for-small-only').length) {
        $.ajax({
            url  : Ajax.site_url + '/wp-json/municipales/v1/regiones',
            type : 'GET',
            success : function(response) {
                for(var i = 0; i < response.length; i++) {
                    $('select[name=regiones]').append('<option value="' + response[i].REGION_ID + '">' + response[i].REGION_NOMBRE + '</option>');
                }
            }
        });
        $('select[name=regiones]').on('change', function() {
            if ($(this).val() == '') {
                return;
            }

            $.ajax({
                url  : Ajax.site_url + '/wp-json/municipales/v1/comunas/' + $(this).val(),
                type : 'GET',
                success : function(response) {
                    for(var i = 0; i < response.length; i++) {
                        $('select[name=comunas]').append('<option value="' + response[i].COMUNA_ID + '">' + response[i].COMUNA_NOMBRE + '</option>');
                    }
                }
            });
        });
        $('select[name=comunas]').on('change', function() {
            if ($(this).val() == '') {
                return;
            }

            window.location.href = Ajax.site_url + '/comuna/#/perfil/' + $(this).val();
        });
    }

    $('.mobile-nav-open').on('click',function(e) {
        e.preventDefault();
        $('.menu-mobile-container').toggleClass('hide');
        return false;
    });
    $('.menu-mobile-container').find('.close').on('click', function(e) {
        e.preventDefault();
        $(this).parent().addClass('hide');
        return false;
    });

    /* Fecha última actualización de archivo de denuncias */
    if ($('#denuncias_last_update').length) {
        $.ajax({
            url  : Ajax.site_url + '/wp-json/municipales/v1/denuncias/lastupdate',
            type : 'GET',
            success : function(response) {
                $('#denuncias_last_update').html(response);
            }
        });
    }

});