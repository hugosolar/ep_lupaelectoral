angular.module('aportante', ['ngRoute', 'ngSanitize','amChartsDirective', 'amChartsGauge', '720kb.tooltips', 'datatables', 'datatables.buttons' ])
.config(function($routeProvider, $locationProvider) {
	
	$routeProvider
	// .when('/',{
	// 	templateUrl: Localize.partials+'candidatos.html',
	// 	controller: 'defaultCtrl'
	// })
	.when('/perfil/:aportante', {
		templateUrl: Localize.partials+'aportante-detail.html',
		controller: 'aportantesCtrl'
		
	})
	.otherwise({
		redirectTo: '/aportantes/1'
	});
})

.controller('aportantesCtrl', function($scope, $http, $location, $routeParams, $timeout, aportanteServ){
	
	$scope.nombre_aportante = null;
	//Gráfico de barras
	
	$scope.$on('$routeChangeSuccess', function() {
		aportanteServ.getAporte($routeParams.aportante).then(function(response){
			if (response) {
				$scope.nombre_aportante = response[0].nombre_aportante;
				$scope.processingInfo = false;
			}
		});
	});
})
.controller('donationsCtrl', function($route, $scope, $http, $q, $location, $routeParams, aportanteServ, DTOptionsBuilder, DTColumnBuilder){
	
	$scope.tableView = Localize.partials+'/table-aportes.html';
	var vm = this;
	$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
        var defer = $q.defer();
        aportanteServ.getAporte($route.current.params.aportante).then(function(res){
        	if (res) {
		       	defer.resolve(res);
		       	$scope.processingInfo = false;
	       	}
       });

        return defer.promise;
       })
		.withLanguageSource(Localize.local_json+'/datatables.json')
		.withButtons([
           { 'extend': 'copyHtml5', text:'Copiar', copyTitle: 'Datos copiados'}, {'extend': 'print', 'text': 'Imprimir'}, 'excelHtml5','pdfHtml5', 'csvHtml5'
        ])
        .withOption('responsive', true);

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('nombre_aportante').withTitle('Nombre Aportante'),
        DTColumnBuilder.newColumn('monto').withTitle('Monto'),
        DTColumnBuilder.newColumn('nombre_candidato').withTitle('Candidato(a)'),
        DTColumnBuilder.newColumn('fecha').withTitle('Fecha'),
        DTColumnBuilder.newColumn('tipo_aporte').withTitle('Tipo Aporte')
    ];

})
.factory('aportanteServ', function($http, $q, $routeParams, $filter, currencyFilter){
	var self = this;
	self.aportes = [];
	self.donationList = [];

	var getAporte = function($id){
		var defer = $q.defer();
		$http.get(Localize.site_url+'/wp-json/municipales/v1/aportante/perfil/'+$id).then(function(response){
			self.aportes = response.data;
			defer.resolve(self.aportes);
		}, function(response){
			defer.reject(response);
		});
		return defer.promise;
	};
	return { getAporte : getAporte };
})
.directive('scrollToItem', function() {                                                      
	return {                                                                                 
	    restrict: 'A',                                                                       
	    scope: {                                                                             
	        scrollTo: "@"                                                                    
	    },                                                                                   
	    link: function(scope, $elm,attr) {                                                   

	        $elm.on('click', function() {                                                
	            jQuery('html,body').animate({scrollTop: jQuery(scope.scrollTo).offset().top }, "slow");
	        });                                                                              
	    }                                                                                    
}});