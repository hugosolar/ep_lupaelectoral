function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();
  
  // remove accents, swap ñ for n, etc
  var from = "ñàáäâèéëêìíïîòóöôùúüûñç·/_,:;";
  var to   = "naaaaeeeeiiiioooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
}
jQuery(document).ready(function($){
	var MAP_WIDTH  = 500;
	var MAP_HEIGHT = 800;
	// console.log(Map);

	var mapContainer = document.getElementById("map-target");
	var map = new Raphael(mapContainer, MAP_WIDTH, MAP_HEIGHT);

	var style = {
	  fill: "#ddd",
	  stroke: "#aaa",
	  "stroke-width": 1,
	  "stroke-linejoin": "round",
	  cursor: "pointer"
	};
	var animationSpeed = 500;
	var hoverStyle = {
	  fill: "#2D9546"
	}
	var textattr = { 
		'font-size': 10, 
		fill: '#000', 
		stroke: 'none', 
		'font-family': 'Arial,Helvetica,sans-serif', 
		'font-weight': 400 
	};

	var regions = {},
		svg_source = Map.svg;

	$('.hide-map-svg').load(svg_source, function(data) {
		
		$(data).find('path').each(function(index,el){
			var obj = $(this),
				id = string_to_slug(obj.attr('id'));

			regions[index] = map.path(obj.attr('d'));
			regions[index].attr(style);
			if (index == 0) {
				regions[index].animate(hoverStyle, animationSpeed);
				var pos2 = regions[index].getBBox(),
					pos_y = ( (pos2.y + pos2.y2)/2 * 0.6),
					pos_x = (pos2.x2 + pos2.x2)/2 + 30;
				$('.map-tooltip').html(obj.attr('id')).css({
					'position': 'absolute',
					top : pos_y + 'px',
					left : pos_x + 'px'
 				}).show();
 				$.ajax({
					url: Map.ajax_url,
					method: 'POST',
					data: {
						action: 'get_location',
						region: id
					},
    				beforeSend: function() {
    					$('.loader').show();
    				}
				}).done(function(data){
					$('.loader').hide();
    				$('.map-cities').html(data);
    			});
			} 
			regions[index].data('id',id);
			regions[index].data('name',obj.attr('id'));
			
			$(regions[index][0]).on('mouseover', function(e){
				regions[index].animate(hoverStyle, animationSpeed);
				var name = regions[index].data('name'),
					pos = $(regions[index][0]).position(),
					pos2 = regions[index].getBBox();

				// 800/1192 = 0.6711409395973155
				var pos_y = ( (pos2.y + pos2.y2)/2 * 0.6),
					pos_x = (pos2.x2 + pos2.x2)/2 + 30;

				$('.map-tooltip').html(name).css({
					'position': 'absolute',
					top : pos_y + 'px',
					left : pos_x + 'px'
 				}).show();
			});
			$(regions[index][0]).on('mouseout', function(e){
				var active = regions[index].data('active');
				if (!active) {
					regions[index].animate(style, animationSpeed);
					$('.map-tooltip').hide();
				} else {

				}
				
			});
			$('.map-result').hide();
			$(regions[index][0]).on('click', function(e){

				regions[index].data('active',true);
				regions[index].animate(hoverStyle, animationSpeed);


				var gindex = index;
				x = 0;
				for (var obj in regions) {
					if (x != gindex) {
						regions[x].data('active',false);
						regions[x].animate(style, animationSpeed);
					}
					x++;
				}
				
				var region = regions[index].data('id');
				$.ajax({
					url: Map.ajax_url,
					method: 'POST',
					data: {
						action: 'get_location',
						region: region
					},
    				beforeSend: function() {
    					$('.loader').show();
    				}
				}).done(function(data){
					$('.loader').hide();
    				$('.map-cities').html(data);
    			});
			});
		});
		$('.close-result').on('click', function(e) {
			e.preventDefault();
			$('.map-result').hide();
			return false;
		});
		$(data).find('tspan').each(function(index,el){
			var obj = $(this),
				x = obj.attr('x'),
				y = obj.attr('y');

			//regions[1].paper.text(x,y,obj.text()).attr(textattr);
		});
		// map.setViewBox(xpos, ypos, width, height, true);
		map.setViewBox(-150, 250, 800, 800, true);
	});
});