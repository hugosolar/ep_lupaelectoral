angular.module('perfil', ['ngRoute', 'ngSanitize','amChartsDirective', 'amChartsGauge', '720kb.tooltips', 'datatables', 'datatables.buttons' ])
.config(function($routeProvider, $locationProvider) {
	
	$routeProvider
	// .when('/',{
	// 	templateUrl: Localize.partials+'candidatos.html',
	// 	controller: 'defaultCtrl'
	// })
	.when('/candidato/:ID', {
		templateUrl: Localize.partials+'profile-detail.html',
		controller: 'perfilCtrl'
		
	})
	.otherwise({
		redirectTo: '/candidato/1'
	});
})

.controller('perfilCtrl', function($scope, $http, $location, $routeParams, $timeout, candidateServ){
	
	$scope.candidatosComunaData = [];
	$scope.limite = {};
	$scope.comuna = 0;
	//Gráfico de barras
	$scope.candidatosComuna = {
	  "type": "serial",
	  "theme": "light",
	  "marginRight": 70,
	  "dataProvider": $scope.candidatosComunaData,
	  "valueAxes": [{
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "Aportes en Millones"
	  }],
	  "startDuration": 1,
	  "graphs": [{
	    "balloonText": "<b>[[category]]: [[value]]</b>",
	    "fillColorsField": "color",
	    "fillAlphas": 0.9,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "monto"
	  }],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "candidato",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "labelRotation": 55,
		labelFunction : function(data, obj, axis) {
			// Se abrevian los nombres intentando (de manera incompleta) identificar ligaduras
			// para solo abreviar los nombres y no los apellidos.
			// Se asume que el primer texto siempre es nombre y se abrevia.
			var label     = obj.category,
				arr       = label.split(' '),
				ligaduras = ['DE', 'LA', 'LAS', 'DEL'];
			arr[0] = arr[0][0].toUpperCase() + '. ';
			for (var i=1; i <= (arr.length - 2 - 1); i++) {
				if (jQuery.inArray(arr[i+1].toUpperCase(), ligaduras) == -1) {
					arr[i] = arr[i][0].toUpperCase() + '. ';
				}
			}
			return arr.join(' ');
		}
	  },
	  "responsive": {
  		"enabled": true,
  		"rules": [{
			"maxWidth": 360,
			"overrides": {
	  			"categoryAxis": { "labelRotation": 90 }
  		 	}
  		}]
	  },
	  "export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
	$scope.gaugeChart = {
		type: "gauge",
		"theme": "light",
		  "axes": [ {
		    "axisThickness": 1,
		    "axisAlpha": 0.2,
		    "tickAlpha": 0.2,
		    "valueInterval": 20,
		    "bands": [ {
		      "color": "#84b761",
		      "endValue": 10,
		      "startValue": 0
		    }, {
		      "color": "#fdd400",
		      "endValue": 40,
		      "startValue": 10
		    }, {
		      "color": "#cc4748",
		      "endValue": 100,
		      "innerRadius": "95%",
		      "startValue": 40
		    } ],
		    "bottomText": "Aportes en millones",
		    "bottomTextYOffset": -20,
		    "endValue": 100
		  } ],
	  "arrows": [ { value: 0 }],
	  "export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
	$scope.$on('$routeChangeSuccess', function() {
		$scope.processingInfo = true;
		$scope.selectedButton = 1;
		$scope.selectedRoundButton = 1;
		$scope.selectedView = 1;
		$scope.limite = {};
		$scope.comuna = 0;
		$scope.aportes = [];
		$scope.candidato = {};
		$scope.aporte_partido = 0;
		$scope.enlaces = [];
		$scope.active_aporte_partido = Localize.aporte_partido;
		candidateServ.getCandidato($routeParams.ID).then(function(response){
			if (response) {
				$scope.candidato_tipo = response;
				$scope.comuna = response.comuna_id;
				$scope.tipo_eleccion = response.tipo_eleccion;
				$scope.candidato_circunscripcion = response.id_circunscripcion;
				$scope.candidato_distrito = response.id_distrito;
				$scope.texto_tipo_candidato = 'ninguno';
				switch(response.tipo_eleccion) {
					case 'presidente': $scope.texto_tipo_candidato = 'Presidente'; break;
					case 'diputados': $scope.texto_tipo_candidato = 'Diputado(a)'; break;
					case 'senadores': $scope.texto_tipo_candidato = 'Senador(a)'; break;
				}
				candidateServ.getCandidatoTipo(response.tipo_eleccion, $routeParams.ID).then(function(response){
					if (response) {
						$scope.id_candidato = $routeParams.ID;
						$scope.candidato = response.candidato;
						$scope.aportes = response.aportes;
						$scope.total_aporte = response.aportes.total;
						$scope.enlaces = response.enlaces;
						$scope.aporte_partido = ( response.candidato.aporte_partido_estimado / 1000000);
						$scope.eleccion = response.candidato.tipo_eleccion;
						$scope.limite = response.limite;
						$scope.limite_obj = null;
						$scope.esPresidente = ($scope.eleccion == 'presidente') ? true : false;
						if ($scope.eleccion == 'presidente') {
							$scope.limite = response.limites.primera_vuelta.limite;
							$scope.limite_obj = response.limites;
							$scope.aportes.total = response.limites.primera_vuelta.total;
						}
						
						$scope.hasDeclaration = response.candidato.url_declaracion;
						if ($scope.eleccion != 'presidente') {
							candidateServ.getCandidatosComuna($scope.comuna, $scope.eleccion).then(function(response){
								var candidatos = [];
								response.forEach(function(item, index){
									var candidato = {};
									candidato.candidato = item.nombre;
									candidato.monto = (item.monto / 1000000);
									if (item.id == $scope.id_candidato) {
										candidato.color = '#FFA500';
									} else {
										candidato.color = '#4B3985';
									}
									candidatos.push(candidato);
								});
								$scope.candidatosComunaData = candidatos;
								$scope.processingInfo = false;
							
							});
						} else {
							candidateServ.getCandidatosPres().then(function(response){
								var candidatos = [];
								response.forEach(function(item, index){
									var candidato = {};
									candidato.candidato = item.nombre;
									candidato.monto = (item.monto / 1000000);
									if (item.id == $scope.id_candidato) {
										candidato.color = '#FFA500';
									} else {
										candidato.color = '#4B3985';
									}
									candidatos.push(candidato);
								});
								$scope.candidatosComunaData = candidatos;
								$scope.processingInfo = false;
							
							});
						}
			
						//GAUGE GRAPHIC
						var limit = Math.floor($scope.limite / 1000000),
						monto = ($scope.aportes.total == 0) ? 0 : ($scope.aportes.total / 1000000);
						$scope.setLimits = function($limit, $value) {
							var total    = Math.round(parseInt(Math.max($limit, $value) * 1.20) / 10) * 10;
							var obj = {
								"total": 	total,
								"lim1": 	parseInt($limit * 0.8),
								"lim2": 	$limit,
								"interval": total / 5,
							}
							return obj;
						}
						$scope.gaugeGraphData = $scope.setLimits(limit,monto);
						$scope.axes = [{
						    "axisThickness": 1,
						    "axisAlpha": 0.2,
						    "tickAlpha": 0.2,
						    "valueInterval": $scope.gaugeGraphData.interval,
						    "bands": [ {
						      "color": "#84b761",
						      "endValue": $scope.gaugeGraphData.lim1,
						      "startValue": 0
						    }, {
						      "color": "#fdd400",
						      "endValue": $scope.gaugeGraphData.lim2,
						      "startValue": $scope.gaugeGraphData.lim1
						    }, {
						      "color": "#cc4748",
						      "endValue": $scope.gaugeGraphData.total,
						      "innerRadius": "95%",
						      "startValue": $scope.gaugeGraphData.lim2
						    } ],
						    "bottomText": "Aportes en millones",
						    "bottomTextYOffset": -20,
						    "endValue": $scope.gaugeGraphData.total
						  } ];
						  if (Localize.aporte_partido) {
							  $scope.arrow = [
							  	{ value : monto, color: '#022863' },
							  	{ value: $scope.aporte_partido, color: '#357c00' }
							  ];
							} else {
								$scope.arrow = [
							  	{ value : monto, color: '#022863' }
							  ];
							}
						  $scope.processingInfo = true;
						  $timeout(function(){
							  $scope.$broadcast('amChartsAlt.updateAxes', $scope.axes, 'gauge_amchart');
						  },1000);
						  $timeout(function(){
						  	  $scope.$broadcast('amChartsAlt.initArrow', $scope.arrow, 'gauge_amchart');
						  	  $scope.processingInfo = false;
						  },1500);

						$scope.tipoAportesChart = {
							"type": "pie",
							"theme": "light",
							"data": [
								{ title: 'Personas \ncon publicidad', value: $scope.aportes.publico },
								{ title: 'Propio', value: $scope.aportes.propio },
								{ title: 'Crédito', value: $scope.aportes.credito },
								{ title: 'Personas \nsin publicidad', value: $scope.aportes.total_spublicidad },
								{ title: 'Partido Político', value: $scope.aportes.partido },
							],
							"titleField": "title",
							"autoMargins": false,
							"marginTop": 0,
							"marginBottom": 0,
							"marginLeft": 0,
							"marginRight": 0,
							"pullOutRadius": 0,
							"valueField": "value",
							"labelRadius": 3,

							"radius": "32%",
							"innerRadius": "60%",
							"labelText": "[[title]]",
							"export": {
								"enabled": true,
								"menu": [ {
								    "class": "export-main",
								    "menu": [ {
								      "label": "Descargar imagen",
								      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
								    }, {
								      "label": "Descargar datos",
								      "menu": [ "CSV", "XLSX" ]
								    } ]
								  } ]
							}
						};
						$scope.donationLimitGraph = function() {
							$scope.selectedButton = 1;
							$scope.processingInfo = true;
							$scope.aportes.total = ($scope.esPresidente) ? $scope.limite_obj.primera_vuelta.total : $scope.limite;
							 $timeout(function(){
								  $scope.$broadcast('amChartsAlt.updateAxes', $scope.axes, 'gauge_amchart');
							  },1000);
							  $timeout(function(){
							  	  $scope.$broadcast('amChartsAlt.initArrow', $scope.arrow, 'gauge_amchart');
							  	  $scope.processingInfo = false;
							  },1500);
						}
						$scope.firstRound = function() {
							$scope.selectedRoundButton = 1;
							$scope.processingInfo = true;
							$scope.limite = $scope.limite_obj.primera_vuelta.limite;
							$scope.aportes.total = $scope.limite_obj.primera_vuelta.total;
							var limit = Math.floor($scope.limite_obj.primera_vuelta.limite / 1000000),
								monto = ($scope.limite_obj.primera_vuelta.total == 0) ? 0 : ($scope.limite_obj.primera_vuelta.total / 1000000);
							$scope.gaugeGraphData = $scope.setLimits(limit,monto);
							if (Localize.aporte_partido) {
								$scope.arrow = [
								  	{ value : ( $scope.limite_obj.primera_vuelta.total / 1000000), color: '#022863' },
								  	{ value: $scope.aporte_partido, color: '#357c00' }
								  ];
								} else {
									$scope.arrow = [
								  		{ value : ( $scope.limite_obj.primera_vuelta.total / 1000000), color: '#022863' }
								  	];
								}
							 $timeout(function(){
								  $scope.$broadcast('amChartsAlt.updateAxes', $scope.axes, 'gauge_amchart');
							  },1000);
							  $timeout(function(){
							  	  $scope.$broadcast('amChartsAlt.initArrow', $scope.arrow, 'gauge_amchart');
							  	  $scope.processingInfo = false;
							  },1500);
						}
						$scope.secondRound = function() {
							$scope.selectedRoundButton = 2;
							$scope.processingInfo = true;
							$scope.limite = $scope.limite_obj.segunda_vuelta.limite;
							$scope.aportes.total = $scope.limite_obj.segunda_vuelta.total;
							var limit = Math.floor($scope.limite_obj.segunda_vuelta.limite / 1000000),
								monto = ($scope.limite_obj.segunda_vuelta.total == 0) ? 0 : ($scope.limite_obj.segunda_vuelta.total / 1000000);
							$scope.gaugeGraphData = $scope.setLimits(limit,monto);
							if (Localize.aporte_partido) {
								$scope.arrow = [
								  	{ value : ($scope.limite_obj.segunda_vuelta.total / 1000000) , color: '#022863' },
								  	{ value: $scope.aporte_partido, color: '#357c00' }
								  ];
							} else {
								$scope.arrow = [
								  	{ value : ($scope.limite_obj.segunda_vuelta.total / 1000000) , color: '#022863' }
								  ];
							}
							 $timeout(function(){
								  $scope.$broadcast('amChartsAlt.updateAxes', $scope.axes, 'gauge_amchart');
							  },1000);
							  $timeout(function(){
							  	  $scope.$broadcast('amChartsAlt.initArrow', $scope.arrow, 'gauge_amchart');
							  	  $scope.processingInfo = false;
							  },1500);
						}
						$scope.typeGraph = function() {
							$scope.selectedButton = 2;
							$scope.aportes.total = $scope.total_aporte;
						}
						$scope.otherCandidates = function() {
							$scope.selectedButton = 3;
							$scope.aportes.total = $scope.total_aporte;
							$scope.processingInfo = true;

							$timeout(function(){
								$scope.$broadcast('amChartsAlt.updateData', $scope.candidatosComunaData, 'candidatosComuna_amchart');
								$scope.processingInfo = false;
							}, 1000);
						}
					}
				});
			}
		});
	});

})
.controller('donationsCtrl', function($route, $scope, $http, $q, $location, $routeParams, candidateServ, DTOptionsBuilder, DTColumnBuilder){
	$scope.esPresidente = false;
	$scope.tableView = Localize.partials+'/table-aportes.html';
	var vm = this;
	$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
        var defer = $q.defer();
        candidateServ.getCandidato($route.current.params.ID).then(function(res){
        	if (res) {
        		if (res.tipo_eleccion != 'presidente') {
        			$scope.dtColumns[4].visible = false;
        		}
		       	candidateServ.getDonations($route.current.params.ID, res.tipo_eleccion).then(function(response){ 
		       		defer.resolve(response);
		       	});
	       	}
       });

        return defer.promise;
       })
		//.withLanguageSource(Localize.local_json+'/datatables.json')
		.withButtons([
           { 'extend': 'copyHtml5', text:'Copiar', copyTitle: 'Datos copiados'}, {'extend': 'print', 'text': 'Imprimir'}, 'excelHtml5','pdfHtml5', 'csvHtml5'
        ])
        .withOption('responsive', true);

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('nombre_aportante').withTitle('Nombre Aportante'),
        DTColumnBuilder.newColumn('monto').withTitle('Monto'),
        DTColumnBuilder.newColumn('fecha').withTitle('Fecha'),
        DTColumnBuilder.newColumn('tipo_aporte').withTitle('Tipo Aporte'),
        DTColumnBuilder.newColumn('vuelta').withTitle('Vuelta')
    ];


})
.controller('spendCtrl', function($route, $scope, $http, $q, $location, $routeParams, candidateServ, DTOptionsBuilder, DTColumnBuilder){
	
	$scope.tableView = Localize.partials+'/table-aportes.html';
	var vm = this;
	$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
        var defer = $q.defer();
       	candidateServ.getSpends($route.current.params.ID).then(function(response){ 
       		defer.resolve(response);
       	});

        return defer.promise;
       })
		.withLanguageSource(Localize.local_json+'/datatables.json')
		.withButtons([
           { 'extend': 'copyHtml5', text:'Copiar', copyTitle: 'Datos copiados'}, {'extend': 'print', 'text': 'Imprimir'}, 'excelHtml5','pdfHtml5', 'csvHtml5'
        ])
        .withOption('responsive', true);

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('nombre_proveedor').withTitle('Nombre Proveedor'),
        DTColumnBuilder.newColumn('monto').withTitle('Monto'),
        DTColumnBuilder.newColumn('descripcion').withTitle('Descripción'),
        DTColumnBuilder.newColumn('glosa').withTitle('Glosa'),
        DTColumnBuilder.newColumn('fecha').withTitle('Fecha')
    ];

})
.factory('candidateServ', function($http, $q, $routeParams, $filter, currencyFilter){
	var self = this;
	self.candidato = [];
	self.donationList = [];

	var getCandidato = function($id){
		var defer = $q.defer();
		$http.get(Localize.site_url+'/wp-json/municipales/v1/candidato/'+$id).then(function(response){
			self.candidato = response.data;
			defer.resolve(self.candidato);
		}, function(response){
			defer.reject(response);
		});
		return defer.promise;
	};
	var getCandidatoTipo = function($tipo, $id){
		var defer = $q.defer();
		$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/'+$tipo+'/candidato/'+$id).then(function(response){
			self.candidato_tipo = response.data;
			defer.resolve(self.candidato_tipo);
		}, function(response){
			defer.reject(response);
		});
		return defer.promise;
	};

	var getCandidatosComuna = function($comuna_id, $tipo) {
		$defer = $q.defer();
		var candidatos = [];
		 $http.get(Localize.site_url+'/wp-json/municipales/v1/candidatos/'+$tipo+'/'+$comuna_id).then(function(res){
			var urlCalls = [];
			angular.forEach(res.data,function(data) {
				urlCalls.push($http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/'+$tipo+'/candidato/'+data.id));
			});
			$q.all(urlCalls).then(function(results){
				results.forEach(function(response, index){
					var candidato = {};
					candidato.id = response.data.candidato.id;
					candidato.nombre = response.data.candidato.nombre_candidato;
					candidato.monto = response.data.aportes.total;
					candidato.partido = response.data.candidato.partido;
					candidato.pacto = response.data.candidato.pacto;
					candidato.comuna = response.data.candidato.comuna;
					candidato.region = response.data.candidato.region;
					candidato.limite = response.data.limite;
					candidato.tipo_aportes = {
						propio: response.data.aportes.propio,
						publico: response.data.aportes.publico,
						spublicidad:  response.data.aportes.total_spublicidad,
					};
					candidatos.push(candidato);
				} );
				$defer.resolve(candidatos);
			})

		});
		 return $defer.promise;
	}
	var getCandidatosPres = function() {
		$defer = $q.defer();
		var candidatos = [];
		 $http.get(Localize.site_url+'/wp-json/municipales/v1/candidatos/presidente').then(function(res){
			var urlCalls = [];
			angular.forEach(res.data,function(data) {
				urlCalls.push($http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/presidente/candidato/'+data.id));
			});
			$q.all(urlCalls).then(function(results){
				results.forEach(function(response, index){
					var candidato = {};
					candidato.id = response.data.candidato.id;
					candidato.nombre = response.data.candidato.nombre_candidato;
					candidato.monto = response.data.aportes.total;
					candidato.partido = response.data.candidato.partido;
					candidato.pacto = response.data.candidato.pacto;
					candidato.comuna = response.data.candidato.comuna;
					candidato.region = response.data.candidato.region;
					candidato.limite = response.data.limite;
					candidato.tipo_aportes = {
						propio: response.data.aportes.propio,
						publico: response.data.aportes.publico,
						spublicidad:  response.data.aportes.total_spublicidad,
					};
					candidatos.push(candidato);
				} );
				$defer.resolve(candidatos);
			})

		});
		 return $defer.promise;
	}
	var getDonations = function($id, $tipo){
		var defer = $q.defer();
		$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/'+$tipo+'/nomina/'+$id).then(function(response){
			//self.donationList = response.data;
			var donations = [];
			response.data.forEach(function(item, index){
				var donation = {};
				if (item.vuelta != null) {
					switch (item.vuelta) {
						case '1': donation.vuelta = 'Primera'; break;
						case '2': donation.vuelta = 'Segunda'; break;
						default:
							donation.vuelta = 'No aplica';
							break;
					}
				} else {
					donation.vuelta = 'No Aplica';
				}
				if (donation.nombre_aportante = '') {
					donation.nombre_aportante = 'NN';
				} else {
					donation.nombre_aportante = item.nombre_aportante;
				}
				donation.monto = $filter('currency')(item.monto,'$',0);
				donation.fecha = item.fecha;
				switch (item.tipo_aporte) {
					case 'PÚBLICO':
					case 'PUBLICO':
					case 'CON PUBLICIDAD':
						donation.tipo_aporte = 'Personas con publicidad';
						break;
					case 'NO PÚBLICO':
					case 'NO PUBLICO':
						donation.tipo_aporte = 'Personas sin publicidad';
						break;
					case 'CRÉDITO':
					case 'CREDITO':
						donation.tipo_aporte = 'Crédito';
						break;
					case 'PROPIO':
						donation.tipo_aporte = 'Propio';
						break;
					case 'PARTIDO POLÍTICO':
					case 'PARTIDO POLITICO':
						donation.tipo_aporte = 'Partido Político';
						break;
					default:
						donation.tipo_aporte = '';
						break;
				}
				donations.push(donation);
			});
			self.donationList = donations;

			defer.resolve(donations);
		}, function(response){
			defer.reject(response);
		});
		return defer.promise;
	};
	var getSpends = function($id){
		var defer = $q.defer();
		$http.get(Localize.site_url+'/wp-json/municipales/v1/gastos/nomina/'+$id).then(function(response){
			//self.donationList = response.data
			var spends = [];
			response.data.forEach(function(item, index){
				var item_spend = {};
				if (item_spend.nombre_proveedor = '') {
					item_spend.nombre_proveedor = 'NN';
				} else {
					item_spend.nombre_proveedor = item.nombre_proveedor;
				}
				item_spend.monto = $filter('currency')(item.monto_documento,'$',0);
				item_spend.fecha = item.fecha_documento;
				item_spend.descripcion = item.descripcion_documento;
				item_spend.glosa= item.glosa_documento;
				spends.push(item_spend);
			});
			self.donationList = spends;

			defer.resolve(spends);
		}, function(response){
			defer.reject(response);
		});
		return defer.promise;
	};
	return { getCandidato : getCandidato, getCandidatoTipo: getCandidatoTipo, getDonations: getDonations, getCandidatosComuna: getCandidatosComuna, getCandidatosPres: getCandidatosPres, getSpends: getSpends };
})
.directive('scrollToItem', function() {                                                      
	return {                                                                                 
	    restrict: 'A',                                                                       
	    scope: {                                                                             
	        scrollTo: "@"                                                                    
	    },                                                                                   
	    link: function(scope, $elm,attr) {                                                   

	        $elm.on('click', function() {                                                
	            jQuery('html,body').animate({scrollTop: jQuery(scope.scrollTo).offset().top }, "slow");
	        });                                                                              
	    }                                                                                    
}});