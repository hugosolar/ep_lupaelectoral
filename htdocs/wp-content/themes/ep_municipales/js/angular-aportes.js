angular.module('aportes', ['ngRoute', 'ngSanitize','amChartsDirective', 'amChartsGauge', '720kb.tooltips' ])
.config(function($routeProvider, $locationProvider) {
	
	$routeProvider
	// .when('/',{
	// 	templateUrl: Localize.partials+'region.html',
	// 	controller: 'defaultCtrl'
	// })
	.when('/region/:ID', {
		templateUrl: Localize.partials+'region-detail.html',
		controller: 'regionDetail',
		resolve: {
			comunasList : function($routeParams, comunasServ) {
				return comunasServ.getComunas();
			}
		}
	})
	.when('/candidato/:ID', {
		templateUrl: Localize.partials+'candidate-detail.html',
		controller: 'candidateDetail'
	})
	.otherwise({
		redirectTo: '/'
	});
})
.controller('aportesCtrl', function($scope, $http, $location, candidates, regions, candidates){
	$scope.selectedIndex = 15;
	$scope.elementType = 'Region';
	$scope.tableList = [];
	$scope.selectComuna = [];

	$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/regiones').then(function(response){
		$scope.tableList = response.data;
		$scope.regionList = response.data;
		regions.current = response.data[0];
		return $http.get(Localize.site_url+'/wp-json/municipales/v1/comunas/'+$scope.regionList[0][0].REGION_ID).then(function(comunas){
			$scope.comunasList = comunas.data;
			$scope.selectComuna = comunas.data[0];
			
		});
	});
	
	$location.path('/region/'+$scope.selectedIndex);
	
	$scope.selectElement = function($element) {
		if ($scope.elementType == 'Region') {
			$scope.selectedIndex = $element[0].REGION_ID;
			$scope.currentSelected = $element;
			regions.setCurrent($element);
			$location.path('/region/'+$element[0].REGION_ID);
		} else if ($scope.elementType == 'Candidato') {
			$scope.selectedIndex = $element.id;
			$scope.currentSelected = $element;
			candidates.setCurrent($element);
			$location.path('/candidato/'+$element.id);
		}
	}
	$scope.updateComunas = function($region) {
		$http.get(Localize.site_url+'/wp-json/municipales/v1/comunas/'+$region[0].REGION_ID).success(function(res){
			$scope.comunasList = res;
			$scope.selectComuna = res[0];
			$scope.updateCandidato(res[0]);
		});
	}
	$scope.updateCandidato = function($comuna) {
		candidates.getFromComuna($comuna.COMUNA_ID).then(function(response){
			$scope.tableList = response;
			candidates.setList(response);
			candidates.setCurrent(response[0]);
			$scope.currentSelected = response[0];
			$scope.selectedIndex = response[0].id;
		});
	}
	$scope.updateElementList = function($element) {
		if ( $scope.elementType == 'Candidato') {
			candidates.getFromComuna($scope.selectComuna.COMUNA_ID).then( function(response) {
				$scope.tableList = response;
				candidates.setCurrent(response[0]);
				$scope.currentSelected = response[0];
				$scope.selectedIndex = response[0].id;
				//$location.path('/candidato/'+response[0].id);
			});

		} else if ( $scope.elementType == 'Region') {
			$scope.tableList = $scope.regionList;
			var current = $scope.regionList[0][0];
			$scope.selectedIndex = current.REGION_ID;
			$scope.currentSelected = current;
			regions.setCurrent($scope.tableList[0]);
			$location.path('/region/'+current.REGION_ID);
		}
	}
	// $scope.$watch( 'selectComuna', function($newValue, $oldValue) {
	// 	//http://municipales.lo/wp-json/municipales/v1/candidatos/5103
	// 	if ($newValue) {
	// 		console.log($newValue.COMUNA_ID);
	// 		$http.get(Localize.site_url+'/wp-json/municipales/v1/candidatos/'+$newValue.COMUNA_ID).success(function(res){
	// 			$scope.tableList = res;
	// 			$location.path('/candidato/'+res[0].id);
	// 		});
	// 	}
	// } );
}).controller('regionDetail', function($scope, $timeout, $http, $location,$routeParams, $timeout, $filter, currencyFilter, $q, regions, comunasServ, comunasList, comunasDonation){
	$scope.selectedButton = 1;
	if (regions.current.length){
		$scope.region = regions.getCurrent();
	} else {
		$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/regiones').then(function(regiones){
			$scope.region = regiones.data[0];
			regions.setCurrent($scope.region);
		});
	}
	var comunas = [];
	if (comunasList) {
		var otros 	= {'title': 'Otros', 'value': 0},
			count 	= 0;
		comunasList.forEach( function(item){
			if (count >= 5) {
				otros.value += parseInt(item.aporte);
			} else {
				var currentLocation = {};
				currentLocation.title = item.nombre;
				currentLocation.value = item.aporte;
				comunas.push(currentLocation);
				count++;
			}
		});
		if (count >= 5) {
			comunas.push(otros);
		}

		$scope.chartData = comunas;
		if (!$scope.chartData.length) {
			$scope.chartData = [{
				title: 'cargando datos...',
				value: 10
			}];
		}
		$scope.pieChart = {
					"type": "pie",
					"theme": "light",
					"data": $scope.chartData,
					"titleField": "title",
					"autoMargins": false,
					"marginTop": 0,
					"marginBottom": 0,
					"marginLeft": 0,
					"marginRight": 0,
					"pullOutRadius": 0,
					"valueField": "value",
					"percentPrecision": 1,
					"labelRadius": 3,
					"radius": "32%",
					"innerRadius": "60%",
					"labelText": "[[title]]",
					"export": {
						"enabled": true,
						"menu": [ {
						    "class": "export-main",
						    "menu": [ {
						      "label": "Descargar imagen",
						      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
						    }, {
						      "label": "Descargar datos",
						      "menu": [ "CSV", "XLSX" ]
						    } ]
						  } ]
					}
				};
	}
	$scope.total_2012 = '';
	$scope.donationTypeGraph = function($year) {
		$scope.selectedButton = ($year == 2016) ? 3 : 4;
		$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/region/'+$scope.region[0].REGION_ID).then(function(response){
			var info_aportes = response.data.aportes_por_tipo_aporte[$year];
			var tipo_aportes = [
				{ title: 'Personas con publicidad', value: info_aportes['CON PUBLICIDAD'] },
				{ title: 'Propio', value: info_aportes['PROPIO'] },
				{ title: 'Personas sin publicidad', value: info_aportes['SIN PUBLICIDAD'] },
				{ title: 'Partido Político', value: info_aportes['PARTIDO POLÍTICO'] },
			];
			$scope.chartData = tipo_aportes;
			$scope.total_2012 = response.data.total_aportes_2012;
			$scope.processingInfo = true;
			$timeout(function() {
				$scope.$broadcast('amCharts.updateData', tipo_aportes, 'Comunas');
				$scope.processingInfo = false;
			},1500);
		});
	}
	$scope.partyGraph = function() {
		$scope.selectedButton = 2;
		$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/region/'+$scope.region[0].REGION_ID).then(function(response){
			var info_aportes = response.data.aportes_por_partido;
			var tipo_aportes = [];
			for (var key in info_aportes) {
			  if (info_aportes.hasOwnProperty(key)) {
			  	var item = {};
			  	item.title = key;
			  	item.value = info_aportes[key];
			    tipo_aportes.push(item);
			  }
			}
			$scope.chartData = tipo_aportes;
			
			$scope.$broadcast('amCharts.updateData', $scope.chartData, 'Comunas');
		});
	}
	$scope.locationGraph = function() {
		$scope.selectedButton = 1;
		var comunas = [],
			otros 	= {'title': 'Otros', 'value': 0},
			count 	= 0;
		comunasList.forEach( function(item){
			if (count >= 5) {
				otros.value += parseInt(item.aporte);
			} else {
				var currentLocation = {};
				currentLocation.title = item.nombre;
				currentLocation.value = item.aporte;
				comunas.push(currentLocation);
				count++;
			}
		});
		if (count >= 5) {
			comunas.push(otros);
		}
		$scope.chartData = comunas;
		$scope.$broadcast('amCharts.updateData', $scope.chartData, 'Comunas');
	}


}).controller('candidateDetail', function($scope, $http, $log, $location, $window, $routeParams, candidates){
	
	$scope.candidate = candidates.getCurrent();
	$scope.limite = $scope.candidate.limite;
	$scope.selectedButton = 1;
	// console.log($scope.candidate);
	var limit = Math.floor($scope.candidate.limite / 1000000),
		monto = ($scope.candidate.monto == 0) ? 0 : ($scope.candidate.monto / 1000000);
	
	$scope.setLimits = function($limit, $value) {
		var min_interval = $limit >= 10 ? 10 : 3;
		var total    = Math.round(parseInt(Math.max($limit, $value) * 1.20) / min_interval) * min_interval;
		var obj = {
			"total": 	total,
			"lim1": 	parseInt($limit * 0.8),
			"lim2": 	$limit,
			"interval": total / 5,
		}
		return obj;
	}
	var graph = $scope.setLimits(limit,monto);

	$scope.gaugeChart = {
		type: "gauge",
		"theme": "light",
		  "axes": [ {
		    "axisThickness": 1,
		    "axisAlpha": 0.2,
		    "tickAlpha": 0.2,
		    "valueInterval": graph.interval,
		    "bands": [ {
		      "color": "#84b761",
		      "endValue": graph.lim1,
		      "startValue": 0
		    }, {
		      "color": "#fdd400",
		      "endValue": graph.lim2,
		      "startValue": graph.lim1
		    }, {
		      "color": "#cc4748",
		      "endValue": graph.total,
		      "innerRadius": "95%",
		      "startValue": graph.lim2
		    } ],
		    "bottomText": "Aportes en millones",
		    "bottomTextYOffset": -20,
		    "endValue": graph.total
		  } ],
	  "arrows": [ {
	  	value: monto
	  } ],
	 "export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};

	var info_aportes = $scope.candidate.tipo_aportes;
	var aportes = [
		{ title: 'Personas con publicidad', value: info_aportes.publico },
		{ title: 'Propio', value: info_aportes.propio },
		{ title: 'Personas sin publicidad', value: info_aportes.spublicidad },
		{ title: 'Partido Político', value: info_aportes.partido },
	];
	$scope.aportesChart = {
		"type": "pie",
		"theme": "light",
		"data": aportes,
		"titleField": "title",
		"autoMargins": false,
		"marginTop": 0,
		"marginBottom": 0,
		"marginLeft": 0,
		"marginRight": 0,
		"pullOutRadius": 0,
		"valueField": "value",
		"labelRadius": 3,
		"percentPrecision": 1,
		"radius": "32%",
		"innerRadius": "60%",
		"labelText": "[[title]]",
		"export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
	$scope.donationLimitGraph = function() {
		$scope.selectedButton = 1;
	}
	$scope.typeGraph = function() {
		$scope.selectedButton = 2;
	}
	$scope.userProfileRedirect = function($id) {
		$window.location.href  = Localize.site_url+'/perfil/#/candidato/'+$id;
	}
	$scope.comunaProfileRedirect = function($id) {
		$window.location.href  = Localize.site_url+'/comuna/#/perfil/'+$id;
	}

})
.controller('top10AportesRegiones', function($scope, $http, $location, $routeParams){
	$scope.selectedButton = 1;
	$scope.headersTable = [
		{ 'value': 'Nº', 'class': ''},
		{ 'value': 'Región', 'class': ''},
		{ 'value': '▼ Monto 2016', 'class': ''},
		{ 'value': 'Monto 2012', 'class': 'hide-for-small-only'}
	];
	$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/top10regiones').then(function(response){
		var elementList = response.data,
			returnList = [];
		elementList.forEach( function(data, index) {
			var item = {};
			item.nombre = data.aporte.region;
			item.monto2012 = data.aporte.monto_2012;
			item.monto2016 = data.aporte.monto_2016;
			returnList.push(item);
		});
		$scope.elementList = returnList;
	});	
})
.controller('top10AportesCandidato', function($scope, $http, $location, $routeParams, $filter, currencyFilter){
	$scope.candidateButton = 1;
	$scope.headersTable = ['Nº','Región', 'Monto 2012', 'Monto 2016' ];

	$scope.result_percent = [];
	$scope.result_electores = [];
	$scope.result_aportes = [];

	$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/top10limitecomuna').then(function(response){
		$scope.headersTable = [
			{'value': 'Nº', 'class': ''},
			{'value': 'Nombre candidato', 'class': ''},
			{'value': 'Comuna', 'class': 'hide-for-small-only'},
			{'value': 'Total Aportes', 'class': 'hide-for-small-only'},
			{'value': '▼ % Límite al Gasto', 'class': ''}
		];
		var elementList = response.data,
			returnList = [];
		elementList.forEach( function(data, index) {
			var item = {};
			item.id = {'class': '', 'value': index+1 };
			item.nombre = {'class': '', 'value': data.nombre };
			item.comuna = {'class': 'hide-for-small-only', 'value': data.comuna };
			item.monto = {'class': 'hide-for-small-only', 'value': $filter('currency')(data.monto,'$',0) };
			item.porcentaje_comuna = {'class': '', 'value': (Math.round(data.porcentaje * 10) / 10)+'%' };
			returnList.push(item);
		});
		$scope.result_percent = returnList;
		$scope.elementList = returnList;
		$scope.candidateButton = 1;
	});

	$scope.spendLimit = function() {
		$scope.candidateButton = 1;
		$scope.headersTable = [
			{'value': 'Nº', 'class': ''},
			{'value': 'Nombre candidato', 'class': ''},
			{'value': 'Comuna', 'class': 'hide-for-small-only'},
			{'value': 'Total Aportes', 'class': 'hide-for-small-only'},
			{'value': '▼ % Límite al Gasto', 'class': ''}
		];
		$scope.elementList = $scope.result_percent;
	}
	$scope.totalElectorLimit = function() {
		$scope.candidateButton = 2;
		$scope.headersTable = [
			{'value': 'Nº', 'class': ''},
			{'value': 'Nombre candidato', 'class': ''},
			{'value': 'Comuna', 'class': 'hide-for-small-only'},
			{'value': '▼ Total aportes/elector', 'class': ''},
			{'value': 'Cantidad electores', 'class': 'hide-for-small-only'}
		];
		if ($scope.result_electores.length) {
			$scope.elementList = $scope.result_electores;
		} else {
			$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/top10montopadron').then(function(response){
				var elementList = response.data,
					returnList = [];
				elementList.forEach( function(data, index) {
					var item = {};
					item.id = {'class': '', 'value': index+1 };
					item.nombre = {'class': '', 'value': data.nombre };
					item.comuna = {'class': 'hide-for-small-only', 'value': data.comuna };
					item.monto_elector = {'class': '', 'value': $filter('currency')(data.monto_x_elector,'$',0) };
					item.electores = {'class': 'hide-for-small-only', 'value': $filter('currency')(data.cantidad_electores,'',0) };
					returnList.push(item);
				});
				$scope.result_electores = returnList;
				$scope.elementList = returnList;
			});
		}
	}
	$scope.totalLimit = function() {
		$scope.candidateButton = 3;
		$scope.headersTable = [
			{'value': 'Nº', 'class': ''},
			{'value': 'Nombre candidato', 'class': ''},
			{'value': '▼ Total Aportes', 'class': ''}
		];
		if ($scope.result_aportes.length) {
			$scope.elementList = $scope.result_aportes;
		} else {
			$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/top10monto').then(function(response){
				var elementList = response.data,
					returnList = [];
				elementList.forEach( function(data, index) {
					var item = {};
					item.id = {'class': '', 'value': index+1 };
					item.nombre = {'class': '', 'value': data.nombre_candidato };
					item.monto = {'class': '', 'value': $filter('currency')(data.monto,'$',0) };
					returnList.push(item);
				});
				$scope.result_aportes = returnList;
				$scope.elementList = returnList;
			});
		}
	}
})
.controller('aportesPastElection',function($scope, $http, $location,$routeParams){
	$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/regiones/comparativa2012').then(function(response){
		var chartData = [],
			data2012 = response.data[2012];
			data2016 = response.data[2016];

		data2012.forEach(function(element,index){
			var region = {};
			region.region = element.romano;
			region.year2012 = parseInt(element.monto);
			if ( data2016[index] != undefined ) {
				region.year2016 = parseInt(data2016[index].monto);
			} else {
				region.year2016 = 0;
			}
			chartData.push(region);
		});
		$scope.$broadcast('amChartsAlt.updateData', chartData, 'aportesPasados_amchart');
	});
	$scope.pastChart = {
		"theme": "none",
	    "type": "serial",
	    "dataProvider": [],
	    "startDuration": 1,
	    "graphs": [{
	        "balloonText": "Aportes en [[category]] región (2012): <b>[[value]]</b>",
	        "fillAlphas": 0.9,
	        "lineAlpha": 0.2,
	        "title": "2012",
	        "type": "column",
	        "fillColors": "#00A19B",
	        "valueField": "year2012"
	    }, {
	        "balloonText": "Aportes en [[category]] región (2016): <b>[[value]]</b>",
	        "fillAlphas": 0.9,
	        "lineAlpha": 0.2,
	        "title": "2016",
	        "fillColors": "#4B3985",
	        "type": "column",
	        "clustered":false,
	        "columnWidth":0.5,
	        "valueField": "year2016"
	    }],
	    legend: {
		  divId: "leyendaComparativa",
		  data: [
		  {
		  	title: 'Aportes 2012',
		  	color: '#00A19B'
		  },
		  {
		  	title: 'Aportes 2016',
		  	color: '#4B3985'
		  }
		  ]
		},
	    "plotAreaFillAlphas": 0.1,
	    "categoryField": "region",
	    "categoryAxis": {
	        "gridPosition": "start"
	    },
	    "export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
})
.factory('candidates', function($q, $http, $location){
	var candidates = {};

	candidates.list = [];
	candidates.current = {};

	candidates.setCurrent = function(candidate) {
		candidates.current = candidate;
	}
	candidates.getCurrent = function() {
			return candidates.current;
	}
	candidates.getFromComuna = function($comuna_id) {
		$defer = $q.defer();
		var candidatos = [];
		 $http.get(Localize.site_url+'/wp-json/municipales/v1/candidatos/'+$comuna_id).then(function(res){
			var urlCalls = [];
			angular.forEach(res.data,function(data) {
				urlCalls.push($http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/candidato/'+data.id));
			});
			$q.all(urlCalls).then(function(results){
				results.forEach(function(response, index){
					var candidato = {};
					candidato.id = response.data.candidato.id;
					candidato.nombre = response.data.candidato.nombre_candidato;
					candidato.monto = response.data.aportes.total;
					candidato.partido = response.data.candidato.partido;
					candidato.pacto = response.data.candidato.pacto;
					candidato.comuna = response.data.candidato.comuna;
					candidato.comuna_id = response.data.comuna.COMUNA_ID;
					candidato.region = response.data.candidato.region;
					candidato.limite = response.data.limite;
					candidato.tipo_aportes = {
						propio: response.data.aportes.propio,
						publico: response.data.aportes.publico,
						spublicidad:  response.data.aportes.total_spublicidad,
						partido:  response.data.aportes.partido,
					};
					candidatos.push(candidato);
				} );
				candidates.setCurrent(candidatos[0]);
				candidates.setList(candidatos);
				$defer.resolve(candidatos);
				$location.path('/candidato/'+candidatos[0].id);
			})

		});
		 return $defer.promise;
	}
	candidates.setList = function(list) {
		candidates.list = list;
	}
	candidates.getList = function() {
		return candidates.list;
	}
	return candidates;
})
.factory('regions', function(){
	var regions = {};

	regions.list = [];
	regions.current = {};

	regions.setCurrent = function(region) {
		regions.current = region;
	}
	regions.getCurrent = function() {
		return regions.current;
	}
	regions.setList = function(list) {
		regions.list = list;
	}
	return regions;
})
/*
	Servicio para retornar las comunas de una region definida
	La promesa se resuelve en el route
*/
.factory('comunasServ', function($http, $q, $routeParams, regions, comunasDonation){
	var comunas = [];
	var getComunas = function(){
		var defer = $q.defer();
		var id = (regions.current[0]) ? regions.current[0].REGION_ID : 15;
		$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/comunas/region/'+id).then(function(response){
			comunas = response.data;
			defer.resolve(comunas);
		}, function(response){
			defer.reject(response);
		});
		return defer.promise;
	};
	return { getComunas : getComunas };
})
.factory('comunasDonation', function($http, $q, $routeParams, regions){
	var totalDonation = function($id_comuna) {
		var defer = $q.defer();
		$http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/total/comuna/'+$id_comuna).then(function(response){
			defer.resolve(response.data);
		}, function(response){
			defer.reject(response);
		});
		return defer.promise;
	}
	return { totalDonation : totalDonation };
})
.directive('scrollToItem', function() {                                                      
	return {                                                                                 
	    restrict: 'A',                                                                       
	    scope: {                                                                             
	        scrollTo: "@"                                                                    
	    },                                                                                   
	    link: function(scope, $elm,attr) {                                                   

	        $elm.on('click', function() {                                                
	            jQuery('html,body').animate({scrollTop: jQuery(scope.scrollTo).offset().top }, "slow");
	        });                                                                              
	    }                                                                                    
}});
