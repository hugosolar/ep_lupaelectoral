angular.module('comuna', ['ngRoute', 'ngSanitize','amChartsDirective', 'amChartsGauge', 'nemLogging', 'ui-leaflet', '720kb.tooltips' ])
.config(function($routeProvider, $locationProvider) {

	$routeProvider
	// .when('/',{
	// 	templateUrl: Localize.partials+'candidatos.html',
	// 	controller: 'defaultCtrl'
	// })
	.when('/perfil/:ID', {
		templateUrl: Localize.partials+'location-detail.html',
		controller: 'perfilComunaCtrlDefault'
	})
	.when('/perfil/:ID/senadores', {
		templateUrl: Localize.partials+'location-detail.html',
		controller: 'perfilComunaCtrlSenadores'

	})
	.when('/perfil/:ID/diputados', {
		templateUrl: Localize.partials+'location-detail.html',
		controller: 'perfilComunaCtrlDiputados'

	})
	.when('/perfil/:ID/presidente', {
		templateUrl: Localize.partials+'location-detail.html',
		controller: 'perfilComunaCtrlPresidente'

	})
	.otherwise({
		redirectTo: '/'
	});
})
//CONTROLLER POR DEFECTO 
.controller('perfilComunaCtrlDefault', function($scope, $http, $route, $location, $window, $routeParams, $timeout, pointsServ, leafletData, leafletBoundsHelpers, $filter, currencyFilter){
	var id = $routeParams.ID;
	$window.location.href  = Localize.site_url+'/comuna/#/perfil/'+id+'/diputados';
})
//CONTROLLER PARA SENADORES
.controller('perfilComunaCtrlSenadores', function($scope, $http, $route, $location, $window, $routeParams, $timeout, pointsServ, leafletData, leafletBoundsHelpers, $filter, currencyFilter){
	$scope.bounds = [];
	$scope.tipoCandidato = 'senadores';
	$scope.comunaId = $routeParams.ID;
	$scope.localUrl = Localize.site_url;

	$scope.profileRedirect = function(tipo) {
		var uniq = (new Date()).getTime().toString();
		var url = Localize.site_url+'/comuna/?d='+uniq.substr(uniq.length-4)+'#/perfil/'+$routeParams.ID+'/'+tipo;
		$window.location.href  = url;
		$route.reload();
	}
	pointsServ.getComuna($routeParams.ID).then(function(response) {
		$scope.comuna = response[0].comuna_nombre;
		$scope.comuna_distrito = response[0].distrito_id;
		$scope.comuna_circunscripcion = response[0].circunscripcion_id;
		$scope.comuna_provincia = response[0].provincia_nombre;
		$scope.comuna_region = response[0].region_nombre;

	});
	pointsServ.getPoints($routeParams.ID).then(function(response){
		var bounds = [];
		response.forEach(function(item,index){
			var obj = {};
			var bound = [ parseFloat(item.latitud), parseFloat(item.longitud)];
			obj.lat = parseFloat(item.latitud);
			obj.lng = parseFloat(item.longitud);
			obj.layer = 'propaganda';
			obj.message = item.nombre_espacio;
			$scope.markers[index] = obj;
		});
		$scope.markerList = response;
		if ($scope.markerList.length > 0) {
			$scope.center = { lat: $scope.markers[0].lat, lng: $scope.markers[0].lng, zoom: 11  };
		} else {
			$scope.center = { lat: -33.436652234599, lng: -70.636580586433, zoom: 4  };
		}
	});
	pointsServ.getCandidatos($routeParams.ID, 'senadores').then(function(response){
		$scope.listCandidate = response;
		$scope.candidatosComunaData = response.map(function(item, index){
			var obj = {};
			obj.candidato = item.nombre;
			obj.monto = (item.monto > 0) ? (item.monto / 1000000) : item.monto;
			obj.monto_large = $filter('currency')(item.monto,'$',0);
			obj.color = '#4B3985';
			return obj;
		});
		$scope.$broadcast('amChartsAlt.updateData', $scope.candidatosComunaData, 'candidatosComuna_amchart');
	});
	$scope.markers = {};
	$scope.markerList = [];
	$scope.center = {};
    $scope.default = {
    	scrollWheelZoom: false
    };
    $scope.layers = {
    	baselayers: {
            osm: {
                name: 'OpenStreetMap',
                type: 'xyz',
                url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                layerOptions: {
                    subdomains: ['a', 'b', 'c'],
                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                    continuousWorld: true
                }
            },
            cycle: {
                name: 'OpenCycleMap',
                type: 'xyz',
                url: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
                layerOptions: {
                    subdomains: ['a', 'b', 'c'],
                    attribution: '&copy; <a href="http://www.opencyclemap.org/copyright">OpenCycleMap</a> contributors - &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                    continuousWorld: true
                }
            }
        },
        overlays: {
            propaganda: {
                name: 'Propaganda',
                type: 'markercluster',
                visible: true
            }
        }
    };
    $scope.candidatosComunaData = [
		{
	  	candidato: "ANDRES SEPULVEDA ROJAS",
	  	color: "#4B3985",
	  	monto: 87989931
	  },
	  {
	  	candidato: "CLAUDIO NICOLAS CASTRO SALAS",
	  	color: "#FFA500",
	  	monto: 100073655
	  },
	  {
	  	candidato: "LUIS JAPAZ LUCIO",
	  	color: "#4B3985",
	  	monto: 2987666
	  }
	];
    //Gráfico de barras
	$scope.candidatosComuna = {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": $scope.candidatosComunaData,
	  "valueAxes": [{
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "Cantidad en millones",
	  }],
	  "startDuration": 1,
	  "graphs": [{
	    "balloonText": "<b>[[category]]: [[description]]</b>",
	    "fillColorsField": "color",
	    "fillAlphas": 0.9,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "monto",
	    "descriptionField": "monto_large"
	  }],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "candidato",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "labelRotation": 55,
		labelFunction : function(data, obj, axis) {
			// Se abrevian los nombres intentando (de manera incompleta) identificar ligaduras
			// para solo abreviar los nombres y no los apellidos.
			// Se asume que el primer texto siempre es nombre y se abrevia.
			var label     = obj.category,
				arr       = label.split(' '),
				ligaduras = ['DE', 'LA', 'LAS', 'DEL'];
			arr[0] = arr[0][0].toUpperCase() + '. ';
			for (var i=1; i <= (arr.length - 2 - 1); i++) {
				if (jQuery.inArray(arr[i+1].toUpperCase(), ligaduras) == -1) {
					arr[i] = arr[i][0].toUpperCase() + '. ';
				}
			}
			return arr.join(' ');
		}
	  },
	  "responsive": {
  		"enabled": true,
  		"rules": [{
			"maxWidth": 360,
			"overrides": {
	  			"categoryAxis": { "labelRotation": 90 }
  		 	}
  		}]
	  },
	  "export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
	$scope.aportes = [{title:'prueba', value: 100}];
	$scope.tipoAaporteChart = {
		"type": "pie",
		"theme": "light",
		"data": $scope.aportes,
		"titleField": "title",
		"autoMargins": false,
		"marginTop": 0,
		"marginBottom": 0,
		"marginLeft": 0,
		"marginRight": 0,
		"pullOutRadius": 0,
		"valueField": "value",
		"categoryField": "title",
		"labelRadius": 3,
		"percentPrecision": 1,
		"radius": "32%",
		"innerRadius": "60%",
		"labelText": "[[title]]",
		"export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
	$scope.selectCandidate = function(item) {
		$scope.chosenCandidate = item;
		$scope.selectedCandidate = item.id;
		$scope.isSelectedCandidate = true;
		$scope.aportes = [
			{ title: 'Personas con publicidad', value: item.tipo_aportes.publico },
			{ title: 'Propio', value: item.tipo_aportes.propio },
			{ title: 'Personas sin publicidad', value: item.tipo_aportes.spublicidad },
			{ title: 'Partido Político', value: item.tipo_aportes.partido }
		]

		$scope.candidatosComunaData.forEach(function(data, index){
			if (data.candidato == $scope.chosenCandidate.nombre) {
				$scope.candidatosComunaData[index].color = '#FFA500';
			} else {
				$scope.candidatosComunaData[index].color = '#4B3985';
			}
		});
		$scope.processingInfo = true;
		$timeout(function() {
			$scope.$broadcast('amCharts.updateData', $scope.aportes, 'aportesType');
			$scope.processingInfo = false;
		}, 1000);
		$scope.$broadcast('amChartsAlt.updateData', $scope.candidatosComunaData, 'candidatosComuna_amchart');
	}
	$scope.selectPoint = function(mark) {
		$scope.selectedMarker = mark.id;
		$scope.center = {
			lat: parseFloat(mark.latitud),
			lng: parseFloat(mark.longitud),
			zoom: 18
		};
		$scope.markerList.forEach(function(item, index){
			if ($scope.selectedMarker == item.id) {
				$scope.markers[index].focus = true;
			} else {
				$scope.markers[index].focus = false;
			}
		});
	}
	$scope.userProfileRedirect = function($id) {
		$window.location.href  = Localize.site_url+'/perfil/#/candidato/'+$id;
	}
})
//CONTROLLER PARA DIPUTADOS
.controller('perfilComunaCtrlDiputados', function($scope, $http, $route, $location, $window, $routeParams, $timeout, pointsServ, leafletData, leafletBoundsHelpers, $filter, currencyFilter){
	$scope.bounds = [];
	$scope.tipoCandidato = 'diputados';
	$scope.comunaId = $routeParams.ID;
	$scope.localUrl = Localize.site_url;
	$scope.profileRedirect = function(tipo) {
		var uniq = (new Date()).getTime().toString();
		var url = Localize.site_url+'/comuna/?d='+uniq.substr(uniq.length-4)+'#/perfil/'+$routeParams.ID+'/'+tipo;
		$window.location.href  = url;
		$route.reload();
	}
	pointsServ.getComuna($routeParams.ID).then(function(response) {
		$scope.comuna = response[0].comuna_nombre;
		$scope.comuna_distrito = response[0].distrito_id;
		$scope.comuna_circunscripcion = response[0].circunscripcion_id;
		$scope.comuna_provincia = response[0].provincia_nombre;
		$scope.comuna_region = response[0].region_nombre;

	});
	pointsServ.getPoints($routeParams.ID).then(function(response){
		var bounds = [];
		response.forEach(function(item,index){
			var obj = {};
			var bound = [ parseFloat(item.latitud), parseFloat(item.longitud)];
			obj.lat = parseFloat(item.latitud);
			obj.lng = parseFloat(item.longitud);
			obj.layer = 'propaganda';
			obj.message = item.nombre_espacio;
			$scope.markers[index] = obj;
		});
		$scope.markerList = response;
		if ($scope.markerList.length > 0) {
			$scope.center = { lat: $scope.markers[0].lat, lng: $scope.markers[0].lng, zoom: 11  };
		} else {
			$scope.center = { lat: -33.436652234599, lng: -70.636580586433, zoom: 4  };
		}
	});
	pointsServ.getCandidatos($routeParams.ID, 'diputados').then(function(response){
		$scope.listCandidate = response;
		$scope.candidatosComunaData = response.map(function(item, index){
			var obj = {};
			obj.candidato = item.nombre;
			obj.monto = (item.monto > 0) ? (item.monto / 1000000) : item.monto;
			obj.monto_large = $filter('currency')(item.monto,'$',0);
			obj.color = '#4B3985';
			return obj;
		});
		$scope.$broadcast('amChartsAlt.updateData', $scope.candidatosComunaData, 'candidatosComuna_amchart');
	});
	$scope.markers = {};
	$scope.markerList = [];
	$scope.center = {};
    $scope.default = {
    	scrollWheelZoom: false
    };
    $scope.layers = {
    	baselayers: {
            osm: {
                name: 'OpenStreetMap',
                type: 'xyz',
                url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                layerOptions: {
                    subdomains: ['a', 'b', 'c'],
                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                    continuousWorld: true
                }
            },
            cycle: {
                name: 'OpenCycleMap',
                type: 'xyz',
                url: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
                layerOptions: {
                    subdomains: ['a', 'b', 'c'],
                    attribution: '&copy; <a href="http://www.opencyclemap.org/copyright">OpenCycleMap</a> contributors - &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                    continuousWorld: true
                }
            }
        },
        overlays: {
            propaganda: {
                name: 'Propaganda',
                type: 'markercluster',
                visible: true
            }
        }
    };
    $scope.candidatosComunaData = [
		{
	  	candidato: "ANDRES SEPULVEDA ROJAS",
	  	color: "#4B3985",
	  	monto: 87989931
	  },
	  {
	  	candidato: "CLAUDIO NICOLAS CASTRO SALAS",
	  	color: "#FFA500",
	  	monto: 100073655
	  },
	  {
	  	candidato: "LUIS JAPAZ LUCIO",
	  	color: "#4B3985",
	  	monto: 2987666
	  }
	];
    //Gráfico de barras
	$scope.candidatosComuna = {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": $scope.candidatosComunaData,
	  "valueAxes": [{
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "Cantidad en millones",
	  }],
	  "startDuration": 1,
	  "graphs": [{
	    "balloonText": "<b>[[category]]: [[description]]</b>",
	    "fillColorsField": "color",
	    "fillAlphas": 0.9,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "monto",
	    "descriptionField": "monto_large"
	  }],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "candidato",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "labelRotation": 55,
		labelFunction : function(data, obj, axis) {
			// Se abrevian los nombres intentando (de manera incompleta) identificar ligaduras
			// para solo abreviar los nombres y no los apellidos.
			// Se asume que el primer texto siempre es nombre y se abrevia.
			var label     = obj.category,
				arr       = label.split(' '),
				ligaduras = ['DE', 'LA', 'LAS', 'DEL'];
			arr[0] = arr[0][0].toUpperCase() + '. ';
			for (var i=1; i <= (arr.length - 2 - 1); i++) {
				if (jQuery.inArray(arr[i+1].toUpperCase(), ligaduras) == -1) {
					arr[i] = arr[i][0].toUpperCase() + '. ';
				}
			}
			return arr.join(' ');
		}
	  },
	  "responsive": {
  		"enabled": true,
  		"rules": [{
			"maxWidth": 360,
			"overrides": {
	  			"categoryAxis": { "labelRotation": 90 }
  		 	}
  		}]
	  },
	  "export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
	$scope.aportes = [{title:'prueba', value: 100}];
	$scope.tipoAaporteChart = {
		"type": "pie",
		"theme": "light",
		"data": $scope.aportes,
		"titleField": "title",
		"autoMargins": false,
		"marginTop": 0,
		"marginBottom": 0,
		"marginLeft": 0,
		"marginRight": 0,
		"pullOutRadius": 0,
		"valueField": "value",
		"categoryField": "title",
		"labelRadius": 3,
		"percentPrecision": 1,
		"radius": "32%",
		"innerRadius": "60%",
		"labelText": "[[title]]",
		"export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
	$scope.selectCandidate = function(item) {
		$scope.chosenCandidate = item;
		$scope.selectedCandidate = item.id;
		$scope.isSelectedCandidate = true;
		$scope.aportes = [
			{ title: 'Personas con publicidad', value: item.tipo_aportes.publico },
			{ title: 'Propio', value: item.tipo_aportes.propio },
			{ title: 'Personas sin publicidad', value: item.tipo_aportes.spublicidad },
			{ title: 'Partido Político', value: item.tipo_aportes.partido }
		]

		$scope.candidatosComunaData.forEach(function(data, index){
			if (data.candidato == $scope.chosenCandidate.nombre) {
				$scope.candidatosComunaData[index].color = '#FFA500';
			} else {
				$scope.candidatosComunaData[index].color = '#4B3985';
			}
		});
		$scope.processingInfo = true;
		$timeout(function() {
			$scope.$broadcast('amCharts.updateData', $scope.aportes, 'aportesType');
			$scope.processingInfo = false;
		}, 1000);
		$scope.$broadcast('amChartsAlt.updateData', $scope.candidatosComunaData, 'candidatosComuna_amchart');
	}
	$scope.selectPoint = function(mark) {
		$scope.selectedMarker = mark.id;
		$scope.center = {
			lat: parseFloat(mark.latitud),
			lng: parseFloat(mark.longitud),
			zoom: 18
		};
		$scope.markerList.forEach(function(item, index){
			if ($scope.selectedMarker == item.id) {
				$scope.markers[index].focus = true;
			} else {
				$scope.markers[index].focus = false;
			}
		});
	}
	$scope.userProfileRedirect = function($id) {
		$window.location.href  = Localize.site_url+'/perfil/#/candidato/'+$id;
	}
})
// CONTROLLER PARA PRESIDENTE
.controller('perfilComunaCtrlPresidente', function($scope, $http, $route, $location, $window, $routeParams, $timeout, pointsServ, leafletData, leafletBoundsHelpers, $filter, currencyFilter){
	$scope.bounds = [];
	$scope.tipoCandidato = 'presidente';
	$scope.comunaId = $routeParams.ID;
	$scope.localUrl = Localize.site_url;
	$scope.profileRedirect = function(tipo) {
		var uniq = (new Date()).getTime().toString();
		var url = Localize.site_url+'/comuna/?d='+uniq.substr(uniq.length-4)+'#/perfil/'+$routeParams.ID+'/'+tipo;
		$window.location.href  = url;
		$route.reload();
	}
	pointsServ.getComuna($routeParams.ID).then(function(response) {
		$scope.comuna = response[0].comuna_nombre;
		$scope.comuna_distrito = response[0].distrito_id;
		$scope.comuna_circunscripcion = response[0].circunscripcion_id;
		$scope.comuna_provincia = response[0].provincia_nombre;
		$scope.comuna_region = response[0].region_nombre;

	});
	pointsServ.getPoints($routeParams.ID).then(function(response){
		var bounds = [];
		response.forEach(function(item,index){
			var obj = {};
			var bound = [ parseFloat(item.latitud), parseFloat(item.longitud)];
			obj.lat = parseFloat(item.latitud);
			obj.lng = parseFloat(item.longitud);
			obj.layer = 'propaganda';
			obj.message = item.nombre_espacio;
			$scope.markers[index] = obj;
		});
		$scope.markerList = response;
		if ($scope.markerList.length > 0) {
			$scope.center = { lat: $scope.markers[0].lat, lng: $scope.markers[0].lng, zoom: 11  };
		} else {
			$scope.center = { lat: -33.436652234599, lng: -70.636580586433, zoom: 4  };
		}
	});
	pointsServ.getCandidatos($routeParams.ID, 'presidente').then(function(response){
		$scope.listCandidate = response;
		$scope.candidatosComunaData = response.map(function(item, index){
			var obj = {};
			obj.candidato = item.nombre;
			obj.monto = (item.monto > 0) ? (item.monto / 1000000) : item.monto;
			obj.monto_large = $filter('currency')(item.monto,'$',0);
			obj.color = '#4B3985';
			return obj;
		});
		$scope.$broadcast('amChartsAlt.updateData', $scope.candidatosComunaData, 'candidatosComuna_amchart');
	});
	$scope.markers = {};
	$scope.markerList = [];
	$scope.center = {};
    $scope.default = {
    	scrollWheelZoom: false
    };
    $scope.layers = {
    	baselayers: {
            osm: {
                name: 'OpenStreetMap',
                type: 'xyz',
                url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                layerOptions: {
                    subdomains: ['a', 'b', 'c'],
                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                    continuousWorld: true
                }
            },
            cycle: {
                name: 'OpenCycleMap',
                type: 'xyz',
                url: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
                layerOptions: {
                    subdomains: ['a', 'b', 'c'],
                    attribution: '&copy; <a href="http://www.opencyclemap.org/copyright">OpenCycleMap</a> contributors - &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                    continuousWorld: true
                }
            }
        },
        overlays: {
            propaganda: {
                name: 'Propaganda',
                type: 'markercluster',
                visible: true
            }
        }
    };
    $scope.candidatosComunaData = [
		{
	  	candidato: "ANDRES SEPULVEDA ROJAS",
	  	color: "#4B3985",
	  	monto: 87989931
	  },
	  {
	  	candidato: "CLAUDIO NICOLAS CASTRO SALAS",
	  	color: "#FFA500",
	  	monto: 100073655
	  },
	  {
	  	candidato: "LUIS JAPAZ LUCIO",
	  	color: "#4B3985",
	  	monto: 2987666
	  }
	];
    //Gráfico de barras
	$scope.candidatosComuna = {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": $scope.candidatosComunaData,
	  "valueAxes": [{
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "Cantidad en millones",
	  }],
	  "startDuration": 1,
	  "graphs": [{
	    "balloonText": "<b>[[category]]: [[description]]</b>",
	    "fillColorsField": "color",
	    "fillAlphas": 0.9,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "monto",
	    "descriptionField": "monto_large"
	  }],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "candidato",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "labelRotation": 55,
		labelFunction : function(data, obj, axis) {
			// Se abrevian los nombres intentando (de manera incompleta) identificar ligaduras
			// para solo abreviar los nombres y no los apellidos.
			// Se asume que el primer texto siempre es nombre y se abrevia.
			var label     = obj.category,
				arr       = label.split(' '),
				ligaduras = ['DE', 'LA', 'LAS', 'DEL'];
			arr[0] = arr[0][0].toUpperCase() + '. ';
			for (var i=1; i <= (arr.length - 2 - 1); i++) {
				if (jQuery.inArray(arr[i+1].toUpperCase(), ligaduras) == -1) {
					arr[i] = arr[i][0].toUpperCase() + '. ';
				}
			}
			return arr.join(' ');
		}
	  },
	  "responsive": {
  		"enabled": true,
  		"rules": [{
			"maxWidth": 360,
			"overrides": {
	  			"categoryAxis": { "labelRotation": 90 }
  		 	}
  		}]
	  },
	  "export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
	$scope.aportes = [{title:'prueba', value: 100}];
	$scope.tipoAaporteChart = {
		"type": "pie",
		"theme": "light",
		"data": $scope.aportes,
		"titleField": "title",
		"autoMargins": false,
		"marginTop": 0,
		"marginBottom": 0,
		"marginLeft": 0,
		"marginRight": 0,
		"pullOutRadius": 0,
		"valueField": "value",
		"categoryField": "title",
		"labelRadius": 3,
		"percentPrecision": 1,
		"radius": "32%",
		"innerRadius": "60%",
		"labelText": "[[title]]",
		"export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
	$scope.selectCandidate = function(item) {
		$scope.chosenCandidate = item;
		$scope.selectedCandidate = item.id;
		$scope.isSelectedCandidate = true;
		$scope.aportes = [
			{ title: 'Personas con publicidad', value: item.tipo_aportes.publico },
			{ title: 'Propio', value: item.tipo_aportes.propio },
			{ title: 'Personas sin publicidad', value: item.tipo_aportes.spublicidad },
			{ title: 'Partido Político', value: item.tipo_aportes.partido }
		]

		$scope.candidatosComunaData.forEach(function(data, index){
			if (data.candidato == $scope.chosenCandidate.nombre) {
				$scope.candidatosComunaData[index].color = '#FFA500';
			} else {
				$scope.candidatosComunaData[index].color = '#4B3985';
			}
		});
		$scope.processingInfo = true;
		$timeout(function() {
			$scope.$broadcast('amCharts.updateData', $scope.aportes, 'aportesType');
			$scope.processingInfo = false;
		}, 1000);
		$scope.$broadcast('amChartsAlt.updateData', $scope.candidatosComunaData, 'candidatosComuna_amchart');
	}
	$scope.selectPoint = function(mark) {
		$scope.selectedMarker = mark.id;
		$scope.center = {
			lat: parseFloat(mark.latitud),
			lng: parseFloat(mark.longitud),
			zoom: 18
		};
		$scope.markerList.forEach(function(item, index){
			if ($scope.selectedMarker == item.id) {
				$scope.markers[index].focus = true;
			} else {
				$scope.markers[index].focus = false;
			}
		});
	}
	$scope.userProfileRedirect = function($id) {
		$window.location.href  = Localize.site_url+'/perfil/#/candidato/'+$id;
	}
})
.controller('comunaCtrl', function($scope, $http, $route, $location, $routeParams){
	// var id = $route.current.params.ID;

})
.factory('pointsServ', function($http, $q, $routeParams, $filter, currencyFilter){
	var self = this;
	self.comuna = [];
	self.points = [];
	self.candidates = [];

	var getComuna = function($id){
		var defer = $q.defer();
		$http.get(Localize.site_url+'/wp-json/municipales/v1/comuna/'+$id).then(function(response){
			self.comuna = response.data;
			defer.resolve(self.comuna);
		}, function(response){
			defer.reject(response);
		});
		return defer.promise;
	};
	var getPoints = function($id){
		var defer = $q.defer();
		$http.get(Localize.site_url+'/wp-json/municipales/v1/puntos/'+$id).then(function(response){
			self.points = response.data;
			defer.resolve(self.points);
		}, function(response){
			defer.reject(response);
		});
		return defer.promise;
	};
	var getCandidatos = function($comuna_id, tipo) {
		if (self.candidates.length > 0) {
			return self.candidates;
		} else {
		$defer = $q.defer();
			var candidatos = [];
			var url = Localize.site_url+'/wp-json/municipales/v1/candidatos/'+tipo+'/'+$comuna_id;
			if (tipo == 'presidente') {
				url = Localize.site_url+'/wp-json/municipales/v1/candidatos/presidente';
			}
			 $http.get(url).then(function(res){
				var urlCalls = [];
				angular.forEach(res.data,function(data) {
					urlCalls.push($http.get(Localize.site_url+'/wp-json/municipales/v1/aportes/'+tipo+'/candidato/'+data.id));
				});
				$q.all(urlCalls).then(function(results){
					results.forEach(function(response, index){
						var candidato = {};
						candidato.id = response.data.candidato.id;
						candidato.nombre = response.data.candidato.nombre_candidato;
						candidato.monto = response.data.aportes.total;
						candidato.partido = response.data.candidato.partido;
						candidato.pacto = response.data.candidato.pacto;
						candidato.comuna = response.data.candidato.comuna;
						candidato.region = response.data.candidato.region;
						candidato.limite = response.data.limite;
						candidato.tipo_aportes = {
							propio: response.data.aportes.propio,
							publico: response.data.aportes.publico,
							spublicidad:  response.data.aportes.total_spublicidad,
							partido: response.data.aportes.partido
						};
						candidatos.push(candidato);
					} );
					self.candidates = candidatos;
					$defer.resolve(candidatos);
				})

			});
			 return $defer.promise;
		}
	}


	return { getComuna: getComuna, getPoints: getPoints, getCandidatos: getCandidatos };
})
.directive('scrollToItem', function() {
	return {
	    restrict: 'A',
	    scope: {
	        scrollTo: "@"
	    },
	    link: function(scope, $elm,attr) {

	        $elm.on('click', function() {
	            jQuery('html,body').animate({scrollTop: jQuery(scope.scrollTo).offset().top }, "slow");
	        });
	    }
}});