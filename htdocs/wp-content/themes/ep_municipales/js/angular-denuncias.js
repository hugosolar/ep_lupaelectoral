angular.module('denuncias', ['ngRoute', 'ngSanitize','amChartsDirective', 'amChartsGauge', '720kb.tooltips', 'datatables', 'datatables.buttons' ])
.controller('denunciasCtrl', function($scope, $http, $location, $routeParams, $timeout, denunciasServ){
	$scope.total_denuncias = 0;
	$scope.tipoDenunciasData = [
					{ title: 'Espacio no autorizado', value: 10 },
					{ title: 'Exdece límite gasto', value: 20 },
					{ title: 'Financiamiento ilegal', value: 30 }
				];
	
	$scope.tipoDenunciasChart = {
				"titles": [{
					"text": "Denuncias por estado",
					"size": 15
				}],
				"type": "pie",
				"theme": "light",
				"data": $scope.tipoDenunciasData,
				"titleField": "title",
				"autoMargins": false,
				"marginTop": 0,
				"marginBottom": 0,
				"marginLeft": 0,
				"marginRight": 0,
				"pullOutRadius": 0,
				"valueField": "value",
				"labelRadius": 3,
				"percentPrecision": 1,
				"radius": "32%",
				"innerRadius": "60%",
				"labelText": "[[title]]",
				"export": {
					"enabled": true,
					"menu": [ {
					    "class": "export-main",
					    "menu": [ {
					      "label": "Descargar imagen",
					      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
					    }, {
					      "label": "Descargar datos",
					      "menu": [ "CSV", "XLSX" ]
					    } ]
					  } ]
				}
			};
	$scope.totalTipoDenunciasData = [
		{ descripcion: 'cargando datos...', total: 10, color: '#00A19B' }
	];

	//Gráfico de barras
	$scope.totalTipoDenuncias = {
	  "titles": [{
	  	"text": "Top 5 por Tipo",
	  	"size": 15
	  }],
	  "type": "serial",
	  "theme": "light",
	  "marginRight": 70,
	  "dataProvider": $scope.totalTipoDenunciasData,
	  "valueAxes": [{
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "Denuncias"
	  }],
	  "startDuration": 1,
	  "graphs": [{
	    "balloonText": "<b>[[category]]: [[value]]</b>",
	    "fillColorsField": "color",
	    "fillAlphas": 0.9,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "total",
	  }],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "descripcion",
	  "categoryAxis": {
	    "gridPosition": "start",
	    // "autoGridCount": false,
	    // "gridCount": $scope.totalTipoDenunciasData.length,
	    "labelRotation": 70,
	    "labelText": " ",
	    labelFunction : function(data, obj, axis) {
	    
	    	var label = obj.category;
    		ellipsis = (label.length > 15) ? '...' : '',
    		data_name = label.substring(0,15) + ellipsis;
	    
	    return data_name;
	    }
	  },
	  "responsive": {
	  		"enabled": true,
	  		 "rules": [
	  		 	{ 
	  		 		"maxWidth": 400,
						"overrides": {
		  		 			"categoryAxis": {
		  		 				"labelRotation": 90
		  		 			}
	  		 		}
	  		 	}
	  		]
	  },
	  "export": {
			"enabled": true,
			"menu": [ {
			    "class": "export-main",
			    "menu": [ {
			      "label": "Descargar imagen",
			      "menu": [ "PNG", "JPG", "SVG", "PDF" ]
			    }, {
			      "label": "Descargar datos",
			      "menu": [ "CSV", "XLSX" ]
			    } ]
			  } ]
		}
	};
	denunciasServ.getDenunciasTipo().then(function(response){
		$scope.tipoDenunciasData = [
			{ title: 'En Análisis', value: response.denunciasTipo.en_analisis },
			{ title: 'Admisible', value: response.denunciasTipo.admisible },
			{ title: 'Inadmisible', value: response.denunciasTipo.inadmisible }
		];

		$scope.totalTipoDenunciasData = [];
		var otros = { "descripcion": "OTROS", "total": 0, "color": "#00A19B" };
		for (var i=0; i < response.denuncias.length; i++) {
			if (i < 5) {
				var obj = {};
				obj.descripcion = response.denuncias[i].descripcion;
				obj.total = parseInt(response.denuncias[i].total);
				obj.color = '#00A19B';
				$scope.totalTipoDenunciasData.push(obj);
			} else {
				otros.total += parseInt(response.denuncias[i].total);
			}
		}
		if (otros.total > 0) {
			$scope.totalTipoDenunciasData.push(otros);
		}

		$scope.total_denuncias = response.denunciasTipo.total;
		$scope.$broadcast('amChartsAlt.updateData', $scope.totalTipoDenunciasData, 'totalDenuncias_amchart');
		$scope.$broadcast('amCharts.updateData', $scope.tipoDenunciasData, 'tipoDenuncias');
   	});
})
.controller('denunciasListCtrl', function($route, $scope, $http, $q, $location, $routeParams, denunciasServ, DTOptionsBuilder, DTColumnBuilder){

	$scope.tableView = Localize.partials+'/table-denuncias.html';
	var vm = this;
	$scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
        var defer = $q.defer();
       	denunciasServ.getDenuncias().then(function(response){
       		defer.resolve(response);
       	});

        return defer.promise;
       })
		.withLanguageSource(Localize.local_json+'/datatables.json')
		.withButtons([
           { 'extend': 'copyHtml5', text:'Copiar', copyTitle: 'Datos copiados'}, {'extend': 'print', 'text': 'Imprimir'}, 'excelHtml5','pdfHtml5', 'csvHtml5'
        ])
        .withOption('responsive', true)
        .withOption('fixedHeader',{footer: true})
        .withOption('footerCallback', function(row, data, start, end, display){
        	var api = this.api(), data;
        	var intVal = function ( i ) {
                return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ? i : 0;
            };
            var total_1 = api.column( 1 ).data().reduce( function (a, b) { return intVal(a) + intVal(b); }, 0 );
            var total_2 = api.column( 2 ).data().reduce( function (a, b) { return intVal(a) + intVal(b); }, 0 );
            var total_3 = api.column( 3 ).data().reduce( function (a, b) { return intVal(a) + intVal(b); }, 0 );
            var total_4 = api.column( 4 ).data().reduce( function (a, b) { return intVal(a) + intVal(b); }, 0 );

            jQuery(api.column(1).footer()).html(total_1);
            jQuery(api.column(2).footer()).html(total_2);
            jQuery(api.column(3).footer()).html(total_3);
            jQuery(api.column(4).footer()).html(total_4);
        })

    $scope.dtColumns = [
        DTColumnBuilder.newColumn('descripcion').withTitle('Descripción'),
        DTColumnBuilder.newColumn('en_analisis').withTitle('En Análisis'),
        DTColumnBuilder.newColumn('admisible').withTitle('Admisible'),
        DTColumnBuilder.newColumn('inadmisible').withTitle('Inadmisible'),
        DTColumnBuilder.newColumn('total').withTitle('Total')
    ];
    $scope.dtColumnDefs = [
    	{ responsivePriority: 1, targets: 0 },
    	{ responsivePriority: 5, targets: 1 },
    	{ responsivePriority: 5, targets: 2 },
    	{ responsivePriority: 5, targets: 3 },
    	{ responsivePriority: 1, targets: 4 },
    ];

})
.factory('denunciasServ', function($http, $q, $routeParams, $filter, currencyFilter){
	var self = this;
	self.denunciasList = [];
	self.denunciasTipo = [];

	var getDenuncias = function(){
		var defer = $q.defer();
		$http.get(Localize.site_url+'/wp-json/municipales/v1/denuncias').then(function(response){
			self.denunciasList = response.data.denuncias;
			self.denunciasTipo = response.data.totales;
			defer.resolve(self.denunciasList);
		}, function(response){
			defer.reject(response);
		});
		return defer.promise;
	};
	var getDenunciasTipo = function(){
		if (self.denunciasTipo.length > 0) {
			return self.denunciasTipo;
		} else {
			var defer = $q.defer();
			$http.get(Localize.site_url+'/wp-json/municipales/v1/denuncias').then(function(response){
				self.denunciasList = response.data.denuncias;
				self.denunciasTipo = response.data.totales;
				var obj = {};
				obj.denunciasTipo = self.denunciasTipo;
				obj.denuncias = self.denunciasList;
				obj.denunciasTotal = response.data.denuncias.length;
				defer.resolve(obj);
			}, function(response){
				defer.reject(response);
			});
			return defer.promise;
		}
	};
	
	return { getDenuncias : getDenuncias, getDenunciasTipo : getDenunciasTipo };
});