<?php 
	/*
		Template name: Perfil Comuna
	*/
	get_header(); 
?>
<section class="main-content">
	<div class="row inner-space">
		<div class="large-12 columns">
		</div>
	</div>
	
	<div ng-view></div>
</section>
<div ng-show="::false" class="big-loader">
    <div class="text-loader">
        Cargando...
        <div class="sk-folding-cube">
		  <div class="sk-cube1 sk-cube"></div>
		  <div class="sk-cube2 sk-cube"></div>
		  <div class="sk-cube4 sk-cube"></div>
		  <div class="sk-cube3 sk-cube"></div>
		</div>
    </div>
</div>
<?php get_footer() ?>