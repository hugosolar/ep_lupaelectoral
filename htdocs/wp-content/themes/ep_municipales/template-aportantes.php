<?php 
	/*
		Template name: Perfil Aportante
	*/
	get_header(); 
?>
<section class="main-content">
	<div class="row inner-space">
		<div class="large-12 columns">
			<!-- <h2 class="section-title"><?php the_title(); ?></h2> -->
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="white box candidate-detail element-detail">
			
				<div class="row">
					<div ng-view></div>
				</div>
			</div>		
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns" id="perfilTablaAportes">
			<div class="white box" ng-controller="donationsCtrl" ng-cloak>
				<div class="show-for-small-only text-center"><button class="secondary small" scroll-to-item scroll-to="#menuButtons"><i class="material-icons">keyboard_arrow_up</i></button></div>
				<h3 class="main-title"><i class="material-icons">local_atm</i> <span>Listado de aportes</span></h3>
				<ng-include src="tableView"></ng-include>
			</div>

		</div>
	</div>
</section>
<div ng-show="::false" class="big-loader">
    <div class="text-loader">
        Cargando...
        <div class="sk-folding-cube">
		  <div class="sk-cube1 sk-cube"></div>
		  <div class="sk-cube2 sk-cube"></div>
		  <div class="sk-cube4 sk-cube"></div>
		  <div class="sk-cube3 sk-cube"></div>
		</div>
    </div>
</div>
<?php get_footer() ?>