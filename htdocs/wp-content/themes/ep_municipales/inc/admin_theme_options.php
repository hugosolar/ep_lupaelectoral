<?php
/*
 * Menú para configurar opciones del tema:
 *     - Gifs animados sobre Nuevas Reglas
 */

function ep_opciones_page()
{
    echo '
        <style>
/* Check button: http://stackoverflow.com/a/7642302*/
#ck-button {
    margin:4px;
    background-color:#EFEFEF;
    border-radius:4px;
    border:1px solid #D0D0D0;
    overflow:auto;
    float:left;
}
#ck-button:hover {
    background:red;
}
#ck-button label {
    float:left;
}
#ck-button label span {
    text-align:center;
    padding:3px 3px;
    display:block;
}
#ck-button label input {
    position:absolute;
    top:-20px;
}
#ck-button input:checked + span {
    background-color:#911;
    color:#fff;
}
/* Tabs */
#tabs ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    text-align: center;
}
#tabs ul li {
    display: inline;
    font-size: 1.5em;
    padding: 5px 5px;
}
#tabs ul li a {
    text-decoration: none;
}
#tabs ul li a:hover {
    background: #ccc;
}
.ui-state-active a {
    font-size: 1.2em;
    color: black;
    text-decoration: underline !important;
}
        </style>
        <div class="wrap">
            <h1>#LupaElectoral - Animaciones</h1>
            <form method="post" action="options.php" enctype="multipart/form-data">
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">Animaciones en Portada</a></li>
                        <li><a href="#tabs-2">Animaciones en Noticias</a></li>
                    </ul>
        ';

    settings_fields("section");

    echo '<div id="tabs-1">';
    do_settings_sections("ep-opciones-portada");
    echo '</div>';

    echo '<div id="tabs-2">';
    do_settings_sections("ep-opciones-noticias");
    echo '</div>';

    submit_button();

    echo '
                </div>
            </form>
        </div>
    ';
}
function add_theme_menu_item()
{
    add_menu_page("#LE Animaciones", "#LE Animaciones", "manage_options", "ep-opciones", "ep_opciones_page", null, 99);
}
add_action("admin_menu", "add_theme_menu_item");

/* PORTADA ***************************************************************/
function nuevasreglas_large_display()
{
    $current_img = json_decode(get_option('nuevasreglas_large'), true);
    if (!empty($current_img)) {
        echo '
            <img src="' . $current_img['url'] . '" style="max-width: 400px;"/>
            <br/>
            <div id="ck-button">
               <label>
                  <input type="checkbox" name="eliminar_large"><span>Eliminar animación</span>
               </label>
            </div>
        ';
    } else {
        echo '<em>No se ha subido ninguna imagen aún.</em>';
    }
    echo '
        <br/>
        <br/>
        <strong>Subir nueva animación: </strong><input type="file" name="nuevasreglas_large" />
    ';
}
function handle_nuevasreglas_large_upload()
{
    if (isset($_POST["eliminar_large"]) && $_POST["eliminar_large"] == 'on') {
        $current_img = json_decode(get_option('nuevasreglas_large'), true);
        @unlink($current_img['file']);
        return '';
    }
    if (!empty($_FILES["nuevasreglas_large"]["tmp_name"])) {
        $rtn = wp_handle_upload($_FILES["nuevasreglas_large"], array('test_form' => false));
        if (isset($rtn['error'])) {
            return '';
        }
        return json_encode($rtn);
    }

    return get_option('nuevasreglas_large');
}

// GIF Small
function nuevasreglas_small_display()
{
    $current_img = json_decode(get_option('nuevasreglas_small'), true);
    if (!empty($current_img)) {
        echo '
            <img src="' . $current_img['url'] . '" style="max-width: 250px;"/>
            <br/>
            <div id="ck-button">
               <label>
                  <input type="checkbox" name="eliminar_small"><span>Eliminar animación</span>
               </label>
            </div>
        ';
    } else {
        echo '<em>No se ha subido ninguna imagen aún.</em>';
    }
    echo '
        <br/>
        <br/>
        <strong>Subir nueva animación: </strong><input type="file" name="nuevasreglas_small" />
    ';
}
function handle_nuevasreglas_small_upload()
{
    if (isset($_POST["eliminar_small"]) && $_POST["eliminar_small"] == 'on') {
        $current_img = json_decode(get_option('nuevasreglas_small'), true);
        @unlink($current_img['file']);
        return '';
    }
    if (!empty($_FILES["nuevasreglas_small"]["tmp_name"])) {
        $rtn = wp_handle_upload($_FILES["nuevasreglas_small"], array('test_form' => false));
        if (isset($rtn['error'])) {
            return '';
        }
        return json_encode($rtn);
    }

    return get_option('nuevasreglas_small');
}

/* NOTICIAS ***************************************************************/
function noticias_large_display()
{
    $desktop_img = json_decode(get_option('noticias_large'), true);
    if (!empty($desktop_img)) {
        echo '
            <img src="' . $desktop_img['url'] . '" style="max-width: 400px;"/>
            <br/>
            <div id="ck-button">
               <label>
                  <input type="checkbox" name="eliminar_noticias_large"><span>Eliminar animación</span>
               </label>
            </div>
        ';
    } else {
        echo '<em>No se ha subido ninguna imagen aún.</em>';
    }
    echo '
        <br/>
        <br/>
        <strong>Subir nueva animación: </strong><input type="file" name="noticias_large" />
    ';
}
function handle_noticias_large_upload()
{
    if (isset($_POST["eliminar_noticias_large"]) && $_POST["eliminar_noticias_large"] == 'on') {
        $current_img = json_decode(get_option('noticias_large'), true);
        @unlink($current_img['file']);
        return '';
    }
    if (!empty($_FILES["noticias_large"]["tmp_name"])) {
        $rtn = wp_handle_upload($_FILES["noticias_large"], array('test_form' => false));
        if (isset($rtn['error'])) {
            return '';
        }
        return json_encode($rtn);
    }

    return get_option('noticias_large');
}

function noticias_small_display()
{
    $current_img = json_decode(get_option('noticias_small'), true);
    if (!empty($current_img)) {
        echo '
            <img src="' . $current_img['url'] . '" style="max-width: 250px;"/>
            <br/>
            <div id="ck-button">
               <label>
                  <input type="checkbox" name="eliminar_noticias_small"><span>Eliminar animación</span>
               </label>
            </div>
        ';
    } else {
        echo '<em>No se ha subido ninguna imagen aún.</em>';
    }
    echo '
        <br/>
        <br/>
        <strong>Subir nueva animación: </strong><input type="file" name="noticias_small" />
    ';
}
function handle_noticias_small_upload()
{
    if (isset($_POST["eliminar_noticias_small"]) && $_POST["eliminar_noticias_small"] == 'on') {
        $current_img = json_decode(get_option('noticias_small'), true);
        @unlink($current_img['file']);
        return '';
    }
    if (!empty($_FILES["noticias_small"]["tmp_name"])) {
        $rtn = wp_handle_upload($_FILES["noticias_small"], array('test_form' => false));
        if (isset($rtn['error'])) {
            return '';
        }
        return json_encode($rtn);
    }

    return get_option('noticias_small');
}


function display_theme_panel_fields()
{
    add_settings_section("section", "Animaciones de \"Nuevas reglas\" en Portada", null, "ep-opciones-portada");
    add_settings_field("nuevasreglas_large", "Animación Desktop", "nuevasreglas_large_display", "ep-opciones-portada", "section");
    register_setting("section", "nuevasreglas_large", "handle_nuevasreglas_large_upload");
    add_settings_field("nuevasreglas_small", "Animación Mobile", "nuevasreglas_small_display", "ep-opciones-portada", "section");
    register_setting("section", "nuevasreglas_small", "handle_nuevasreglas_small_upload");

    add_settings_section("section", "Animaciones en Noticias", null, "ep-opciones-noticias");
    add_settings_field("noticias_large", "Animación Desktop", "noticias_large_display", "ep-opciones-noticias", "section");
    register_setting("section", "noticias_large", "handle_noticias_large_upload");
    add_settings_field("noticias_small", "Animación Mobile", "noticias_small_display", "ep-opciones-noticias", "section");
    register_setting("section", "noticias_small", "handle_noticias_small_upload");
}

add_action("admin_init", "display_theme_panel_fields");
