<?php
class fsite {
	static function check_angular_page() {
		global $post;

		$angular_pages = array(
			'aportes',
			'gastos',
			'perfil',
			'comuna',
			'denuncias',
			'aportante'
			);
		if ( ( in_array( $post->post_name, $angular_pages ) ) && ( !is_home() ) ) {
			return true;
		} else {
			return false;
		}
	}
	public function get_post_thumbnail_url( $postid = null, $size = 'featured-home' ){
		if ( is_null($postid) ){
			global $post;
			$postid = $post->ID;
		}
		$thumb_id = get_post_thumbnail_id( $postid );
		$img_src  = wp_get_attachment_image_src( $thumb_id, $size );
		return $img_src ? current( $img_src ) : '';
	}
	static function page_title(){
		$get= get_queried_object();
		if (is_post_type_archive())
			return 'Archivo: '.$get->labels->name;
		if (is_category())
			return 'Categoría '.$get->name;
		if (is_tag())
			return 'Tag "'.$get->name.'"';
		if (is_tax())
			return ''.$get->name;
		if (is_search())
			return 'Búsqueda por: &#8220;'. get_search_query() .'&#8221;';
		if (is_404())
			return 'Ups, Página no encontrada';
	}
	static function since_time($a) {    
		    //get current timestampt
		    $b = strtotime("now");
		    //get timestamp when tweet created
		    $c = strtotime($a);
		    //get difference
		    $d = $b - $c;
		    //calculate different time values
		    $minute = 60;
		    $hour = $minute * 60;
		    $day = $hour * 24;
		    $week = $day * 7;
		    if(is_numeric($d) && $d > 0) {
		        //if less then 3 seconds
		        if($d < 3) return "Ahora";
		        //if less then minute
		        if($d < $minute) return "Hace ". floor($d) . " segundos";
		        //if less then 2 minutes
		        if($d < $minute * 2) return "Hace 1 minuto";
		        //if less then hour
		        if($d < $hour) return "Hace ". floor($d / $minute) . " minutos";
		        //if less then 2 hours
		        if($d < $hour * 2) return "Hace 1 hora";
		        //if less then day
		        if($d < $day) return "Hace ". floor($d / $hour) . " horas";
		        //if more then day, but less then 2 days
		        if($d > $day && $d < $day * 2) return "Ayer";
		        //if less then year
		        if($d < $day * 365) return "Hace ". floor($d / $day) . " días";
		        //else return more than a year
		        return "Más de 1 año";
		    }    
		}
	static function get_entries($position=0,$size=1,$category=null) {
		$args = array(
				'post_type' => array('post'),
				'posts_per_page' => $size,
				'post_status' => 'publish',
				'offset' => $position
				);
		if ( !empty( $category ) ) {
			$args['category_name'] = $category;
		} 
		$featured = new WP_Query($args);
		if ($featured->have_posts()) {
			return $featured->posts;
		} else {
			return false;
		}
	}
	static function get_cpt($position=0, $post_type='post', $size=-1,$taxonomy_name=null,$term=null) {
		$args = array(
				'post_type' => $post_type,
				'posts_per_page' => $size,
				'post_status' => 'publish',
				'offset' => $position
				);
		if (!empty($taxonomy_name) ) {
			$args['tax_query'] = array(
				array(
						'taxonomy' => $taxonomy_name,
						'field' => 'slug',
						'terms' => $term
					)
				);
		}
		$cpt_query = new WP_Query($args);
		if ($cpt_query->have_posts()) {
			return $cpt_query->posts;
		} else {
			return false;
		}
	}
	static function get_last_video() {
		$query = new WP_Query(array(
			'post_type' => 'post',
			'posts_per_page' => 1,
			'category_name' => 'videos'
		));
		if ($query->have_posts()) {
			return current($query->posts);
		} else {
			return false;
		}
	}
	static function get_facebook_link($post, $content=null) {
		$text = ( $content ) ? $content : do_excerpt(wp_strip_all_tags($post->post_content),array('length' => 150));
		$caption = get_the_title($post->ID);
		$url = get_permalink($post->ID);
		$img = self::get_post_thumbnail_url($post,'featured-home');
		$out = '<a href="#" class="facebook-share" data-text="'.$text.'" data-caption="'.$caption.'" data-url="'.$url.'" data-img="'.$img.'"><i class="dashicons dashicons-facebook-alt"></i></a> ';
		return $out;
	}
	static function get_tweet_link($post,$content=null) {
		$text = ( $content ) ? urlencode( $content ) : urlencode($post->post_title);
		$url = urlencode(wp_get_shortlink($post->ID));
		$out = '<a href="https://twitter.com/intent/tweet?text='.$text.'&url='.$url.'" class="tweet-share share-tw"><span class="dashicons dashicons-twitter"></span></a>';
		return $out;
	}
	static function get_whatsapp_link($post,$content=null) {
		$text = ($content) ? urlencode( $content ) : urlencode($post->post_title);
		$url = urlencode(wp_get_shortlink($post->ID));
		$slug = $post->post_name;
		$out = '<a class="show-for-small-only" href="whatsapp://send?text='.$text.'%2F'.$url.'%2F%3Futm_source%3Dwhatsapp%26utm_medium%3Dboton_compartir%26utm_campaign%3D'.$slug.'"><i class="ion-social-whatsapp"></i></a>';
		return $out;
	}
	static function share_article($post, $content) {
		return self::get_facebook_link($post, $content).' '.self::get_tweet_link($post, $content).' '.self::get_whatsapp_link($post, $content);
	}
	public function get_location() {
		function comuna_cmp($a, $b) {
			$from = ['ñ','à','á','ä','â','è','é','ë','ê','ì','í','ï','î','ò','ó','ö','ô','ù','ú','ü','û','ñ','ç'];
			$to = ['n','a','a','a','a','e','e','e','e','i','i','i','i','o','o','o','o','u','u','u','u','n','c'];
			return strcmp(str_replace($from, $to, mb_strtolower($a->nombre)),
				str_replace($from, $to, mb_strtolower($b->nombre)));
		}
		$region = $_POST['region'];
		$json_comunas = wp_remote_get(JSON_TEST_URL.'region_comunas.json');
		if (!is_wp_error($json_comunas)) {
			$comunas = json_decode($json_comunas['body']);
			uasort($comunas->{$region}->comunas, 'comuna_cmp');
			$out = '<h5>Comunas de la región de '.$comunas->{$region}->nombre.'</h5>';
			$out .= '<ul class="menu vertical featured-cities">';
			foreach ($comunas->{$region}->comunas as $item) {
				$out .= '<li><a href="'.site_url('comuna/#/perfil/'.$item->id).'"> <i class="material-icons">my_location</i> '.$item->nombre.'</a></li>';
			}
			$out .= '</ul>';
			echo $out;
			exit(0);
		}
	}
}

add_action( 'wp_ajax_get_location', array( 'fsite', 'get_location' ) );
add_action( 'wp_ajax_nopriv_get_location', array( 'fsite', 'get_location' ) );