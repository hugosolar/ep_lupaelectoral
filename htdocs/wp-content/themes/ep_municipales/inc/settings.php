<?php
use GutenPress\Forms;
use GutenPress\Forms\Element;
use GutenPress\Validate;
use GutenPress\Validate\Validations;
use GutenPress\Model;

class ThemeSettings{
	private $flash;
	public $settings;
	public function __construct(){
		$this->init();
		$this->flash = array(
			'updated' => __('Configuraciones del sitio guardadas', 'ep_municipales'),
			'error'   => __('Hubo un problema guardando las opciones del sitio', 'ep_municipales')
		);
		$this->settings = get_option( 'site_theme_settings');
	}
	public function init(){
		add_action('admin_menu', array($this, 'addAdminMenu'));
		add_action('admin_init', array($this, 'saveSettings'));
	}
	public function addAdminMenu(){
		add_submenu_page( 'index.php' , _x('Configuraciones', 'site settings title', 'ep_municipales'), _x('Configuraciones', 'site settings menu', 'ep_municipales'), 'edit_theme_options', 'ep_municipales-site-settings', array($this, 'adminMenuScreen'));
	}
	public function get_categories() {
		$categories = get_terms('category');
		$select_categories = array('' => 'Ninguno', 'column' => 'Columnas de opinión');
		foreach( $categories as $cat ) {
			$select_categories[$cat->slug] = $cat->name;
		}
		return $select_categories;
	}
	public function adminMenuScreen(){
		echo '<div class="wrap">';
			screen_icon('index');
			echo '<h2>'. _x('Configuraciones', 'site settings title', 'ep_municipales') .'</h2>';
			if ( ! empty($_GET['msg']) && isset($this->flash[ $_GET['msg'] ]) ) :
				echo '<div class="updated">';
					echo '<p>'. $this->flash[ $_GET['msg'] ] .'</p>';
				echo '</div>';
			endif;
			$data = get_option( 'site_theme_settings' );
			$form = new Forms\Form('site-settings');
			$form->addElement( new Element\InputText(
				_x('Título columna 1 Portada', 'site settings fields', 'ep_municipales'),
				'home_title_col_1',
				array(
					'value' => isset($data['home_title_col_1']) ? $data['home_title_col_1'] : ''
				)
			) )->addElement( new Element\Select(
				_x('Contenido columna 1 Portada', 'site settings fields', 'ep_municipales'),
				'home_content_col_1',
				$this->get_categories(),
				array(
					'value' => isset($data['home_content_col_1']) ? $data['home_content_col_1'] : ''
				)
			) )->addElement( new Element\InputText(
				_x('Título columna 2 Portada', 'site settings fields', 'ep_municipales'),
				'home_title_col_2',
				array(
					'value' => isset($data['home_title_col_2']) ? $data['home_title_col_2'] : ''
				)
			) )->addElement( new Element\Select(
				_x('Contenido columna 2 Portada', 'site settings fields', 'ep_municipales'),
				'home_content_col_2',
				$this->get_categories(),
				array(
					'value' => isset($data['home_content_col_2']) ? $data['home_content_col_2'] : ''
				)
			) )->addElement( new Element\InputText(
				_x('Título columna 3 Portada', 'site settings fields', 'ep_municipales'),
				'home_title_col_3',
				array(
					'value' => isset($data['home_title_col_3']) ? $data['home_title_col_3'] : ''
				)
			) )->addElement( new Element\Select(
				_x('Contenido columna 3 Portada', 'site settings fields', 'ep_municipales'),
				'home_content_col_3',
				$this->get_categories(),
				array(
					'value' => isset($data['home_content_col_3']) ? $data['home_content_col_3'] : ''
				)
			) )->addElement( new Element\InputText(
				_x('Título columna 4 Portada', 'site settings fields', 'ep_municipales'),
				'home_title_col_4',
				array(
					'value' => isset($data['home_title_col_4']) ? $data['home_title_col_4'] : ''
				)
			) )->addElement( new Element\Select(
				_x('Contenido columna 4 Portada', 'site settings fields', 'ep_municipales'),
				'home_content_col_4',
				$this->get_categories(),
				array(
					'value' => isset($data['home_content_col_4']) ? $data['home_content_col_4'] : ''
				)
			) );

			/*
			Fin opciones newsletter
			*/
			$form->addElement( new Element\InputSubmit(
				_x('Guardar', 'site settings fields', 'ep_municipales')
			) )->addElement( new Element\WPNonce(
				'update_site_settings',
				'_site_settings_nonce'
			) )->addElement( new Element\InputHidden(
				'action',
				'update_site_settings'
			) );
			echo '<h3>'._x('Home settings', 'site settings fields', 'ep_municipales').'</h3>';
			echo $form;
		echo '</div>';
	}


	public function saveSettings(){
		if ( empty($_POST['action']) )
			return;
		if ( $_POST['action'] !== 'update_site_settings' )
			return;
		if ( ! wp_verify_nonce( $_POST['_site_settings_nonce'], 'update_site_settings' ) )
			wp_die( _x("You are not supposed to do that", 'site settings error', 'ep_municipales') );
		if ( ! current_user_can( 'edit_theme_options' ) )
			wp_die( _x("You are not allowed to edit this options", 'site settings error', 'ep_municipales') );
		$fields = array(
			'home_title_col_1',
			'home_content_col_1',
			'home_title_col_2',
			'home_content_col_2',
			'home_title_col_3',
			'home_content_col_3',
			'home_title_col_4',
			'home_content_col_4',
		);
		$raw_post = stripslashes_deep( $_POST );
		$data = array_intersect_key($raw_post, array_combine($fields, $fields) );
		update_option( 'site_theme_settings' , $data );
		wp_redirect( admin_url('admin.php?page=ep_municipales-site-settings&msg=updated', 303) );
		exit;
	}
}
$_set = new ThemeSettings;


class MpostMeta extends Model\PostMeta{
    protected function setId(){
        return 'post';
    }
    protected function setDataModel(){
        return array(
        	new Model\PostMetaData(
                'url_noticia',
                'URL de la noticia',
                '\GutenPress\Forms\Element\InputText'
            ),
            new Model\PostMetaData(
                'url_video',
                'URL de video',
                '\GutenPress\Forms\Element\InputText',
                array(
                	'description' => 'Url de video puede ser de Youtube o Vimeo'
                )
            )
	        );
    }
}
new Model\Metabox( 'MpostMeta', 'Enlace a noticia', 'post', array('priority' => 'high') );