<?php
/*
	Remove script version to better caching
*/
function _remove_script_version( $src ){
	$parts = explode( '?ver', $src );
	return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}

 // search filter
function make_search_filter($query) {
	$pt = $query->get('post_type');
	if ( empty( $pt ) ) {
		if ( !$query->is_admin && $query->is_search) {
			$query->set('post_type', array('post','publications','stats','infography','testimonials') ); // id of page or post
		}
	}
	return $query;
}
//add_filter( 'pre_get_posts', 'make_search_filter' );