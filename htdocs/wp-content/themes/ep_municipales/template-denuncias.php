<?php 
	/*
		Template name: Denuncias
	*/
	get_header(); 
?>
<section class="main-content" ng-controller="denunciasCtrl" ng-cloak>
	<div class="row inner-space">
		<div class="large-12 columns">
			<!-- <h2 class="section-title"><?php the_title(); ?></h2> -->
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="white box">
				<div class="total">
					<div class="row space-bottom">
		                <div class="large-9 large-centered columns text-center">
		                	<h3 class="section-title">Denuncias</h3>
		                    <h5 class="subheader">
		                        Aquí encontrarás información actualizada sobre las denuncias recibidas por infracciones a las nuevas reglas y  las sanciones impuestas
		                    </h5>

							<!-- <i class="material-icons">announcement</i> -->
							<span class="title">TOTAL DENUNCIAS:</span>
							<span class="number">{{total_denuncias}}</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="large-6 columns">
						<div ng-show="total_denuncias > 0">
							<gauge-chart id="totalDenuncias" options="totalTipoDenuncias" height="350" style="height: 550px;" width="100%"></gauge-chart>
						</div>
					</div>
					<div class="large-6 columns">
						<div ng-show="total_denuncias > 0">
							<am-chart id="tipoDenuncias" options="tipoDenunciasChart" height="350" style="height: 450px;" width="100%"></am-chart>
						</div>
					</div>
				</div>
				<div ng-show="total_denuncias == 0">
					<div class="callout warning">
						<h5>Sin denuncias</h5>
						<p>Por el momento no se registran denuncias</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="white box" ng-controller="denunciasListCtrl">
				<h3 class="main-title"><i class="material-icons">announcement</i> <span>Listado de denuncias</span></h3>
				<ng-include src="tableView"></ng-include>
				<div class="row space-bottom space-top">
	                <div class="large-9 large-centered columns text-center">
	                    <h4 class="subheader">
	                        Si conoces alguna infracción a las nuevas reglas, puedes denunciar ante el Servel <a href="http://denuncias.servel.cl" target="_blank">aquí</a>.
	                    </h4>
	                    <div class="inner-space">
	                    	<div class="show-for-small-only">
	                    		<img src="<?php bloginfo('template_directory') ?>/img/whatsapp-acusete-mobile.gif" alt="">
	                    	</div>
	                    	<div class="hide-for-small-only">
	                    		<img src="<?php bloginfo('template_directory') ?>/img/whatsapp-acusete-desktop.gif" alt="">
	                    	</div>
	                    </div>
					</div>
				</div>
				<p class="description text-right">
					Esta sección se actualiza a medida que el Servel facilita la información. Última actualización <em id="denuncias_last_update">xx/xx/xx</em>.
				</p>
			</div>
		</div>
	</div>
</section>
<div ng-show="::false" class="big-loader">
    <div class="text-loader">
        Cargando...
        <div class="sk-folding-cube">
		  <div class="sk-cube1 sk-cube"></div>
		  <div class="sk-cube2 sk-cube"></div>
		  <div class="sk-cube4 sk-cube"></div>
		  <div class="sk-cube3 sk-cube"></div>
		</div>
    </div>
</div>
<?php get_footer(); ?>