<!doctype html>
<?php
	global $post;
	$if_angular = ( fsite::check_angular_page() ) ? 'ng-app="'.$post->post_name.'"' : '';
 ?>
<html <?php echo $if_angular ?>>
<head>
	<?php 
		if (!is_home()){
			echo '<base href="/'.$post->post_name.'/">';
		} 
	?>
	
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php bloginfo('name'); ?> <?php wp_title('|') ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header class="main-header green">
	<div class="show-for-small-only mobile-nav primary">
		<div class="row">
			<div class="small-3 columns">
			  <a href="<?php bloginfo('url') ?>">
				<div class="mobile-logo<?php echo is_front_page() ? ' mobile-logo-front' : '' ?>"></div>
			  </a>
			</div>
			<div class="small-4 columns">
				<div class="clearfix">
					<a href="#" class="search-open float-left"><span class="dashicons dashicons-search"></span></a>
					<a href="#" class="mobile-nav-open float-right"><span class="dashicons dashicons-menu"></span></a>
				</div>
			</div>
		</div>
	</div>
	<div class="menu-mobile-container hide">
		<a class="close" href="#"><span class="dashicons dashicons-no-alt"></span></a>
		<?php 
			  $args = array(
				  'theme_location' => 'principal',
				  'container' => '',
				  'depth' => 1,
				  'items_wrap' => '<ul id = "%1$s" class = "menu vertical %2$s">%3$s</ul>'
				  );

			  wp_nav_menu( $args );
		 ?>
	</div>
	<div class="row hide-for-small-only">
		<div class="large-2 columns">
			<a href="<?php bloginfo('url') ?>"><h1 class="site-logo<?php echo is_front_page() ? ' site-logo-front' : '' ?>">EP-Municipales</h1></a>
		</div>
		<div class="large-10 columns">
			<nav class="main-menu">
				<?php 
				$args = array(
					'theme_location' => 'principal',
					'items_wrap' => '<ul id = "%1$s" class="menu %2$s">%3$s</ul>',
					'container' => '',
					'depth' => 1
				);
				wp_nav_menu( $args );
			 ?>
			</nav>
	  <nav class="search">
		<a href="#" class="search-button"><i class="material-icons">search</i></a>
	  </nav>
	  <nav class="social">
		<ul class="menu">
		  <li><a href="https://www.facebook.com/espaciopublicochile/" title="Facebook Espacio Público" target="_blank"><span class="dashicons dashicons-facebook"></span></a></li>
		  <li><a href="https://twitter.com/EsPublicoCL" title="Twitter Espacio Público" target="_blank"><span class="dashicons dashicons-twitter"></span></a></li>
		</ul>
	  </nav>
		</div>
	</div>
</header>
<div class="float-search-box the-search">
	<div class="row">
		<div class="large-10 large-offset-2 columns box white">
			<div class="search-box">
				<input id="ep_search" type="text" class="search-input" placeholder="Buscar por nombre de candidato o comuna">
			</div>
			<div class="search-results hide">
			</div>
		</div>
	</div>
</div>