<?php get_header(); ?>
<section class="main-content">
	<div class="row collapse">
		<div class="large-12 columns featured-content">
			<img src="<?php bloginfo('template_directory') ?>/img/home2.jpg">
			<div class="featured-box hentry">
				<div class="lupa-logo"></div>
				<div class="spacing"><h4 class="entry-subtitle">Infórmate, participa, denuncia</h4></div>
				<p class="entry-summary">
					Conoce los lugares públicos donde se puede desplegar propaganda electoral. Accede al listado de personas que financian las campañas de los candidatos y los montos que reciben. Revisa las denuncias que se han presentado al SERVEL. Infórmate sobre las nuevas reglas de campaña. Denuncia irregularidades que veas en la vía pública. ¡Participa fiscalizando!
				</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div id="elecciones-countdown-box" class="large-7 large-centered indicators countdown columns blue text-center">
			<h3>Para las elecciones faltan:</h3>
			<div id="elecciones-countdown" class="clearfix"></div>
		</div>
	</div>
	<div class="row inner-space mobile-no-space">
		<div class="large-10 large-centered columns">
			<ul class="tabs" data-tabs id="home-tabs">
			  <li class="tabs-title is-active"><a href="#map" aria-selected="true">Mapa</a></li>
			  <li class="tabs-title"><a href="#search">Candidatos / Comuna / Aportantes</a></li>
			  <li class="tabs-title link"><a href="<?php echo site_url( 'datos' ) ?>">Datos</a></li>
			  <li class="tabs-title link"><a href="<?php echo site_url( 'denuncias' ); ?>">Denuncias</a></li>
			</ul>
			<div class="tabs-content" data-tabs-content="home-tabs">
			  <div class="tabs-panel is-active" id="map">
			    <div class="row">
			    	<div class="large-6 columns map-container hide-for-small-only">
			    		<a name="mapa"></a>
						<div class="text-center">Haz click en el mapa para mostrar las comunas de cada Región</div>
			    		<div id="map-target"></div>
			    		<div class="hide-map-svg hide"></div>
			    		<div class="map-tooltip"></div>
			    	</div>
			    	<div class="large-6 small-12 columns map-related">
			    		<div class="hide-for-small-only">
				    		<div class="map-information">
				    			<h4><i class="material-icons">place</i> Listado de comunas</h4>
				    		</div>
							<div class="map-cities">
								<h5>¿Quieres visitar algunas de las comunas con más electores?</h5>
								<ul class="menu vertical featured-cities">
								</ul>
							</div>
						</div>
						<div class="show-for-small-only">
							<p>Seleccione una región para desplegar sus comunas</p>
							<select name="regiones" class="select-regiones">
								<option value="">Seleccione región</option>
							</select>
							<select name="comunas" class="select-comunas">
								<option value="">Seleccione una comuna</option>
							</select>
						</div>
						<div class="loader"></div>
			    	</div>
			    </div>
			  </div>
			  <div class="tabs-panel" id="search">
			    <div class="search-box the-search">
			    	<!-- <form action="POST"> -->
		    		<input id="ep_search" type="text" class="search-input" placeholder="Buscar por nombre de candidato o comuna">
		    		<input type="submit" class="button submit-search dashicons" value="">
			    	<!-- </form> -->
			    </div>
			    <div class="search-results hide">
			    </div>
			  </div>
			</div>
		</div>
	</div>
	<div class="row inner-space">
		<div class="large-12 large-centered columns">
			<?php 
				$post_video = fsite::get_last_video();
				$video = $post_video->post_url_video;
				if (!empty($video)) {
					echo '<div class="flex-video widescreen">';
					echo videos::get_video($video);
					echo '</div>';
				}
			?>
		</div>
	</div>
<?php
$gif_large = json_decode(get_option('nuevasreglas_large'), true);
$gif_small = json_decode(get_option('nuevasreglas_small'), true);
$gif_small = empty($gif_small) ? $gif_large : $gif_small;
if (!empty($gif_large)):
?>
	<div class="row">
		<div class="large-10 large-centered columns space-bottom space-top">
			<img data-interchange="[<?php echo $gif_small['url']; ?>, small], [<?php echo $gif_large['url']; ?>, large]">
		</div>
	</div>
<?php endif; ?>
<?php 
	$cifras = false;
	if ($cifras): 
?>
	<div class="row">
		<div id="indicators" class="large-10 large-centered indicators space-bottom columns blue">
			<div class="large-up-3 small-up-3 row clearfix">
				<div class="column">
					<div class="row collapse">
						<div class="large-5 columns icon">
							<i class="icon-mapa big-icon"></i>
						</div>
						<div class="large-7 columns content">
							<span class="number comunas">345</span>
							<span class="description">Comunas</span>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="row collapse">
						<div class="large-4 columns icon">
							<i class="icon-candidato big-icon"></i>
						</div>
						<div class="large-8 columns content">
							<span class="number candidatos">235</span>
							<span class="description">Candidatos a alcalde</span>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="row collapse">
						<div class="large-5 columns icon">
							<i class="icon-urna big-icon"></i>
						</div>
						<div class="large-7 columns content">
							<span class="number partidos">15</span>
							<span class="description">Partidos</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
	<div class="row">
		<div class="large-10 large-centered columns space-bottom">
			<img data-interchange="[<?php bloginfo('template_directory') ?>/img/media_partner_cel.jpg, small], [<?php bloginfo('template_directory') ?>/img/media_partner.jpg, large]" style="width: 100%;">
		</div>
	</div>
	<div class="row">
		<div class="large-10 large-centered columns space-bottom">
			<div class="white box">
				<div class="row">
					<div class="large-2 column">
						<div class="support-text">Con el apoyo de:</div>
					</div>
					<div class="large-5 column">
						<img src="<?php bloginfo('template_directory') ?>/img/embajada-canada.png" alt="">
					</div>
					<div class="large-5 column">
						<img src="<?php bloginfo('template_directory') ?>/img/logo-fes.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="yellow">
		<div class="row inner-space mobile-no-space why">
			<div class="large-12 columns">
				<h3 class="entry-title">¿Para qué este proyecto?</h3>
				<div class="large-up-3 small-up-3 row clearfix">
					<div class="column">
						<article class="hentry entry-reason">
							<span class="icons">
								<img class="icon-binoculares" src="<?php bloginfo('template_directory') ?>/img/binoculars.svg" />
								<!-- <i class="icon-binoculares big-icon"></i> -->
							</span>
							<h4 class="entry-title">Vigilancia ciudadana</h4>
							<p class="entry-summary">
								Ahora tod@s podemos fiscalizar que efectivamente se cumplan las nuevas reglas. Conócelas y denuncia cualquier infracción ante el Servel.
							</p>
						</article>
					</div>
					<div class="column">
						<article class="hentry entry-reason">
							<span class="icons">
								<i class="icon-pilar big-icon"></i>
							</span>
							<h4 class="entry-title">Fortalecer la democracia</h4>
							<p class="entry-summary">
								En estas elecciones tenemos nuevas reglas de campañas.Buscamos informar de estas a la ciudadanía para que sean efectivamente más transparentes, equitativas y limpias.
							</p>
						</article>
					</div>
					<div class="column">
						<article class="hentry entry-reason">
							<span class="icons">
								<img class="icon-binoculares" src="<?php bloginfo('template_directory') ?>/img/voting-urn.svg" />
								<!-- <i class="icon-votar big-icon"></i> -->
							</span>
							<h4 class="entry-title">Votar informad@</h4>
							<p class="entry-summary">
								Queremos darte datos relevantes de las nuevas campañas para que cuentes con más y mejor información a la hora de votar y participar en estas elecciones.
							</p>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
