<?php get_header(); ?>

<section class="main-content">
	<div class="row inner-space">
		<div class="large-12 columns white box">
        <?php while (have_posts()): the_post(); ?>
			<h2 class="section-title"><?php the_title(); ?></h2>
            <?php the_content(); ?>
        <?php endwhile; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>