<?php
/*
Template Name: Blog
*/
?>

<?php 
    get_header(); 
    the_post();
?>


<section class="main-content">
    <div class="row inner-space space-top">
        <div class="large-11 large-centered white box">

            <div class="row space-bottom">
                <div class="large-9 large-centered columns text-center">
                    <h2 class="section-title">
                        <?php the_title(); ?>
                    </h2>
                </div>
            </div>

            <?php
            $cat_noticias_id = get_cat_ID('Noticias');
            $post_query = new WP_Query([
                'posts_per_page' => 8,
                'orderby'        => 'date',
                'order'          => 'DESC',
                'post_status'    => 'publish',
                'paged'          => get_query_var('paged'),
                'tag_id'         => is_numeric(get_query_var('tag_id')) ? get_query_var('tag_id') : '',
                'category__not_in' => [$cat_noticias_id],
            ]);
            ?>

            <?php if ( $post_query->have_posts() ) : ?>

                <div class="row">
                    <div class="large-9 columns">
                        <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
                            <div class="noticia">
                                <div class="large-11 large-centered columns inner-small-space">
                                     <?php 
                                        $post_format = get_post_format();
                                        if (has_post_thumbnail() && ( ($post_format == 'image') || (empty($post_format)) )): 
                                    ?>
                                        <div class="thumbnail-post">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php the_post_thumbnail('landscape-big'); ?>
                                            </a>
                                        </div>
                                    <?php elseif ( !empty($post->post_url_video) && ($post_format == 'video') ): ?>
                                        <div class="entry-video flex-video widescreen">
                                            <?php echo videos::get_video($post->post_url_video); ?>
                                        </div>
                                    <?php endif; ?>
                                    <div class="row titulo">
                                        <div class="small-12 columns">
                                            <h4>
                                                <a href="<?php the_permalink() ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="row detalles">
                                        <div class="small-12 large-5 columns">
                                            <i class="material-icons">label</i><?php the_category(' ',', '); ?>
                                        </div>
                                        <div class="small-12 large-4 columns large-text-right">
                                            <i class="material-icons">today</i> <?php the_time('d \d\e F \d\e Y') ?>
                                        </div>
                                    </div>
                                    <div class="row contenido">
                                        <div class="small-12 columns">
                                            <?php the_content('Ver más'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php if (function_exists('wp_pagenavi')) { wp_pagenavi(['query' => $post_query]); } ?>
                        </div>
                        <div class="large-3 columns">
                            <aside class="sidebar">
                                <?php dynamic_sidebar('page'); ?>
                            </aside>
                        </div>
                    </div>


                <?php wp_reset_postdata(); ?>

            <?php else : ?>
                <p>No hay noticias</p>
            <?php endif; ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>
