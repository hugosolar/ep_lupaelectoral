<footer class="main-footer">
	<div class="row inner-space">
		<div class="large-4 columns">
			<img src="<?php bloginfo('template_directory') ?>/img/logo-blanco.svg" alt="">
		</div>
		<div class="large-4 columns">
			<h4 class="footer-title">Quiénes somos</h4>
			<p>
				Esta plataforma es un proyecto de <strong>Espacio Público</strong>, centro de estudios independiente, de centroizquierda, conformado por un grupo de profesionales de distintas disciplinas, que tiene como objetivo contribuir a mejorar el debate, diseño y ejecución de políticas públicas, con el fin de desarrollar mejores oportunidades para todas y todos.
			</p>
		</div>
		<div class="large-4 columns">
			<h4 class="footer-title">Contáctanos</h4>
			<ul class="no-bullet">
				<li>
					<i class="material-icons">home</i> Santa Lucía 188, piso 7, Santiago
				</li>
				<li>
					<i class="material-icons">phone</i> <a href="tel:+56223334502">(+56 2) 2333 4502</a>
				</li>
				<li>
					<i class="material-icons">email</i> <a href="mailto:contacto@espaciopublico.cl">contacto@espaciopublico.cl</a>
				</li>
				<li>
					<i class="material-icons">link</i> <a href="http://www.espaciopublico.cl/">www.espaciopublico.cl</a>
				</li>
				<li class="rrss">
					<span class="dashicons dashicons-facebook"></span> <a href="https://www.facebook.com/espaciopublicochile/" target="_blank">https://www.facebook.com/espaciopublicochile/</a>
				</li>
				<li class="rrss">
					<span class="dashicons dashicons-twitter"></span> <a href="https://twitter.com/EsPublicoCL" target="_blank">https://twitter.com/EsPublicoCL</a>
				</li>
			</ul>
		</div>

	</div>
</footer>
<?php wp_footer(); ?>