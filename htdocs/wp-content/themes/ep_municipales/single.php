<?php 
	get_header(); 
	the_post();
?>
<section class="main-content">
	<div class="row inner-space space-top">
		<div class="large-10 large-centered columns white box">
			<header class="single-header">
				<?php if (has_post_thumbnail()): ?>
					<div class="thumbnail-post">
						<?php the_post_thumbnail('landscape-big'); ?>
					</div>
				<?php endif; ?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>
			<section class="single-content">
				<div class="row inner-space">
					<div class="large-9 columns">
						<div class="row entry-meta">
							<div class="small-12 large-5 columns">
		                        <i class="material-icons">label</i><?php the_category(' ',', '); ?>
		                    </div>
		                    <div class="small-12 large-4 columns large-text-right">
		                        <i class="material-icons">today</i> <?php the_time('d \d\e F \d\e Y') ?>
		                    </div>
						</div>
						<?php the_content(); ?>
						<button href="#" onclick="window.history.back()" class="button primary">Volver</button>
					</div>
					<div class="large-3 columns">
						<aside class="sidebar">
							<?php dynamic_sidebar( 'single' ); ?>
						</aside>
					</div>
				</div>
			</section>
		</div>
	</div>
</section>
<?php get_footer(); ?>