<?php 
	/*
		Template name: Gastos
	*/
	get_header(); 
?>
<section class="main-content" ng-controller="aportesCtrl" ng-cloak>
	<div class="row inner-space">
		<div class="large-12 columns">
			<!-- <h2 class="section-title"><?php the_title(); ?></h2> -->
		</div>
	</div>
	<div class="row">
		<div class="large-7 columns">
			<div class="white box">
				<div class="clearfix">
					<span class="bold-title float-left">Gastos de</span>
					<div class="float-right select-space">
						<select name="tipo_tipo" ng-model="elementType" ng-change="updateElementList()" id="tipo_tipo" class="select-box">
							<option value="Region" selected="selected">Regiones</option>
							<option value="Candidato">Candidatos</option>
						</select>
					</div>
				</div>
				<div class="switch-content" ng-switch on="elementType">
					<div class="region-filter clearfix" ng-switch-when="Candidato">
						<div class="row">
							<div class="large-6 columns">
								<span class="filter-text">Región</span>
								<select ng-init="selectRegion = regionList[0]" name="selectRegion" id="selectRegion" class="select-box" ng-change="updateComunas(selectRegion)" ng-model="selectRegion" ng-options="region[0].REGION_NOMBRE for region in regionList">
								</select>
							</div>
							<div class="large-6 columns">
								<span class="filter-text">Comuna</span>
								<select name="selectComuna" id="selectComuna" class="select-box" ng-change="updateCandidato(selectComuna)" ng-model="selectComuna" ng-options="comuna.COMUNA_NOMBRE for comuna in comunasList">
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="white box">
					<table class="formated-tables">
						<thead>
							<tr>
								<th>{{elementType}}</th>
								<th>Monto</th>
							</tr>
						</thead>
						<tbody ng-switch on="elementType">

							<tr ng-class="{ 'active': value[0].REGION_ID == selectedIndex }" ng-repeat="value in tableList" ng-click="selectElement(value)" ng-switch-when="Region">

								<td>{{value[0].REGION_NOMBRE}}</td>
								<td>{{value[1].monto | currency:"$":0}}</td>
							</tr>
							<tr ng-class="{ 'active': value.id == selectedIndex }" ng-repeat="value in tableList" ng-click="selectElement(value)" ng-switch-when="Candidato">

								<td>{{value.nombre}}</td>
								<td>{{value.total_gastos | currency:"$":0}}</td>
							</tr>
						</tbody>
					</table>
			</div>
		</div>
		<div class="large-5 columns">
			<div class="white box">
				<div ng-view></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="white box">
				<div class="top10tables" ng-switch on="elementType">
					<div class="candidate-table" ng-switch-when="Candidato">

						<h3 class="main-title"><i class="material-icons">local_atm</i> <span>Top 10 gastos de candidatos</span></h3>
						<div ng-controller="top10AportesCandidato">
							<table class="formated-tables no-hover">
								<thead>
									<tr>
										<th ng-repeat="item in headersTable" class="{{item.class}}">{{item.value}}</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="value in elementList">
										<td ng-repeat="(key, data) in value" class="{{data.class}}">{{data.value}}</td>
									</tr>
								</tbody>
							</table>
							<div ng-show="elementList.length == 0">
								<div class="callout">
									<p>Cargando datos...</p>
								</div>
							</div>
						</div>
					</div>
					<div class="region-table" ng-switch-when="Region">
						<h3 class="main-title"><i class="material-icons">local_atm</i> <span>Top 10 gastos de Regiones</span></h3>
						<div ng-controller="top10AportesRegiones">
							<table class="formated-tables no-hover">
									<thead>
										<tr>
											<th ng-repeat="item in headersTable" class="{{item.class}}">{{item.value}}</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="value in elementList">
											<td ng-repeat="(key, data) in value" class="{{data.class}}">{{data.value}}</td>
										</tr>
									</tbody>
								</table>
								<div ng-show="elementList.length == 0">
									<div class="callout">
										<p>Cargando datos...</p>
									</div>
								</div>
							<div class="row">
								<div class="large-8 columns button-list">
									<div class="row large-up-3 clearfix">
										<div class="column">
											<button ng-click="spendRegionLimit()" class="button" ng-class="{ 'active': 1 == listButton }">
												<i class="material-icons">location_on</i><br>
												<span>Gasto por región</span>
											</button>
											<span class="has-tip" tooltips tooltip-template="Total de gastos separados por región y comparados con gastos de año 2012"><i class="material-icons">live_help</i></span>
										</div>
										<div class="column">
											<button ng-click="spendLocationLimit()" class="button" ng-class="{ 'active': 2 == listButton }">
												<i class="material-icons">map</i><br>
												<span>% Límite al Gasto Comuna</span>
											</button>

											<span class="has-tip" tooltips tooltip-template="Porcentaje de gastos con respecto al límite de gastos establecido en la comuna"><i class="material-icons">live_help</i></span>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if (VISTA_COMPARATIVA): ?>
	<div class="row" ng-controller="aportesPastElection">
		<div class="large-12 columns">
			<div class="white box">
				<span class="bold-title">Comparativa año 2012</span>

				<gauge-chart id="aportesPasados" options="pastChart" height="100%" width="100%"></gauge-chart>

				<div id="leyendaComparativa"></div>
			</div>
		</div> 
	</div>
	<?php endif; ?>
</section>
<div ng-show="::false" class="big-loader">
    <div class="text-loader">
        Cargando...
        <div class="sk-folding-cube">
		  <div class="sk-cube1 sk-cube"></div>
		  <div class="sk-cube2 sk-cube"></div>
		  <div class="sk-cube4 sk-cube"></div>
		  <div class="sk-cube3 sk-cube"></div>
		</div>
    </div>
</div>
<?php get_footer(); ?>