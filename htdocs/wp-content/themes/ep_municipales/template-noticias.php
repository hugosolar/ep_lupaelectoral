<?php
/*
Template Name: Noticias
*/
?>

<?php get_header(); ?>


<section class="main-content">
    <div class="row inner-space">
        <div class="large-11 large-centered white box">
            <?php
                $tag = null;
                // FIX: debiese usarse get_query_var, pero no esta devolviendo el dato :-/
                //      (tengo mezclados los contextos de los posts?)
                if (is_numeric($wp_query->query_vars['tag_id'])) {
                    $tag = get_tag($wp_query->query_vars['tag_id']);
                }
            ?>
            <div class="row space-bottom">
                <div class="large-9 large-centered columns text-center">
                    <h2 class="section-title">
                        <?php if (is_null($tag)): ?>
                            NOTICIAS
                        <?php else: ?>
                            <a href="<?php echo site_url('/noticias'); ?>">NOTICIAS</a> - <small><?php echo $tag->name; ?></small>
                        <?php endif; ?>
                    </h2>
                    <h5 class="subheader">
                        Aquí encontrarás las noticias destacadas relacionadas con las elecciones municipales y datos semanales de interés entregados por la Lupa Electoral.
                    </h5>
                </div>
            </div>

            <?php
            $post_query = new WP_Query([
                'posts_per_page' => 8,
                'orderby'        => 'date',
                'order'          => 'DESC',
                'category_name'   => 'noticias',
                'post_status'    => 'publish',
                'paged'          => get_query_var('paged'),
                'tag_id'         => is_numeric(get_query_var('tag_id')) ? get_query_var('tag_id') : '',
            ]);
            ?>

            <?php if ( $post_query->have_posts() ) : ?>

                <div class="row space-bottom tag-cloud">
                    <div class="large-7 large-centered columns text-center">
                        <?php wp_tag_cloud(['smallest' => 0.8, 'largest' => 1.6, 'unit' => 'rem']); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="small-12 text-right space-bottom">
                        <?php if (function_exists('wp_pagenavi')) { wp_pagenavi(['query' => $post_query]); } ?>
                    </div>
                </div>

                <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
                <div class="row noticia">
                    <div class="large-11 large-centered columns inner-small-space">
                        <div class="row titulo">
                            <div class="small-12 columns">
                                <h4>
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'post_url_noticia', true); ?>" target="_blank">
                                        <?php the_title(); ?>
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <div class="row detalles">
                            <div class="small-12 large-6 columns">
                                <i class="material-icons">label</i><?php the_tags(' ',', '); ?>
                            </div>
                            <div class="small-12 large-3 columns large-text-right">
                                <i class="material-icons">today</i> <?php the_time('d \d\e F \d\e Y') ?>
                            </div>
                            <div class="small-12 large-3 columns large-text-left">
                                <i class="material-icons">open_in_new</i> <a href="<?php echo get_post_meta(get_the_ID(), 'post_url_noticia', true); ?>" target="_blank">Ver noticia</a>
                            </div>
                        </div>
                        <div class="row contenido">
                            <div class="small-12 columns">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>

                <div class="row">
                    <div class="small-12 text-right space-top">
                        <?php if (function_exists('wp_pagenavi')) { wp_pagenavi(['query' => $post_query]); } ?>
                    </div>
                </div>

                <?php wp_reset_postdata(); ?>

            <?php else : ?>
                <p>No hay noticias</p>
            <?php endif; ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>
