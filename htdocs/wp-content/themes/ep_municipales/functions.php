<?php
/**
 * Functions: lista de funciones del theme Municipales
 * Funciones nuevas para el sitio
 * EP Municipales
 * @version 2.0
 * @package ep_municipales
 */

/* Theme Constants (to speed up some common things) ------*/
define('HOME_URI', get_bloginfo('url'));
define('PRE_HOME_URI', get_bloginfo('url') . '/wp-content/themes');
define('SITE_NAME', get_bloginfo('name'));
define('THEME_URI', get_template_directory_uri());
define('THEME_IMG', THEME_URI . '/img');
define('THEME_CSS', THEME_URI . '/css');
define('THEME_FONTS', THEME_URI . '/fonts');
define('PRE_THEME_JS', PRE_HOME_URI . '/js');
define('THEME_JS', THEME_URI . '/js');
define('MULTILINGUAL', function_exists('qtrans_use'));
/*
calling related files
 */
include TEMPLATEPATH . '/inc/settings.php';
include TEMPLATEPATH . '/inc/helpers.php';
include TEMPLATEPATH . '/inc/site.php';
include TEMPLATEPATH . '/inc/render.php';
include TEMPLATEPATH . '/inc/filters.php';
include TEMPLATEPATH . '/inc/admin_theme_options.php'; // FIX: va este archivo o settings.php ?

// Deshabilitada la barra de admin mientras se ve el sitio público
show_admin_bar( false );

/**
 * Images
 * ------
 * */
// Add theme suppor for post thumbnails
add_theme_support('post-thumbnails');
// Define the default post thumbnail size

// set_post_thumbnail_size( 200, 130, true );

// Define custom thumbnail sizes
// add_image_size( $name, $width, $height, $crop );
add_image_size('squared', 300, 300, true);
add_image_size('featured-home', 1200, 500, true);
add_image_size('landscape-small', 100, 80, true);
add_image_size('landscape-medium', 300, 205, true);
add_image_size('landscape-big', 1000, 500, true);

/*
REGISTER SIDEBARS
 */
/*
Theme sidebars
 */
$mandatory_sidebars = array(
    'Página'  => array(
        'name' => 'page',
        'size' => 4,
    ),

    'Entrada' => array(
        'name' => 'single',
        'size' => 4,
    ),
);
$mandatory_sidebars = apply_filters('vtte_base_mandatory_sidebars', $mandatory_sidebars);
foreach ($mandatory_sidebars as $sidebar => $id_sidebar) {
    register_sidebar(array(
        'name'          => $sidebar,
        'id'            => $id_sidebar['name'],
        'before_widget' => '<section id="%1$s" class="widget %2$s">' . "\n",
        'after_widget'  => '</section>',
        'before_title'  => '<header class="widget-header"><h3 class="widget-title">',
        'after_title'   => '</h3></header>',
        'size'          => $id_sidebar['size'],
    ));
}

/**
 * Theme specific stuff
 * --------------------
 * */

/**
 * Theme singleton class
 * ---------------------
 * Stores various theme and site specific info and groups custom methods
 **/
class site
{
    private static $instance;

    protected $settings;

    const id                         = __CLASS__;
    const theme_ver                  = '20140624';
    const theme_settings_permissions = 'edit_theme_options';
    private function __construct()
    {
        /**
         * Get our custom theme options so we can easily access them
         * on templates or other admin pages
         * */
        // $this->settings = get_option( __CLASS__ .'_theme_settings' );

        $this->actions_manager();

    }
    public function __get($key)
    {
        return isset($this->$key) ? $this->$key : null;
    }
    public function __isset($key)
    {
        return isset($this->$key);
    }
    public static function get_instance()
    {
        if (!isset(self::$instance)) {
            $c              = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
    public function __clone()
    {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }
    /**
     * Setup theme actions, both in the front and back end
     * */
    public function actions_manager()
    {
        if (is_admin()) {
            //
        } else {
            //add_filter('wp_title', array($this, 'original_title'), 1, 1);
            //add_filter('wp_title', array($this, 'wp_title'), 99, 3);
            //add_filter('wpseo_canonical', array($this, 'canonical'), 99, 1);
            //add_filter('request', array($this, 'filter_request'));
        }
        add_action('after_setup_theme', array($this, 'setup_theme'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_styles'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_action('enqueue_scripts', array($this, 'enqueue_scripts'));
        add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));
        add_action('init', array($this, 'init_functions'));
        add_action('init', array($this, 'register_menus_locations'));

    }
    public function init_functions()
    {
        add_post_type_support('page', 'excerpt');
    }
    /**
     * habilitar funcionalidades del tema
     * @return void
     */
    public function setup_theme()
    {
        // habilitar post formats
        add_theme_support('post-formats', array('gallery', 'image', 'video', 'audio'));
        add_theme_support('post-thumbnails');
        add_theme_support('automatic_feed_links');
        add_theme_support('menus');
    }

    public function register_menus_locations()
    {
        register_nav_menus(array(
            'principal' => 'Menú Principal',
        ));
    }

    public function get_post_thumbnail_url($postid = null, $size = 'landscape-medium')
    {
        if (is_null($postid)) {
            global $post;
            $postid = $post->ID;
        }
        $thumb_id = get_post_thumbnail_id($postid);
        $img_src  = wp_get_attachment_image_src($thumb_id, $size);
        return $img_src ? current($img_src) : '';
    }

    public function enqueue_styles()
    {
        // Front-end styles
        wp_enqueue_style('material_design_icons', 'https://fonts.googleapis.com/icon?family=Material+Icons');
        wp_enqueue_style('amchart_export', THEME_JS . '/amcharts/plugins/export/export.css');
        wp_enqueue_style('plugins_dependencies', THEME_CSS . '/dependencies.css');
        wp_enqueue_style('sass_plugins_dependencies', THEME_CSS . '/sass-dependencies.css');
        wp_enqueue_style('ep_municipales_style', get_template_directory_uri() . '/style.css');
        wp_enqueue_style('dashicons');
    }

    public function admin_enqueue_scripts()
    {

        // admin scripts
        global $pagenow;
        // wp_enqueue_script( 'theme_admin_scripts', THEME_JS .'/admin_scripts.js', array('jquery'), true );
        if (is_admin() && (($pagenow == 'index.php') || ($pagenow == 'admin.php'))) {
            if ($_GET['page'] == 'ep_municipales-site-settings') {
                wp_enqueue_media();
            }

            //wp_enqueue_script( 'script-admin', THEME_JS . '/admin_scripts.js', array('jquery'), THEME_VERSION );
        }
        if (is_admin() && (($pagenow == 'link-add.php') || ($pagenow == 'link.php'))) {
            wp_enqueue_media();
        }
        if (is_admin() && ($pagenow == 'widgets.php')) {
            wp_enqueue_media();
            //wp_enqueue_script( 'script-admin', THEME_JS . '/admin_scripts.js', array('jquery'), THEME_VERSION );
        }
        // if ($pagenow == 'nav-menus.php' || $pagenow == 'edit-tags.php') {
        //     wp_enqueue_media();
        // }
        if (is_admin()) {
            wp_enqueue_script('admin-tabs', THEME_JS . '/admin_tabs.js', true, array('jquery'));
            wp_enqueue_script('jquery-ui-tabs', true, array('jquery'));
        }
    }

    public function enqueue_scripts()
    {
        // front-end scripts
        wp_enqueue_script('jquery', true);
        wp_enqueue_script('datatables', THEME_JS . '/datatables.js', array('jquery'), self::theme_ver, true);
        //Por alguna razon si no incluyo esto, foundation tira error JS, creo que es un bug segun lei
        wp_enqueue_script('media-query', THEME_JS . '/foundation.util.mediaQuery.js', array('dependencies'), self::theme_ver, true);
      
        wp_enqueue_script('dependencies', THEME_JS . '/dependencies.js', array('jquery'), self::theme_ver, true);
        wp_enqueue_script('leaflet', THEME_JS . '/leaflet.js', array('jquery'), self::theme_ver, true);
        wp_enqueue_script('angular', THEME_JS . '/angular.js', array('jquery'), self::theme_ver, true);
        /*Amcharts*/
        wp_enqueue_script('amcharts', THEME_JS . '/amcharts/amcharts.js', array('jquery'), self::theme_ver, true);
        wp_enqueue_script('amcharts-gauge', THEME_JS . '/amcharts/gauge.js', array('amcharts'), self::theme_ver, true);
        wp_enqueue_script('amcharts-pie', THEME_JS . '/amcharts/pie.js', array('amcharts'), self::theme_ver, true);
        wp_enqueue_script('amcharts-serial', THEME_JS . '/amcharts/serial.js', array('amcharts'), self::theme_ver, true);
        wp_enqueue_script('amcharts-xy', THEME_JS . '/amcharts/xy.js', array('amcharts'), self::theme_ver, true);
        wp_enqueue_script('amcharts-export', THEME_JS . '/amcharts/plugins/export/export.min.js', array('amcharts'), self::theme_ver, true);
        wp_enqueue_script('amcharts-responsive', THEME_JS . '/amcharts/plugins/responsive/responsive.min.js', array('amcharts'), self::theme_ver, true);

        if (is_page('aportes')) {
	        wp_enqueue_script('lupa-electoral-aportes-app', THEME_JS . '/angular-aportes.js', array('angular'), self::theme_ver, true);
	        wp_localize_script(
	            'lupa-electoral-aportes-app',
	            'Localize',
	            array(
	                'partials' => trailingslashit(get_template_directory_uri()) . 'partials/',
	                'gastos' => VISTA_GASTOS,
	                'comparativa' => VISTA_COMPARATIVA,
	                'site_url' => site_url(),
	            )
	        );
        }
        if (is_page('gastos')) {
	        wp_enqueue_script('lupa-electoral-gastos-app', THEME_JS . '/angular-gastos.js', array('angular'), self::theme_ver, true);
	        wp_localize_script(
	            'lupa-electoral-gastos-app',
	            'Localize',
	            array(
	                'partials' => trailingslashit(get_template_directory_uri()) . 'partials/',
	                'gastos' => VISTA_GASTOS,
	                'comparativa' => VISTA_COMPARATIVA,
	                'site_url' => site_url()
	            )
	        );
        }
        if (is_page('aportante')) {
            wp_enqueue_script('angular-datatables', THEME_JS . '/angular-datatables.js', array('angular'), self::theme_ver, true);
            wp_enqueue_script('lupa-electoral-aportante-app', THEME_JS . '/angular-aportante.js', array('angular'), self::theme_ver, true);
            wp_localize_script(
                'lupa-electoral-aportante-app',
                'Localize',
                array(
                    'partials' => trailingslashit(get_template_directory_uri()) . 'partials/',
                    'local_json' => JSON_TEST_URL,
                    'gastos' => VISTA_GASTOS,
                    'vuelta' => VUELTA,
                    'site_url' => site_url(),
                    'aporte_partido' => APORTE_PARTIDO
                )
            );
        }
         if (is_page('perfil')) {
         	wp_enqueue_script('angular-datatables', THEME_JS . '/angular-datatables.js', array('angular'), self::theme_ver, true);
	        wp_enqueue_script('lupa-electoral-perfil-app', THEME_JS . '/angular-perfil.js', array('angular'), self::theme_ver, true);
	        wp_localize_script(
	            'lupa-electoral-perfil-app',
	            'Localize',
	            array(
	                'partials' => trailingslashit(get_template_directory_uri()) . 'partials/',
	                'local_json' => JSON_TEST_URL,
	                'gastos' => VISTA_GASTOS,
                    'vuelta' => VUELTA,
	                'site_url' => site_url(),
                    'aporte_partido' => APORTE_PARTIDO
	            )
	        );
        }
        if (is_page('comuna')) {
	        wp_enqueue_script('lupa-electoral-comuna-app', THEME_JS . '/angular-comuna.js', array('angular'), self::theme_ver, true);
	        wp_localize_script(
	            'lupa-electoral-comuna-app',
	            'Localize',
	            array(
	                'partials' => trailingslashit(get_template_directory_uri()) . 'partials/',
	                'gastos' => VISTA_GASTOS,
	                'site_url' => site_url()
	            )
	        );
        }
        if (is_page('denuncias')) {
        	wp_enqueue_script('angular-datatables', THEME_JS . '/angular-datatables.js', array('angular'), self::theme_ver, true);
	        wp_enqueue_script('lupa-electoral-denuncias-app', THEME_JS . '/angular-denuncias.js', array('angular'), self::theme_ver, true);
	        wp_localize_script(
	            'lupa-electoral-denuncias-app',
	            'Localize',
	            array(
	                'partials' => trailingslashit(get_template_directory_uri()) . 'partials/',
	                'gastos' => VISTA_GASTOS,
	                'local_json' => JSON_TEST_URL,
	                'site_url' => site_url()
	            )
	        );
        }
        wp_enqueue_script('angular-amcharts', THEME_JS . '/amcharts-angular.js', array('angular'), self::theme_ver, true);

        wp_enqueue_script('ep_municipales_script', THEME_JS . '/script.js', array('jquery'), self::theme_ver, true);
        wp_localize_script('ep_municipales_script', 'Ajax', [
            'url'      => admin_url('admin-ajax.php'),
            'site_url' => site_url(),
        ]);

        if (is_home()) {
            wp_enqueue_script('raphael_js', 'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.0/raphael-min.js', array('jquery'), self::theme_ver, true);
            wp_enqueue_script('municipales_map', THEME_JS . '/map.js', array('jquery'), self::theme_ver, true);
            wp_enqueue_script('countdown', THEME_JS . '/jquery.countdown.min.js', array('jquery'), self::theme_ver, true);
            //attach data to script.js
            $svg_data = array(
                'svg'      => get_bloginfo('template_directory') . '/img/map3.svg',
                'ajax_url' => admin_url('admin-ajax.php'),
            );
            wp_localize_script('municipales_map', 'Map', $svg_data);
            // wp_enqueue_script('jquery-ui-autocomplete', true, array('jquery'));
        }
    }
}

/**
 * Instantiate the class object
 * You can access its methods or variables by globalizing $wpbp or
 * using the get_instance() method (it's a singleton class, so it won't
 * create a new instance)
 * */

$_s = site::get_instance();
