<?php

$path_json = "data/";

function clean($str) {
    // http://stackoverflow.com/a/23066553
    return trim(preg_replace('/[^\PC\s]/u', '', str_replace("\n", "", $str)));
}

// CSV Header
echo "\"REGION\";\"COMUNA\";\"NOMBRE PUNTO\";\"LATITUD\";\"LONGITUD\"\n";

if (($dir_json = opendir($path_json))) {
    while (($region_name = readdir($dir_json)) !== false) {
        $path_region = $path_json . $region_name;
        if (!in_array($region_name, array('.', '..')) && is_dir($path_region)) {
            if (($dir_region = opendir($path_region))) {
                while (($comuna_file_name = readdir($dir_region)) !== false) {
                    $path_comuna = $path_region . '/' . $comuna_file_name;
                    if (!in_array($comuna_file_name, array('.', '..')) && is_file($path_comuna)) {
                        $comuna_name = str_replace('.json.str', '', $comuna_file_name);
                        $json_str    = file_get_contents($path_comuna);
                        $json        = json_decode($json_str, true);
                        foreach ($json as $entry) {
                            if ($entry['geometry']['type'] == 'Point') {
                                $longitud = clean($entry['geometry']['coordinates'][0]);
                                $latitud  = clean($entry['geometry']['coordinates'][1]);
                                $nombre   = clean($entry['properties']['layer']['data']['name']);
                                echo "\"$region_name\";\"$comuna_name\";\"$nombre\";\"$latitud\";\"$longitud\"\n";
                            }
                        }
                    }
                }
                closedir($dir_region);
            }
        }
    }
    closedir($dir_json);
}
