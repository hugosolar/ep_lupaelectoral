<?php

$data_str = file_get_contents('regiones-comunas-ids.json.str');
$data     = json_decode($data_str);

foreach ($data as $region_name => $region_data) {
    mkdir('data/' . $region_name);
    foreach ($region_data as $comuna_name => $comuna_id) {
        file_put_contents(
            'data/' . $region_name . '/' . $comuna_name . '.json.str',
	    fopen('https://ih-dss-api-servel1.herokuapp.com/visualizations-by-token/' . $comuna_id . '/data/', 'r')
        );
    }
}
