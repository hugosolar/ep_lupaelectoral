/*
 * Code that must be executed within the browser.
 * This uses the data and resources loaded un DOM to extract
 * the geographic and identification data.
 *
 * TL;DR: copy&paste this code on the browser console and
 * copy&paste the result on a file named regiones-comunas-ids.json.str
 *
 * URL: https://www.servel.cl/nomina-de-espacios-publicos-autorizados-elecciones-presidencial-parlamentarias-y-de-cores/
 */

var result = {};
jQuery(".tab-pane.clearfix").each(function (r_index, r_elem) {
	var r_txt = jQuery(r_elem).clone().children().remove().end().text().trim();
	result[r_txt] = {};
	jQuery(r_elem).find(".panel.panel-default").each(function (c_index, c_elem) {
		var c_txt = jQuery(c_elem).find("h5").text().trim();
		var c_id = jQuery(c_elem).find(".panel-collapse a:not([class^=rspkr_dr])")
			.attr('href')
			.replace('https://app-servel-2017.instagis.net/#/visualization/', '');
		result[r_txt][c_txt] = c_id;
	});
});
copy(JSON.stringify(result));
