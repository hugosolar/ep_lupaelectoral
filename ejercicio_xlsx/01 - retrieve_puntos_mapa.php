<?php

require_once 'spout-2.5.0/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

$filePath = 'documentos/01 - Información sobre puntos habilitados para campaña electoral.xlsx';

$reader = ReaderFactory::create(Type::XLSX);
$reader->open($filePath);

$puntos_mapa = [];

foreach ($reader->getSheetIterator() as $sheet) {
    $pass_header = false;

    // Only the first sheet
    if ($sheet->getIndex() === 0) {
        foreach ($sheet->getRowIterator() as $row) {
            // First row is the Column Header
            if (! $pass_header) {
                $pass_header = true;
                continue;
            }

            $puntos_mapa[] = [
                "REGION"         => trim($row[0]),
                "COMUNA"         => trim($row[1]),
                "NOMBRE_ESPACIO" => trim($row[2]),
                "LONGITUD"       => trim($row[3]),
                "LATITUD"        => trim($row[4]),
            ];

            echo implode($puntos_mapa[count($puntos_mapa) - 1], "\t") . "\n";
		}
	}
	break;
}

$reader->close();

// echo json_encode($puntos_mapa) . "\n";

