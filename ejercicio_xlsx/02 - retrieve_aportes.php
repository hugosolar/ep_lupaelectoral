<?php

require_once 'spout-2.5.0/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

$filePath = 'documentos/02- Información sobre aportes electorales.xlsx';

$reader = ReaderFactory::create(Type::XLSX);
$reader->open($filePath);

$aportes = [];

foreach ($reader->getSheetIterator() as $sheet) {
    $pass_header = false;

    // Only the first sheet
    if ($sheet->getIndex() === 0) {
        foreach ($sheet->getRowIterator() as $row) {
            // First row is the Column Header
            if (! $pass_header) {
                // var_dump($row); die();
                $pass_header = true;
                continue;
            }

            $aportes[] = [
                "NOMBRE_APORTANTE"         => trim($row[0]),
                "NOMBRE_BENEFICIARIO"      => trim($row[1]),
                "REGION"                   => trim($row[2]),
                "COMUNA"                   => trim($row[3]),
                "PACTO"                    => trim($row[4]),
                "PARTIDO"                  => trim($row[5]),
                "TIPO_APORTE"              => trim($row[6]),
                // INFO: Conversión de EXCEL_DATE a formato "d-m-Y"
                "FECHA_APORTE"             => gmdate("d-m-Y", (((int)$row[7] -25569) * 86400)),
                // INFO: Conversión a valor numérico
                "MONTO_APORTE"             => (int) str_replace(['$', '.'], ['', ''], $row[8]),
            ];

            echo implode($aportes[count($aportes) - 1], "\t") . "\n";
		}
	}
	break;
}

$reader->close();

// echo json_encode($aportes) . "\n";

