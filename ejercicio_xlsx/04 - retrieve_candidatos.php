<?php

require_once 'spout-2.5.0/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

$filePath = 'documentos/04 - Información sobre listado de candidatos a alcaldes.xlsx';

$reader = ReaderFactory::create(Type::XLSX);
$reader->open($filePath);

$candidatos = [];

foreach ($reader->getSheetIterator() as $sheet) {
    $pass_header = false;

    // Only the first sheet
    if ($sheet->getIndex() === 0) {
        foreach ($sheet->getRowIterator() as $row) {
            // First row is the Column Header
            if (! $pass_header) {
                $pass_header = true;
                continue;
            }

            $candidatos[] = [
                "REGION"                => trim($row[0]),
                "COMUNA"                => trim($row[1]),
                "NOMBRE_CANDIDATO"      => trim($row[2]),
                "PARTIDO"               => trim($row[3]),
                "PACTO"                 => trim($row[4]),
                "DECLARACION_INTERESES" => trim($row[5]),
            ];

            echo implode($candidatos[count($candidatos) - 1], "\t") . "\n";
		}
	}
	break;
}

$reader->close();

// echo json_encode($candidatos) . "\n";

