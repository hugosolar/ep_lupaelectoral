<?php

require_once 'spout-2.5.0/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

$filePath = 'documentos/03 - Información sobre gastos electorales.xlsx';

$reader = ReaderFactory::create(Type::XLSX);
$reader->open($filePath);

$gastos = [];

foreach ($reader->getSheetIterator() as $sheet) {
    $pass_header = false;

    // Only the first sheet
    if ($sheet->getIndex() === 0) {
        foreach ($sheet->getRowIterator() as $row) {
            // First row is the Column Header
            if (! $pass_header) {
                // var_dump($row); die();
                $pass_header = true;
                continue;
            }

            $gastos[] = [
				"ELECCION" 					 => trim($row[0]),
				"COMUNA"   					 => trim($row[1]),
				"RUT_CANDIDATO" 			 => trim($row[2]),
				"DV_RUT_CANDIDATO" 			 => trim($row[3]),
				"NOMBRE_CANDIDATO" 			 => trim($row[4]),
				"FILIACION" 				 => trim($row[5]),
				"PARTIDO" 					 => trim($row[6]),
				"PACTO" 				 	 => trim($row[7]),
				"SUBPACTO" 					 => trim($row[8]),
				"TIPO_PLANILLA" 			 => trim($row[9]),
				"RUT_PROVEEDOR" 			 => trim($row[10]),
				"DV_RUT_PROVEEDOR" 			 => trim($row[11]),
                "NOMBRE_PROVEEDOR" 			 => trim($row[12]),
                // INFO: Conversión de EXCEL_DATE a formato "d-m-Y"
				"FECHA_DOCUMENTO" 			 => gmdate("d-m-Y", (((int)$row[13] -25569) * 86400)),
				"TIPO_DOCUMENTO" 			 => trim($row[14]),
				"DESCRIPCION_TIPO_DOCUMENTO" => trim($row[15]),
				"NUMERO_DOCUMENTO" 			 => trim($row[16]),
				"TIPO_CUENTA" 				 => trim($row[17]),
				"DESCRIPCION_TIPO_CUENTA" 	 => trim($row[18]),
				"PARA_REEMBOLSO" 			 => trim($row[19]),
				"GLOSA_DOCUMENTO" 			 => trim($row[20]),
                // INFO: Conversión a valor numérico
				"MONTO" 					 => (int) str_replace(['$', '.'], ['', ''], $row[21])
            ];

            echo implode($gastos[count($gastos) - 1], "\t") . "\n";
		}
	}
	break;
}

$reader->close();

// echo json_encode($gastos) . "\n";

